/**
 * Created by donald on 11/18/2015.
 */

function overrider(dir){
    var parameters = [];
    var dataURI = dir+"/data/customizr.json";
    var logoURI=dir+"/logo/";
    $.getJSON(dataURI, function(json) {
        //console.log(json.file_logo_name); // this will show the info it in firebug console

        /*parameters["status"]=json.status;
        parameters["file_logo_name"]=json.file_logo_name;
        parameters["table_header"]=json.table_header;
        parameters["link_active"]=json.link_active;
        parameters["link_hover"]=json.link_hover;
        parameters["logout_button_default"]=json.logout_button_default;
        parameters["logout_button_hover"]=json.logout_button_hover;
        parameters["primary_button_default"]=json.logout_button_default;
        parameters["primary_button_hover"]=json.logout_button_hover;
        //alert(parameters["file_logo_name"]);*/
        $('#companyLogo').attr('src',logoURI+json.file_logo_name);
        // Avatar span
        $('#avatar_span').css({"border-bottom":"8px solid "+json.link_active});
        //Tables
        $('thead').css({"background-color":json.table_header});
        $('.paginate_button.current').css({
            "background-color":json.table_header
        });
        $('.paginate_button.current').hover({
            "background-color":json.table_header
        });
        //Primary buttons
        $('.btn-primary').css({"background-color":json.primary_button_default});
        $('.btn-primary').hover(function(){$(this).css({"background-color":json.primary_button_hover,"border-color":json.primary_button_hover});},function(){$(this).css({"background-color":json.primary_button_default,"border-color":json.primary_button_default});});
        //Logout button
        $('.btn-danger').css({"background-color":json.logout_button_default});
        $('.btn-danger').hover(function(){$(this).css({"background-color":json.logout_button_hover});},function(){$(this).css({"background-color":json.logout_button_default});});
        //Links
        /*-$('a').hover(function(){$(this).css({"color":json.link_hover});},function(){
            //$(this).css({"color":"#000"});
            //$(this).addClass('active')
        });*/
        //$('a:hover').css({"color":json.link_hover+"!important"});
        $('span.plainte_login').parent().css({"background-color":json.link_active});
        $('#separator_login').css({"border-bottom":"4px solid "+json.link_active});
        $('#company_logo_login').attr('src',logoURI+json.file_logo_name);
        $('li a').hover(
            function(){$(this).css({
                "color":json.link_hover,

            });},
            function(){$(this).css({
                "color":"#333",
                "font-family":"Roboto-Light"
            });
                if($(this).hasClass('active')){
                    $(this).css({"color":json.link_active});
                }
            });
        $('li a.active').css({"color":json.link_active});

        //

    });
    //Setting collected values to application


}
/*$(document).ready(function(){
    alert('oIm customizr let me make my magic');
    //Parsing Json file
    var parameters = [];
    var data = "./data/customizr.json";
    var logo,tableHeader,linkActive,linkDefault,logouButtonDefault,logouButtonHover,primaryButtonDefault,primaryButtonHover;
    $.getJSON("{{asset('customizr/data/customizr.json')}}", function(json) {
        console.log(json.file_logo_name); // this will show the info it in firebug console

        parameters["status"]=json.status;
        parameters["file_logo_name"]=json.file_logo_name;
        parameters["table_header"]=json.table_header;
        parameters["link_active"]=json.link_active;
        parameters["link_hover"]=json.link_hover;
        parameters["logout_button_default"]=json.logout_button_default;
        parameters["logout_button_hover"]=json.logout_button_hover;
        parameters["primary_button_default"]=json.logout_button_default;
        parameters["primary_button_hover"]=json.logout_button_hover;
        alert(parameters["file_logo_name"]);
    });
    alert(parameters["file_logo_name"]);
    //Setting collected values to application
    img="./logo/"+parameters["file_logo_name"];
    $('#companyLogo').attr('src',img);

});*/

