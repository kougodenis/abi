 $(document).ready(function(){

     //$("#main-menu").css("height", $(".borderStyle").height()+parseInt(40) + "px");


     $('#main-menu').metisMenu({
        //  preventDefault: false
     });

        $("#gplainte_gplaintebundle_satisfaction_rdv,#gplainte_gplaintebundle_notification_date,#debut,#fin").datetimepicker({
            lang:'fr',
            timepicker:false,
            format:'d-m-Y'
        });
		
		$("#datejournal").datetimepicker({
         lang:'fr',
         //timepicker:false,
         format:'Y-m-d H:i:s'
     });
		
      $('#myModal').modal();

      $("#gplainte_gplaintebundle_affecter_dateAffectation").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#gplainte_gplaintebundle_satisfaction_rdv,#debut,#fin").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});
     $("#gplainte_gplaintebundle_notification_date").mask("99-99-9999",{placeholder:"dd-mm-yyyy"});

     //var h=0,i=0,j=0;
     $("#gplainte_gplaintebundle_affecter2_plainte,label[for='gplainte_gplaintebundle_affecter2_plainte']").hide();

     $("#gplainte_gplaintebundle_satisfaction_rdv,label[for='gplainte_gplaintebundle_satisfaction_rdv']").hide();
     $("#gplainte_gplaintebundle_satisfaction_reformulation,label[for='gplainte_gplaintebundle_satisfaction_reformulation']").hide();


     $('#gplainte_gplaintebundle_satisfaction_libelle').change(function(e){
         if ($(this).val()=='N'){

             $('#gplainte_gplaintebundle_satisfaction_rdv,label[for="gplainte_gplaintebundle_satisfaction_rdv"]').show();
             $('#gplainte_gplaintebundle_satisfaction_reformulation,label[for="gplainte_gplaintebundle_satisfaction_reformulation"]').show();

         }else{
             $('#gplainte_gplaintebundle_satisfaction_rdv,label[for="gplainte_gplaintebundle_satisfaction_rdv"]').hide();
             $('#gplainte_gplaintebundle_satisfaction_reformulation,label[for="gplainte_gplaintebundle_satisfaction_reformulation"]').hide();

         }
     });


     $('#service,#produit,#agent,#autres,#autreobjet').hide();

     $('#gplainte_gplaintebundle_plainte_client,#gplainte_gplaintebundle_plainte_bureau').chosen();
     //#gplainte_gplaintebundle_plainte_niveau,#gplainte_gplaintebundle_plainte_choix


     $("#gplainte_gplaintebundle_plainte_choix").change(function(e){
     //if ($(this).val()=="1"){
     //    $('#service').show();
     //    $('#gplainte_gplaintebundle_plainte_service').chosen();
     //}else{
     //    $('#service').hide();
     //}
     if ($(this).val()=="3"){
         $('#produit').show();
         $('#gplainte_gplaintebundle_plainte_produit').chosen();
     }else{
         $('#produit').hide();
     }
     //if ($(this).val()=="2"){
     //
     //    $('#agent').show();
     //    $('#gplainte_gplaintebundle_plainte_agent').chosen();
     //
     //}else{
     //    $('#agent').hide();
     //}
     if ($(this).val()=="4"){
         $('#autres').show();
     }else{
         $('#autres').hide();
     }

  });

     $('#bureaux').hide();

     $("input[name=generale]").click(function(e){
         //alert("bonjour");
         if ($(this).val()==2){
             $('#bureaux').show();
         }else{
             $('#bureaux').hide();
         }
     })
     $('#gplainte_gplaintebundle_plainte_niveau').change(function(e){
        if ($(this).val()==16){
            $('#autreobjet').show();
        }else{
            $('#autreobjet').hide();
        }
     });




  });
