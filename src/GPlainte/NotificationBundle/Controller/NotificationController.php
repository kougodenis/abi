<?php

namespace GPlainte\NotificationBundle\Controller;

use GPlainte\GPlainteBundle\Entity\NormeIndicateur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NotificationController extends Controller
{
    public function notificationsAction()
    {

        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            //get Complaint's repository
            $repository = $this->getDoctrine()
                ->getRepository('GPlainteNotificationBundle:Notifs');
            // query for all complaints matching the is_published, is_anonyme
            $notifications = $repository->findAll();
            return $this->render('GPlainteNotificationBundle:Notification:notifications.html.twig',array(
                'notifications' =>$notifications
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

        //return $this->render('GPlainteNotificationBundle:Notification:notifications.html.twig', array('name' => $name));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function configAction(Request $request)
    {
        if ($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $path = substr($this->get('kernel')->getRootDir(), 0, -4) . '/src/GPlainte/NotificationBundle/Data/';
            $filename = 'notification.json';
            $params = json_decode(file_get_contents($path . '' . $filename));



            //get Niveau's repository or Subjects of complaint
            $repository = $this->getDoctrine()
                ->getRepository('GplainteBundle:Niveau');
            // query for all complaints matching the is_published, is_anonyme
            $subjects = $repository->findAll();

            $nullFlag = 0;
            if ($params == NULL) {
                $nullFlag = 1;
            }


//            echo "titi"; die();


            //Form is submission(Updating Alerts Configuration)
            if ($request->getMethod() == 'POST') {
                require_once( $this->get('kernel')->getRootDir().'/../web/plugins/ForceUTF8/Encoding.php');

                //collect form data
                $subjectCount = $request->request->get('_count');

                //Processing Delay - Delai de traitement

                $data = array();
                for($i=0;$i<intval($subjectCount);$i++){
                    //var_dump($subjects[$i]->getLibelle());
                    //$data[] = ;
//                    $libelle = str_replace('à','a',str_replace('ê','e',str_replace('è','e',str_replace('é','e',$subjects[$i]->getLibelle()))));

                    $id = $subjects[$i]->getId();

                    //var_dump($libelle);
                    //$tmp = iconv("utf-8",mb_detect_encoding($libelle), $libelle);
                    //$tmp = \ForceUTF8\Encoding::fixUTF8(\ForceUTF8\Encoding::toUTF8($libelle));
                    //var_dump($tmp);
                    //$tmp = mb_detect_encoding($subjects[$i]->getLibelle(),'UTF-8');
//                    $data[str_replace(' ','_',trim(strtolower($libelle)))] = $request->request->get('_duremax_'.$i);
                    $data[$id] = $request->request->get('_duremax_'.$i);


                }

                //$d=json_encode($data);
                //($d);
                /*var_dump(intval($subjectCount));
                var_dump(count($data));
                var_dump($data);*/
                //var_dump($data);
                //$dureeMax = $request->request->get('_duremax');
                //Recurrence
                $periodeRecurrence = $request->request->get('_perrec');
                $seuilRecurrence = $request->request->get('_seuilrec');
                //TODO:: Ensure entered values are not negative and at least one profile is select when in scenario of profil
                $profils = $request->request->get('_profils');
                $mail = $request->request->get('_mail');
                $phone = $request->request->get('_phone');
                $codetel = $request->request->get('_codetel');
                $param_alerte_affectation_mail=$request->request->get('_param_alerte_affectation_mail');
                $param_alerte_affectation_sms=$request->request->get('_param_alerte_affectation_sms');


                $today = date("d.m.Y h:i:s");

                $jsondata = json_encode(array('dm' => $data, 'pr' => $periodeRecurrence, 'sr' => $seuilRecurrence, 'ft' => "1", 'mail' => $mail,'tel'=>$phone, 'profils' => $profils,'ref_date'=>$today,'param_alerte_affectation_mail'=>$param_alerte_affectation_mail,'param_alerte_affectation_sms'=>$param_alerte_affectation_sms,'codetel'=>$codetel));
                file_put_contents($path . '' . $filename, $jsondata);

                file_put_contents(substr($this->get('kernel')->getRootDir(), 0, -4).'/web/data/notification.json',$jsondata);


                //Creating indicateur for Dm
//
                if(intval($params->ft)==0) {
                    $em = $this->getDoctrine()->getManager();
                    foreach($subjects as $key => $subject){
                        //$libelle = str_replace('ê','e',str_replace('è','e',str_replace('é','e',$subject->getLibelle())));
                        $duree = $data[$subject->getId().''];
                        $indicateur = new NormeIndicateur();
                        $indicateur->setLibelle('Délai de traitement pour '.$subject->getLibelle());
                        $indicateur->setNormeVal($duree);
                        $indicateur->setJour($duree);
                        $indicateur->setNormeOp('<=');
                        $indicateur->setNormeUnite('jr');

                        $em->persist($indicateur);
                    }

                    //Creating indicateur for Pr
                    $indicateur2 = new NormeIndicateur();
                    $indicateur2->setLibelle('Période de recurrence');
                    $indicateur2->setNormeVal($periodeRecurrence);
                    $indicateur2->setJour($periodeRecurrence);
                    $indicateur2->setNormeOp('<=');
                    $indicateur2->setNormeUnite('jr');
                    //$em = $this->getDoctrine()->getManager();
                    $objectif2 = $em->getRepository('GplainteBundle:ObjectifIndicateur')->findOneBy(array("libelle" => "La periode d'observation des plaintes récurrentes"));
                    $indicateur2->setObjectifindicateur($objectif2);
                    //Creating indicateur for Sr
                    $indicateur3 = new NormeIndicateur();
                    $indicateur3->setLibelle('Seuil de recurrence');
                    $indicateur3->setNormeVal($seuilRecurrence);
                    $indicateur3->setJour($seuilRecurrence);
                    $indicateur3->setNormeOp('<=');
                    $indicateur3->setNormeUnite('pl');
                    //$em = $this->getDoctrine()->getManager();
                    $objectif3 = $em->getRepository('GplainteBundle:ObjectifIndicateur')->findOneBy(array("libelle" => "La Limite fixée pour les plaintes récurrentes"));
                    $indicateur3->setObjectifindicateur($objectif3);

                    //$em->persist($indicateur);
                    $em->persist($indicateur2);
                    $em->persist($indicateur3);
                    $em->flush();
                }
                else{
                    //TODO:SI les indicateurs ont déja été créés, les mettre à jour
                    $em = $this->getDoctrine()->getManager();
                    foreach($subjects as $key => $subject){

                        //$libelle = str_replace('ê','e',str_replace('è','e',str_replace('é','e',$subject->getLibelle())));
                        $duree = $data[$subject->getId().''];
                        $indicateur1 = $em->getRepository('GplainteBundle:NormeIndicateur')->findOneBy(array("libelle"=>'Délai de traitement pour '.$subject->getLibelle()));
                        $indicateur1->setNormeVal($duree);
                        $indicateur1->setNormeUnite('jr');

                        $em->persist($indicateur1);
                    }
                    $indicateur2 = $em->getRepository('GplainteBundle:NormeIndicateur')->findOneBy(array("libelle"=>"Période de recurrence"));
                    $indicateur3 = $em->getRepository('GplainteBundle:NormeIndicateur')->findOneBy(array("libelle"=>"Seuil de recurrence"));


                    $indicateur2->setNormeVal($periodeRecurrence);
                    $indicateur2->setNormeUnite('jr');

                    $indicateur3->setNormeVal($seuilRecurrence);
                    $indicateur3->setNormeUnite('pl');


                    $em->persist($indicateur2);
                    $em->persist($indicateur3);
                    $em->flush();


                }


                //var_dump( $dureeMax.' / '.$periodeRecurrence.' /
                $paramsDm = json_decode(json_encode($params->dm), true);


                $this->get('session')->getFlashBag()->add('feedback', '1');
                return $this->render('GPlainteNotificationBundle:Notification:config.html.twig', array('feedback'=> 1, 'subjects' => $subjects, 'params' => $params, 'flag' => $nullFlag, 'dms' => $paramsDm));

            }

            if(intval($params->ft)==1) {

                    $paramsDm = json_decode(json_encode($params->dm), true);
                    $keysDm = array_keys($paramsDm);
                    return $this->render('GPlainteNotificationBundle:Notification:config.html.twig', array('params' => $params, 'flag' => $nullFlag, 'subjects' => $subjects, 'dms' => $paramsDm, 'keys' => $keysDm));

            }
            else{
                $nullFlag = 1;
                return $this->render('GPlainteNotificationBundle:Notification:config.html.twig', array('params' => $params, 'flag' => $nullFlag, 'subjects' => $subjects));
            }
            //return $this->render('GPlainteNotificationBundle:Notification:notifications.html.twig', array('name' => $name));
        } else {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

    }

    public function replace_utf8($seq){
        return str_replace('ê','e',str_replace('è','e',str_replace('é','e',$seq)));
    }
}
