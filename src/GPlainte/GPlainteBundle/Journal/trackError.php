<?php
/**
 * Created by PhpStorm.
 * User: Maurice
 * Date: 14/08/2016
 * Time: 07:48
 */

namespace GPlainte\GPlainteBundle\Journal;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;



class trackError {

    protected $em;
    protected $request;
    protected $container;

    public function __construct(EntityManager $entityManager, Request $request, Container $container)
    {
        $this->em = $entityManager;
        $this->request=$request;
        $this->container=$container;
    }

    public function traceError($idEvent='', $msg,$line=''){

        $action=$this->em->getRepository('UserBundle:DataEvent')->find($idEvent);
        $path = substr($this->container->get('kernel')->getRootDir(), 0, -4) . '/src/GPlainte/GplainteBundle/Journal/';
        $filename = 'errors.json';
        $today = date("d m Y h:i:s");
//        $e= new Exception();
        $data=$today.'   '.$this->request->getMethod().'   '.$this->request->getRequestUri().'   '.$msg.'   '.$line.'   '.$action->getLibelle();
        $jsondata = json_encode(array("erreur"=>$data));
        file_put_contents($path . '' . $filename, $jsondata, FILE_APPEND | LOCK_EX);
    }

    public function trackEvent($idEvent){
        $user = $this->container->get('security.context')->getToken()->getUser();
        $action=$this->em->getRepository('UserBundle:DataEvent')->find($idEvent);
        $event= new SaveComplaintEvent($action,$user);
        $this->container->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);
    }

}