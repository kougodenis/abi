<?php
/**
 * Created by PhpStorm.
 * User: Maurice
 * Date: 13/08/2016
 * Time: 12:45
 */

namespace GPlainte\GPlainteBundle\Journal;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

use GPlainte\UserBundle\EventListener\UserConnected;



class UserListener {

    private $message;
    protected $userConnected;

    public function __construct($message,UserConnected $userConnected)
    {
        $this->message = $message;
        $this->userConnected = $userConnected;
    }


    public function onSaveComplaint(SaveComplaintEvent $event)
    {

        $this->userConnected->journalizeActivity($event->getMessage(),$event->getUser());

    }

}