<?php
/**
 * Created by PhpStorm.
 * User: Maurice
 * Date: 14/08/2016
 * Time: 07:48
 */

namespace GPlainte\GPlainteBundle\Journal;


final class ValuesEvent {
    const addPlainte="Enregistrement d'une plainte";
    const updatePlainte="Mise à jour plainte";
    const addDenonciation="Enregistrement d'une dénonciation";
    const listPLainte="Consultation des plaintes à affecter";
    const affecterPlainte="Affectation d'une plainte";
    const plainteEnAttenteTraitement="Consultation plaintes en attente de traitement";
    const plainteTraitement="Traitement des plaintes";
    const plainteEnAttenteValidation="Consultation plaintes en attentes de validation";
    const detailTraitement="Consultation detail plainte";
    const approuverTraitement="Approbation traitement d'une plainte";
    const desApprouverTraitement="désapprobation traitement d'une plainte";
    const listSolution="Consultation des plaintes à nofifier aux clients";
    const addNotification="Enregistrement d'une notification au client";
    const plaintesNotifier="Consultation des plaintes notifiées aux clients";
    const addSatisfaction="Enregistrement satisfaction client";
    const listSatisfaction="Consultation d' avis d'un client";
    const verifSatisfaction="Enregistrement vérification satisfaction du client";
    const listDenonciation="Consultation des dénonciations";
    const detailDenonciation="Consultation des details de dénonciation";
    const justifierDenonciation="Enregistrement de la justification d'une dénonciation";
    const suiviPlainte="Consultation de la page suivi des plaintes";
    const archivePlainte="Consultation des archives";
    const addFaq="Enregistrement des FAQ";
    const updateFaq="Modification d'une FAQ";
    const deleteFaq="Suppression d'une FAQ";
    const listFaq="Consultation des FAQs";

}