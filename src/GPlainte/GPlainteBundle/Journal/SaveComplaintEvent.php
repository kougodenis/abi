<?php
/**
 * Created by PhpStorm.
 * User: Maurice
 * Date: 12/08/2016
 * Time: 21:06
 */

namespace GPlainte\GPlainteBundle\Journal;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Security\Core\User\UserInterface;
use GPlainte\UserBundle\Entity\DataEvent;

class SaveComplaintEvent extends Event {


    protected $message;
    protected $user;
    protected $autorise;

    public function __construct(DataEvent $message, UserInterface $user){
            $this->message=$message;
            $this->user=$user;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }



    /**
     * @return mixed
     */
    public function getAutorise()
    {
        return $this->autorise;
    }

    /**
     * @param mixed $autorise
     */
    public function setAutorise($autorise)
    {
        $this->autorise = $autorise;
    }



}