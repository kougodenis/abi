<?php
/**
 * Created by PhpStorm.
 * User: Maurice
 * Date: 08/09/2016
 * Time: 16:06
 */

namespace GPlainte\GPlainteBundle\Journal;

use FOS\UserBundle\Model\UserManagerInterface;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;



class ExceptionListener
    {

        protected $container;

        public function __construct($container, UserManagerInterface $userManager)
        {
            $this->container=$container;
            $this->userManager = $userManager;
        }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();
        $message = sprintf(
            'My Error says: %s with code: %s',
            $exception->getMessage(),
            $exception->getCode()
        );

        // Customize your response object to display the exception details
        $response = new Response();
        $response->setContent("toto");

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
//        if ($exception instanceof HttpExceptionInterface) {
//            $response->setStatusCode($exception->getStatusCode());
//            $response->headers->replace($exception->getHeaders());
//        } else {
//            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
//        }

        // Send the modified response object to the event
        $event->setResponse($response);
    }

}