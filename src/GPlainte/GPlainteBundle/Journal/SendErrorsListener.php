<?php
/**
 * Created by PhpStorm.
 * User: Maurice
 * Date: 24/09/2016
 * Time: 18:03
 */

namespace GPlainte\GPlainteBundle\Journal;

use FOS\UserBundle\Model\UserManagerInterface;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class SendErrorsListener
 {
     // La date de fin de la version bŒta :
     // - Avant cette date , on affichera un compte à rebours (J-3 par exemple )
     // - AprŁs cette date , on n’affichera plus le  bŒta 
     protected $dateFin;

     public function __construct ( $dateFin )
            {
              $this->dateFin = new \Datetime ( $dateFin );
         }

 // MØthode pour ajouter le  bŒta  à une rØponse
     protected function displayBeta (Response $reponse , $joursRestant)
     {
         $content = $reponse->getContent();

         // Code à rajouter
         $html = '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fermer</span></button>

      </div>
      <div class="modal-body">

      </div>
    </div></div></div>';

         // Insertion du code dans la page , dans le <h1 > du header
//         $content = preg_replace ('#<h1 >(.*?) </h1 >#iU',
//         '<h1 >$1'.$html.'</h1 >',$content);
         $content = preg_replace('#<h1>(.*?)</h1>#iU',
             '<h1>$1'.$html.'</h1>',
             $content,
             1);

         // Modification du contenu dans la rØponse
         $reponse->setContent($html);
//var_dump($html); die();

         return $reponse;
     }

    public function onKernelResponse (FilterResponseEvent $event )
        {
         // On teste si la requŒte est bien la requŒte principale
             if ( HttpKernelInterface::MASTER_REQUEST !== $event ->getRequestType ()) {
                return ;
             }

         // On rØcupŁre la rØponse que le noyau a insØrØe dans l’événement
//            $response = $event->getResponse();

            $joursRestant = $this->dateFin->diff(new \ Datetime ())->days;


            if ( $joursRestant > 0) {
             // On utilise notre mØthode  reine 
            $response = $this->displayBeta($event->getResponse() , $joursRestant );
            }

//            echo $response; die();

             $event->setResponse($response);
        }
 }