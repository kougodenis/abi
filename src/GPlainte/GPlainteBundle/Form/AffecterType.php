<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AffecterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('dateAffectation','text')
//            ->add('recevable')
            ->add('service','entity', array(
                    'class' => 'GPlainte\GPlainteBundle\Entity\Service',
                    'property' => 'libelle',
                    'multiple'=>false,
                    'empty_value'=>'complaint.dept.select','translation_domain' => 'messages',))
//            ->add('plainte','entity', array(
//                    'class' => 'GPlainte\GPlainteBundle\Entity\Plainte',
//                    'property' => 'id',
//                    'multiple'=>false,
//                    'empty_value'=>"Sélectionner la plainte"))
//            ->add('action')
//            ->add('faq')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\Affecter'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_affecter';
    }
}
