<?php
namespace GPlainte\GPlainteBundle\Form;
use GPlainte\NotificationBundle\Entity\Notifs;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use GPlainte\GPlainteBundle\Entity\Plainte;
use GPlainte\GPlainteBundle\Entity\Agent;
use GPlainte\GPlainteBundle\Entity\Client;

class PlainteHandler
{
protected $form;
protected $request;
protected $em;
private $user;

public function __construct(Form $form, Request $request, EntityManager $em, Agent $user)
{
    $this->form = $form;
    $this->request = $request;
    $this->em = $em;
    $this->user=$user;
}
public function process()
{
   // if( $this->request->isXmlHttpRequest() )
   // {
    if( $this->request->getMethod() == 'POST' )
    {

            $this->form->handleRequest($this->request);
            if( $this->form->isValid() )
            {
                $this->onSuccess($this->form->getData());
                return true;
            }
   }
    return false;
    //} 
}

public function onSuccess(Plainte $plainte)
{
    $plainte->setAffection(0);
    $plainte->setTraitement(0);
    $plainte->setAgent($this->user);
    $this->em->persist($plainte);

    $this->em->flush();
}
}
