<?php
namespace GPlainte\GPlainteBundle\Form;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use GPlainte\GPlainteBundle\Entity\Affecter;
use GPlainte\GPlainteBundle\Entity\Agent;

class AffecterHandler2
{
protected $form;
protected $request;
protected $em;
    protected $user;

public function __construct(Form $form, Request $request, EntityManager $em, Agent $user)
{
    $this->form = $form;
    $this->request = $request;
    $this->em = $em;
    $this->user=$user;
}
public function process()
{
   // if( $this->request->isXmlHttpRequest() )
   // {
    if( $this->request->getMethod() == 'POST' )
    {
            $this->form->handleRequest($this->request);
            if( $this->form->isValid() )
            {

                $this->onSuccess($this->form->getData());
                return true;
            }
   }
    return false;
    //} 
}

public function onSuccess(Affecter $Affecter)
{
//    $Affecter->setPlainte($Affecter->getPlainte()->getId());
    $Affecter->getAction()->setDateAction(new \DateTime());
    $Affecter->getAction()->setNotification(0);
    $Affecter->setApprobation(0);

//    $this->em->get
    $Affecter->setAgent($this->user);
    $this->em->persist($Affecter);
    $this->em->flush();
}
}
