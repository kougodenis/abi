<?php
namespace GPlainte\GPlainteBundle\Form;
use GPlainte\NotificationBundle\Entity\Notifs;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use GPlainte\GPlainteBundle\Entity\Denonciation;
use GPlainte\GPlainteBundle\Entity\Agent;

class DenonciationHandler
{
protected $form;
protected $request;
protected $em;
private $user;

public function __construct(Form $form, Request $request, EntityManager $em, Agent $user)
{
    $this->form = $form;
    $this->request = $request;
    $this->em = $em;
    $this->user=$user;
}
public function process()
{
   // if( $this->request->isXmlHttpRequest() )
   // {
    if( $this->request->getMethod() == 'POST' )
    {
            $this->form->handleRequest($this->request);
            if( $this->form->isValid() )
            {
                $this->onSuccess($this->form->getData());
                return true;
            }
   }
    return false;
    //} 
}

public function onSuccess(Denonciation $plainte)
{

    $plainte->setAgent($this->user);
    $plainte->setEtatclient(false);
    $this->em->persist($plainte);

    $this->em->flush();
}
}
