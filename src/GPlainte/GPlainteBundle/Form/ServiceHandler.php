<?php
namespace GPlainte\GPlainteBundle\Form;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use GPlainte\GPlainteBundle\Entity\Service;
use GPlainte\GPlainteBundle\Entity\Institution;

class ServiceHandler
{
protected $form;
protected $request;
protected $em;
protected $inst;

public function __construct(Form $form, Request $request, EntityManager $em, Institution $inst)
{
    $this->form = $form;
    $this->request = $request;
    $this->em = $em;
    $this->inst=$inst;
}
public function process()
{
   // if( $this->request->isXmlHttpRequest() )
   // {
    if( $this->request->getMethod() == 'POST' )
    {
            $this->form->handleRequest($this->request);
            if( $this->form->isValid() )
            {
                $this->onSuccess($this->form->getData());
                return true;
            }
   }
    return false;
    //} 
}

public function onSuccess(Service $service)
{
    $institution=$this->inst;
    $service->setInstitution($institution);
    $this->em->persist($service);
    $this->em->flush();
}
}
