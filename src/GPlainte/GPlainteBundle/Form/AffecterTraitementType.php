<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use GPlainte\GPlainteBundle\Entity\AgentRepository;

class AffecterTraitementType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('dateAffectation','date',array('widget' =>'single_text','format' => 'dd-MM-yyyy'))
//            ->add('recevable','choice',array('choices'=>array('O'=>'Oui','N'=>'Non'),'empty_value'=>"Sélectionner la recevabilité"))
            ->add('service','entity', array(
                    'class' => 'GPlainte\GPlainteBundle\Entity\Service',
                    'property' => 'libelle',
                    'multiple'=>false,
                    'empty_value'=>"Sélectionner le service"))
            ->add('plainte','entity', array(
                    'class' => 'GPlainte\GPlainteBundle\Entity\Plainte',

                    'multiple'=>false,
//                'query_builder'=>function(PlainteRepository $er){
//                    return $er->getListeObjetPlainte();
//                },
                'property' => 'id',
                'empty_value'=>"Sélectionner la plainte"))
            ->add('action', new ActionType())
//            ->add('agent','entity', array(
//                'class' => 'GPlainte\GPlainteBundle\Entity\Agent',
//                'multiple'=>false,
//                 'property' => 'nomprenom',
//                'empty_value'=>"Sélectionner votre nom"))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\Affecter'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_affecter2';
    }
}
