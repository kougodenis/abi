<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NormeIndicateurType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle')
            ->add('normeVal')
            ->add('jour')
            ->add('objectifindicateur','entity',array(
                'class' => 'GPlainte\GPlainteBundle\Entity\ObjectifIndicateur',
                'property' => 'libelle',
                'multiple'=>false,
                'empty_value'=>"objective.select",
                'translation_domain' => 'messages',
                'required'=>false
            ))
            ->add('normeUnite')
            ->add('normeOp')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\NormeIndicateur'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_normeindicateur';
    }
}
