<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AgentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('fonction','entity', array(
            'class' => 'GPlainte\GPlainteBundle\Entity\Fonction',
            'property' => 'libelle',
            'multiple'=>false,
            'empty_value'=>"position.select",'translation_domain'=>'messages'))
            ->add('sexe','choice',array('choices'=>array('M'=>'Masculin','F'=>'Féminin'),
                'empty_value'=>'sex.select','translation_domain'=>'messages'))
            ->add('telephone')
            ->add('file','file',array('required'=>false))
            ->add('email')
            ->add('service',
                'entity', array(
                    'class' => 'GPlainte\GPlainteBundle\Entity\Service',
                    'property' => 'libelle',
                    'multiple'=>false,
                    'empty_value'=>"dept.select",'translation_domain'=>'messages'))
            ->add('chef','checkbox',array('required'=>false))
        ->add('dg','checkbox',array('required'=>false))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\Agent'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_agent';
    }
}
