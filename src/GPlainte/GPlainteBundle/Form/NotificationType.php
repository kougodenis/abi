<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use GPlainte\GPlainteBundle\Entity\ActionRepository;

class NotificationType extends AbstractType
{
//    private $action;
//    public function __construct($action){
//        $this->action=$action;
//
//    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $idb=$this->action;
        $builder
            ->add('date','date',array('widget' =>'single_text','format' => 'dd-MM-yyyy'))
            ->add('moyencommunication','choice',array('choices'=>array('appel'=>'Appel téléphonique','sms'=>'SMS','email'=>'E-mail','courrier'=>'Courrier'),
        'empty_value'=>'notification.means.select','translation_domain' => 'messages',))

//            ->add('action','entity', array(
//                'class' => 'GPlainte\GPlainteBundle\Entity\Action',
//                'property' => 'id',
//                'multiple'=>false,
//                'query_builder'=>function(ActionRepository $er) use ($idb){
//                    return $er->getAction($idb);
//                },
//               ))

            ->add('agent','entity', array(
                'class' => 'GPlainte\GPlainteBundle\Entity\Agent',
                'property' => 'nomprenom',
                'multiple'=>false,
                'empty_value'=>'notification.agent.select','translation_domain' => 'messages',
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\Notification'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_notification';
    }
}
