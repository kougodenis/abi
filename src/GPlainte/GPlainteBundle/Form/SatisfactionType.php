<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
class SatisfactionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle','choice',array('choices'=>array('O'=>'satisfaction.oui','N'=>'satisfaction.non'),
                'empty_value'=>"satisfaction.appreciation.select",'translation_domain' => 'messages',))
            ->add('rdv','date',array('widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                'required'=>false)
                )
            ->add('reformulation','textarea',array('required'=>false))
            ->add('observation','textarea',array('required'=>false))
            ->add('mention', 'checkbox', array(
                'label'    => 'défaut d\'information?',
                'required' => false,'translation_domain' => 'messages',
            ));
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\Satisfaction'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_satisfaction';
    }
}
