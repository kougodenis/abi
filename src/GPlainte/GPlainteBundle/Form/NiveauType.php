<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NiveauType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle')
            ->add('typeniveau',
                'entity', array(
                    'class' => 'GPlainte\GPlainteBundle\Entity\typeNiveau',
                    'property' => 'libelle',
                    'multiple'=>false,
                    'empty_value'=>"level.select",'translation_domain'=>'messages'
                )
            )
            ->add('choix',
                'entity', array(
                    'class' => 'GPlainte\GPlainteBundle\Entity\Choix',
                    'property' => 'libelle',
                    'multiple'=>false,
                    'empty_value'=>"complaint.type.select",'translation_domain'=>'messages'
                )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\Niveau'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_niveau';
    }
}
