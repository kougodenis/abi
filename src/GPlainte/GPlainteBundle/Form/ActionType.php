<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ActionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('solution')
            ->add('prevention')
            ->add('observation','textarea',array('required'=>false))
//            ->add('dateAction','date',array('widget' =>'single_text','format' => 'dd-MM-yyyy'))

//            ->add('client','entity', array(
//                    'class' => 'GPlainte\GPlainteBundle\Entity\Client',
//                    'property' => 'nom',
//                    'multiple'=>false,
//                    'empty_value'=>"Sélectionner le client"))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\Action'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_action';
    }
}
