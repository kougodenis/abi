<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DenonciationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('description')
            ->add('produit','entity', array(
                'class' => 'GPlainte\GPlainteBundle\Entity\Produit',
                'property' => 'libelle',
                'multiple'=>false,
                'empty_value'=>"complaint.product.select",
                'required'=>false,'translation_domain' => 'messages',))
            ->add('niveau',
                'entity', array(
                    'class' => 'GPlainte\GPlainteBundle\Entity\Niveau',
                    'property' => 'libelle',
                    'multiple'=>false,
                    'empty_value'=>"complaint.subject.select",'translation_domain' => 'messages',))
//            ->add('service')
            ->add('bureau',
                'entity', array(
                    'class' => 'GPlainte\GPlainteBundle\Entity\Bureau',
                    'property' => 'libelle',
                    'multiple'=>false,
                    'empty_value'=>"complaint.office.select",'translation_domain' => 'messages',
                    'required'=>false))
            ->add('autres',new AutresType(),array('required'=>false))
            ->add('choix','entity', array(
                'class' => 'GPlainte\GPlainteBundle\Entity\Choix',
                'property' => 'libelle',
                'multiple'=>false,
                'empty_value'=>'complaint.type.select',
                'required'=>false,
                'translation_domain' => 'messages',
            ))

            ->add('autreobjet','textarea',array('required'=>false))
            ->add('mobile','checkbox',array('required'=>false))
            ->add('codeplaintemobile','textarea',array('required'=>false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\Denonciation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_denonciation';
    }
}
