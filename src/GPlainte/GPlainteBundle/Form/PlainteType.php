<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PlainteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('choix','choice',array('choices'=>array('Service'=>'Service','Produit'=>'Produit','Agent'=>'Agent','Autres'=>'Autres'),
//                'empty_value'=>'Sélectionner le type de plainte'))
            ->add('choix','entity', array(
                'class' => 'GPlainte\GPlainteBundle\Entity\Choix',
                'property' => 'libelle',
                'multiple'=>false,
                'empty_value'=>'complaint.type.select',
                'required'=>false,
                'translation_domain' => 'messages',
                ))

                ->add('telephone','textarea',array('required'=>false))
            ->add('numdossier','textarea',array('required'=>false))
            ->add('description','textarea',array('required'=>false))
//            ->add('agent',new AgentType2(),array('required'=>false))
//            ->add('agent','entity', array(
//                'class' => 'GPlainte\GPlainteBundle\Entity\Agent',
//                'property' => 'nomprenom',
//                'multiple'=>false,
//                'empty_value'=>"Sélectionner l'agent",
//                'required'=>false))
//            ->add('produit',new ProduitType(),array('required'=>false))
            ->add('produit','entity', array(
                'class' => 'GPlainte\GPlainteBundle\Entity\Produit',
                'property' => 'libelle',
                'multiple'=>false,
                'empty_value'=>"complaint.product.select",
                'required'=>false,'translation_domain' => 'messages',))
//            ->add('client',new ClientType())
            ->add('client','entity', array(
                'class' => 'GPlainte\GPlainteBundle\Entity\Client',
                'property' => 'nomprenom',
                'multiple'=>false,
                'empty_value'=>"complaint.client.select",
                'required'=>false,'translation_domain' => 'messages',))
            ->add('niveau',
                'entity', array(
                    'class' => 'GPlainte\GPlainteBundle\Entity\Niveau',
                    'property' => 'libelle',
                    'multiple'=>false,
                    'empty_value'=>"complaint.subject.select",'translation_domain' => 'messages',))
//            ->add('service',
//                'entity', array(
//                    'class' => 'GPlainte\GPlainteBundle\Entity\Service',
//                    'property' => 'libelle',
//                    'multiple'=>false,
//                    'empty_value'=>"Sélectionner le service",
//                    'required'=>false))
            ->add('bureau',
                'entity', array(
                    'class' => 'GPlainte\GPlainteBundle\Entity\Bureau',
                    'property' => 'libelle',
                    'multiple'=>false,
                    'empty_value'=>"complaint.office.select",'translation_domain' => 'messages',
                    'required'=>false))
            ->add('autres',new AutresType(),array('required'=>false))
            ->add('autreobjet','textarea',array('required'=>false))
            ->add('mobile','checkbox',array('required'=>false))
            ->add('codeplaintemobile','textarea',array('required'=>false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\Plainte'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_plainte';
    }
}
