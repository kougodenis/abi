<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BureauType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle')
            ->add('agence',
                'entity', array(
                    'class' => 'GPlainte\GPlainteBundle\Entity\Agence',
                    'property' => 'libelle',
                    'multiple'=>false,
                    'empty_value'=>"agency.select",'translation_domain'=>'messages'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\Bureau'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_bureau';
    }
}
