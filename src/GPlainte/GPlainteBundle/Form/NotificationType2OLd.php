<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use GPlainte\GPlainteBundle\Entity\ActionRepository;

class NotificationType2 extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
//            ->add('date')
            ->add('moyencommunication','choice',array('choices'=>array('appel'=>'Appel téléphonique','sms'=>'SMS','email'=>'E-mail','courrier'=>'Courrier'),
        'empty_value'=>'Sélectionner le moyen de communication'))


            ->add('agent','entity', array(
                'class' => 'GPlainte\GPlainteBundle\Entity\Agent',
                'property' => 'nomprenom',
                'multiple'=>false,
                'empty_value'=>"Sélectionner l'agent",
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\Notification'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_notification2';
    }
}
