<?php
namespace GPlainte\GPlainteBundle\Form;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use GPlainte\GPlainteBundle\Entity\Notification;

class NotificationHandler
{
    protected $form;
    protected $request;
    protected $em;

    public function __construct(Form $form, Request $request, EntityManager $em)
    {
        $this->form = $form;
        $this->request = $request;
        $this->em = $em;
    }
    public function process()
    {
        // if( $this->request->isXmlHttpRequest() )
        // {
        if( $this->request->getMethod() == 'POST' )
        {
            $this->form->handleRequest($this->request);
            if( $this->form->isValid() )
            {
                $this->onSuccess($this->form->getData());
                return true;
            }
        }
        return false;
        //}
    }

    public function onSuccess(Notification $Notification)
    {
//        $Notification->getAction()->setNotification(1);
//        $Notification->setAction()
        $this->em->persist($Notification);
        $this->em->flush();
    }
}
