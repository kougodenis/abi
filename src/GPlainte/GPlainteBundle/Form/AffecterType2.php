<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use GPlainte\GPlainteBundle\Entity\AgentRepository;

class AffecterType2 extends AbstractType
{
    private $idservice;

    public function __construct($service=0){
        $this->idservice=$service;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $idserviceV=$this->idservice;
        $builder
            ->add('dateAffectation','date',array('widget' =>'single_text','format' => 'dd-MM-yyyy'))
//            ->add('recevable','choice',array('choices'=>array('O'=>'Oui','N'=>'Non'),'empty_value'=>"Sélectionner la recevabilité"))
            ->add('service','entity', array(
                    'class' => 'GPlainte\GPlainteBundle\Entity\Service',
                    'property' => 'libelle',
                    'multiple'=>false,
                    'empty_value'=>'complaint.dept.select','translation_domain' => 'messages',))
            ->add('plainte','entity', array(
                    'class' => 'GPlainte\GPlainteBundle\Entity\Plainte',

                    'multiple'=>false,
//                'query_builder'=>function(PlainteRepository $er){
//                    return $er->getListeObjetPlainte();
//                },
                'property' => 'id',
                'empty_value'=>'complaint.select','translation_domain' => 'messages',))
            ->add('action', new ActionType())
//            ->add('agent','entity', array(
//                'class' => 'GPlainte\GPlainteBundle\Entity\Agent',
//                'multiple'=>false,
//                 'property' => 'nomprenom',
//                'query_builder'=>function(AgentRepository $er) use ($idserviceV){
//                    return $er->getListeAgent($idserviceV);
//                },
//                'empty_value'=>"Sélectionner votre nom"))
//            ->add('agent', new AgentType())
//            ->add('faq')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\Affecter'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_affecter2';
    }
}
