<?php

namespace GPlainte\GPlainteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClientType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('raisonSociale')
            ->add('email','email',array('required'=>false))
            ->add('numDossier')
            ->add('quartier')
            ->add('telephone1')
        ->add('ville')
            ->add('sexe','choice',array('choices'=>array('Masculin'=>'Masculin','Feminin'=>'Féminin'),
                'empty_value'=>'sex.select','translation_domain'=>'messages'))
        //    ->add('telephone2')
//            ->add('satisfaction',
//                'entity', array(
//                    'class' => 'GPlainte\GPlainteBundle\Entity\Satisfaction',
//                    'property' => 'libelle',
//                    'multiple'=>false,
//                    'empty_value'=>"Sélectionner la satisfaction"))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\GPlainteBundle\Entity\Client'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_gplaintebundle_client';
    }
}
