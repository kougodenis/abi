<?php

namespace GPlainte\GPlainteBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientControllerTest extends WebTestCase
{
    public function testShowclient()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/showClient');
    }

    public function testAddclient()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/addClient');
    }

    public function testDeleteclient()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/deleteClient');
    }

}
