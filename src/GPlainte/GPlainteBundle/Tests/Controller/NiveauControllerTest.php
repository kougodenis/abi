<?php

namespace GPlainte\GPlainteBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NiveauControllerTest extends WebTestCase
{
    public function testAddniveau()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/addNiveau');
    }

    public function testShowniveau()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/showNiveau');
    }

    public function testDeleteniveau()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/deleteNiveau');
    }

}
