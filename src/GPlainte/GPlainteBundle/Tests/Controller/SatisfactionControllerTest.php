<?php

namespace GPlainte\GPlainteBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SatisfactionControllerTest extends WebTestCase
{
    public function testShowsatisfaction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/showSatisfaction');
    }

    public function testAddsatisfaction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/addSatisfaction');
    }

    public function testDeletesatisfaction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/deleteSatisfaction');
    }

}
