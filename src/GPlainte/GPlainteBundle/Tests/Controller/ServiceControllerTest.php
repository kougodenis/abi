<?php

namespace GPlainte\GPlainteBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServiceControllerTest extends WebTestCase
{
    public function testShowservice()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/showService');
    }

    public function testAddservice()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/addService');
    }

    public function testDeleteservice()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/deleteService');
    }

}
