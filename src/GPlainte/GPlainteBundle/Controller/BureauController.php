<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GPlainte\GPlainteBundle\Entity\Bureau;
use GPlainte\GPlainteBundle\Form\BureauType;
use GPlainte\GPlainteBundle\Form\BureauHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

class BureauController extends Controller
{

    public function addBureauAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Bureau');
            $user = $this->container->get('security.context')->getToken()->getUser();

            $listBureau = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            if ($val=="afficherForm"){
                $Bureau=$repository->find($id);
                $form = $this->createForm(new BureauType(), $Bureau);
                $val="modifier";

            }else
                if ($val=="modifier"){} elseif ($val=="supprimer"){
                    $Bureau = new Bureau();
                    $form = $this->createForm(new BureauType(), $Bureau);
                }else{
                    $Bureau = new Bureau();
                    $form = $this->createForm(new BureauType(), $Bureau);
                    $val="ajouter";

                }
            $entiteSuppr = $em->getRepository('GplainteBundle:Bureau')->find($id);

            if ($val=="ajouter"){
                $Bureau = new Bureau();
                $form = $this->createForm(new BureauType(), $Bureau);
                $formHandler = new BureauHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $libelle=$data['libelle'];
                $recup=  $em->getRepository('GplainteBundle:Bureau')->findByLibelle($libelle);

                if (count($recup)== 0){

                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:Bureau');
                        $listBureau = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(38);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Bureau:addBureau.html.twig',array('Bureau' => $form->createView(),'msg'=>1,
                            'list'=>$listBureau,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));

                    }

                } else {
                    $repository = $em->getRepository('GplainteBundle:Bureau');
                    $listBureau = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Bureau:addBureau.html.twig',array('Bureau' => $form->createView(),'msgErr'=>1,
                        'list'=>$listBureau,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }


            if ($val=="modifier" && $id!=0){

                if( !$Bureau = $em->getRepository('GplainteBundle:Bureau')->find($id) )
                {
                    $repository = $em->getRepository('GplainteBundle:Bureau');
                    $listBureau = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Bureau:addBureau.html.twig',array('Bureau' => $form->createView(),'msgErr'=>"Cette Bureau n'existe pas",
                        'list'=>$listBureau,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

                $form = $this->createForm(new BureauType(), $Bureau);
                $formHandler = new BureauHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                if($formHandler->process())
                {
                    $repository = $em->getRepository('GplainteBundle:Bureau');
                    $listBureau = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(37);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->render('GplainteBundle:Bureau:addBureau.html.twig',array('Bureau' => $form->createView(),'msg'=>2,
                        'list'=>$listBureau,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }

            $repository = $em->getRepository('GplainteBundle:Bureau');
            $listBureau = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(36);
            $event= new SaveComplaintEvent($action,$user);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:Bureau:addBureau.html.twig',array('Bureau' => $form->createView(),

                'list'=>$listBureau,
                'val'=>$val,
                'id'=>$id,
                'suppr'=>$entiteSuppr
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


    public function showNiveauAction()
    {
        return array(
                // ...
            );
    }


    public function deleteBureauAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GplainteBundle:Bureau')->find($id);
            $user = $this->container->get('security.context')->getToken()->getUser();

            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_Bureau',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();
                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(39);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);
                    return $this->redirect($this->generateUrl('gplainte_add_Bureau',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){

                return $this->redirect($this->generateUrl('gplainte_add_Bureau',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:Bureau');
            $listBureau = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $Bureau = new Bureau();
            $form = $this->createForm(new BureauType(), $Bureau);

            return $this->render('GplainteBundle:Bureau:addBureau.html.twig', array(
                'Bureau' => $form->createView(),
                'list'=>$listBureau,
                'val'=>$val,
                'id'=>$id
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }


    }


}
