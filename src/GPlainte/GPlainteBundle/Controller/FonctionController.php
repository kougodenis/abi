<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GPlainte\GPlainteBundle\Entity\Fonction;
use GPlainte\GPlainteBundle\Form\FonctionType;
use GPlainte\GPlainteBundle\Form\FonctionHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

class FonctionController extends Controller
{

    public function addFonctionAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
                $em=$this->getDoctrine()->getManager();
                $repository = $em->getRepository('GplainteBundle:Fonction');
                $user = $this->container->get('security.context')->getToken()->getUser();

                $listFonction = $repository->findBy(
                    array(),
                    array('id'=>'DESC')
                );

                if ($val=="afficherForm"){
                    $fonction=$repository->find($id);
                    $form = $this->createForm(new FonctionType(), $fonction);
                    $val="modifier";

                }else
                    if ($val=="modifier"){} elseif ($val=="supprimer"){
                        $fonction = new Fonction();
                        $form = $this->createForm(new FonctionType(), $fonction);
                    }else{
                        $fonction = new Fonction();
                        $form = $this->createForm(new FonctionType(), $fonction);
                        $val="ajouter";

                    }
                $entiteSuppr = $em->getRepository('GplainteBundle:Fonction')->find($id);

                if ($val=="ajouter"){
                    $fonction = new Fonction();
                    $form = $this->createForm(new FonctionType(), $fonction);
                    $formHandler = new FonctionHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                    $request=  $this->getRequest();
                    $data=$request->request->get($form->getName());
                    $libelle=$data['libelle'];
                    $recup=  $em->getRepository('GplainteBundle:Fonction')->findByLibelle($libelle);

                    if (count($recup)== 0){

                        if($formHandler->process())
                        {
                            $repository = $em->getRepository('GplainteBundle:Fonction');
                            $listFonction = $repository->findBy(
                                array(),
                                array('id'=>'DESC')
                            );

                            $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(44);
                            $event= new SaveComplaintEvent($action,$user);
                            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                            return $this->render('GplainteBundle:Fonction:addFonction.html.twig',array('fonction' => $form->createView(),'msg'=>1,
                                'list'=>$listFonction,
                                'val'=>$val,
                                'id'=>$id,
                                'suppr'=>$entiteSuppr
                            ));

                        }

                    } else {
                        $repository = $em->getRepository('GplainteBundle:Fonction');
                        $listFonction = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );
                        return $this->render('GplainteBundle:Fonction:addFonction.html.twig',array('fonction' => $form->createView(),'msgErr'=>1,
                            'list'=>$listFonction,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }
                }


                if ($val=="modifier" && $id!=0){

                    if( !$fonction = $em->getRepository('GplainteBundle:Fonction')->find($id) )
                    {
                        $repository = $em->getRepository('GplainteBundle:Fonction');
                        $listfonction = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );
                        return $this->render('GplainteBundle:Fonction:addFonction.html.twig',array('fonction' => $form->createView(),'msgErr'=>"Cette fonction n'existe pas",
                            'list'=>$listfonction,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }

                    $form = $this->createForm(new FonctionType(), $fonction);
                    $formHandler = new FonctionHandler($form, $this->get('request'), $this->getDoctrine()->getManager());

                    $request=  $this->getRequest();
                    $data=$request->request->get($form->getName());
                    $libelle=$data['libelle'];
//                    $recup=  $em->getRepository('GplainteBundle:Fonction')->findByLibelle($libelle);
//
//                    if (count($recup)== 0){
                        if($formHandler->process())
                        {
                            $repository = $em->getRepository('GplainteBundle:Fonction');
                            $listfonction = $repository->findBy(
                                array(),
                                array('id'=>'DESC')
                            );

                            $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(45);
                            $event= new SaveComplaintEvent($action,$user);
                            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                            return $this->render('GplainteBundle:Fonction:addFonction.html.twig',array('fonction' => $form->createView(),'msg'=>2,
                                'list'=>$listfonction,
                                'val'=>$val,
                                'id'=>$id,
                                'suppr'=>$entiteSuppr
                            ));
                        }
//                    }else {
//                        $repository = $em->getRepository('GplainteBundle:Fonction');
//                        $listFonction = $repository->findBy(
//                            array(),
//                            array('id'=>'DESC')
//                        );
//                        return $this->render('GplainteBundle:Fonction:addFonction.html.twig',array('fonction' => $form->createView(),'msgErr'=>1,
//                            'list'=>$listFonction,
//                            'val'=>$val,
//                            'id'=>$id,
//                            'suppr'=>$entiteSuppr
//                        ));
//                    }

                }

                $repository = $em->getRepository('GplainteBundle:Fonction');
                $listfonction = $repository->findBy(
                    array(),
                    array('id'=>'DESC')
                );

                $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(47);
                $event= new SaveComplaintEvent($action,$user);
                $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                return $this->render('GplainteBundle:Fonction:addFonction.html.twig',array('fonction' => $form->createView(),

                    'list'=>$listfonction,
                    'val'=>$val,
                    'id'=>$id,
                    'suppr'=>$entiteSuppr
                ));
            }
            else{
                return $this->redirect( $this->generateUrl('fos_user_security_login'));
            }

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


    public function showNiveauAction()
    {
        return array(
                // ...
            );
    }


    public function deleteFonctionAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em = $this->getDoctrine()->getManager();
//            echo $id; die();

            $entity = $em->getRepository('GplainteBundle:Fonction')->find($id);
            $user = $this->container->get('security.context')->getToken()->getUser();

            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_fonction',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(46);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->redirect($this->generateUrl('gplainte_add_fonction',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){

                return $this->redirect($this->generateUrl('gplainte_add_fonction',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:Fonction');
            $listfonction = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $fonction = new Fonction();
            $form = $this->createForm(new FonctionType(), $fonction);

            return $this->render('GplainteBundle:Fonction:addFonction.html.twig', array(
                'fonction' => $form->createView(),
                'list'=>$listfonction,
                'val'=>$val,
                'id'=>$id
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


}
