<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GPlainte\GPlainteBundle\Entity\Niveau;
use GPlainte\GPlainteBundle\Entity\typeNiveau;
use GPlainte\GPlainteBundle\Form\typeNiveauType;

use GPlainte\GPlainteBundle\Form\NiveauType;
use GPlainte\GPlainteBundle\Form\NiveauHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

class NiveauController extends Controller
{

    public function addNiveauAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Niveau');
            $user = $this->container->get('security.context')->getToken()->getUser();

            $listNiveau = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            if ($val=="afficherForm"){
                $niveau=$repository->find($id);
                $form = $this->createForm(new NiveauType(), $niveau);
                $val="modifier";

                $typeniveau = new typeNiveau();
                $form2 = $this->createForm(new typeNiveauType(), $typeniveau);
                $val2="ajouter";

            }else
                if ($val=="modifier"){
                    $typeniveau = new typeNiveau();
                    $form2 = $this->createForm(new typeNiveauType(), $typeniveau);
                    $val2="ajouter";
                } elseif ($val=="supprimer"){
                    $niveau = new Niveau();
                    $form = $this->createForm(new NiveauType(), $niveau);
                    $typeniveau = new typeNiveau();
                    $form2 = $this->createForm(new typeNiveauType(), $typeniveau);
                    $val2="ajouter";
                }else{
                    $niveau = new Niveau();
                    $form = $this->createForm(new NiveauType(), $niveau);
                    $val="ajouter";
                    $typeniveau = new typeNiveau();
                    $form2 = $this->createForm(new typeNiveauType(), $typeniveau);
                    $val2="ajouter";
                }
            $entiteSuppr = $em->getRepository('GplainteBundle:Niveau')->find($id);

            if ($val=="ajouter"){
                $Niveau = new Niveau();
                $form = $this->createForm(new NiveauType(), $Niveau);
                $formHandler = new NiveauHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $libelle=$data['libelle'];
                $recup=  $em->getRepository('GplainteBundle:Niveau')->findByLibelle($libelle);

                if (count($recup)== 0){

                    if($formHandler->process())
                    {



                        $repository = $em->getRepository('GplainteBundle:Niveau');
                        $listNiveau = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(64);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Niveau:addNiveau.html.twig',array('niveau' => $form->createView(),'msg'=>1,
                            'list'=>$listNiveau,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));

                    }

                } else {
                    $repository = $em->getRepository('GplainteBundle:Niveau');
                    $listNiveau = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Niveau:addNiveau.html.twig',array('niveau' => $form->createView(),'msgErr'=>1,
                        'list'=>$listNiveau,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }


            if ($val=="modifier" && $id!=0){

                if( !$niveau = $em->getRepository('GplainteBundle:Niveau')->find($id) )
                {
                    $repository = $em->getRepository('GplainteBundle:Niveau');
                    $listniveau = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Niveau:addNiveau.html.twig',array('niveau' => $form->createView(),'msgErr'=>"Cette FAQ n'existe pas",
                        'list'=>$listniveau,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

                $form = $this->createForm(new NiveauType(), $niveau);
                $formHandler = new NiveauHandler($form, $this->get('request'), $this->getDoctrine()->getManager());

                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $libelle=$data['libelle'];
                $recup=  $em->getRepository('GplainteBundle:Niveau')->findByLibelle($libelle);

                if (count($recup)== 0){

                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:Niveau');
                        $listniveau = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(65);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Niveau:addNiveau.html.twig',array('niveau' => $form->createView(),'msg'=>2,
                            'list'=>$listniveau,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }
                }else {
                    $repository = $em->getRepository('GplainteBundle:Niveau');
                    $listNiveau = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Niveau:addNiveau.html.twig',array('niveau' => $form->createView(),'msgErr'=>1,
                        'list'=>$listNiveau,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }

            $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(67);
            $event= new SaveComplaintEvent($action,$user);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:Niveau:addNiveau.html.twig',array('niveau' => $form->createView(),
                'typeniveau' => $form2->createView(),
                'val2'=>$val2,
                'list'=>$listNiveau,
                'val'=>$val,
                'id'=>$id,
                'suppr'=>$entiteSuppr
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


    public function showNiveauAction()
    {
        return array(
                // ...
            );
    }


    public function deleteNiveauAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GplainteBundle:Niveau')->find($id);
            $user = $this->container->get('security.context')->getToken()->getUser();


            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_niveau',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(66);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->redirect($this->generateUrl('gplainte_add_niveau',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){

                return $this->redirect($this->generateUrl('gplainte_add_niveau',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:Niveau');
            $listniveau = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $niveau = new Niveau();
            $form = $this->createForm(new NiveauType(), $niveau);

            return $this->render('GplainteBundle:Niveau:addNiveau.html.twig', array(
                'niveau' => $form->createView(),
                'list'=>$listniveau,
                'val'=>$val,
                'id'=>$id
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


}
