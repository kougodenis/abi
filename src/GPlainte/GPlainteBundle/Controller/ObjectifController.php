<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GPlainte\GPlainteBundle\Entity\ObjectifIndicateur;
use GPlainte\GPlainteBundle\Form\ObjectifIndicateurType;
use GPlainte\GPlainteBundle\Form\ObjectifIndicateurHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

class ObjectifController extends Controller
{

    public function addObjectifAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $user = $this->container->get('security.context')->getToken()->getUser();

            $repository = $em->getRepository('GplainteBundle:ObjectifIndicateur');

            $listObjectifIndicateur = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            if ($val=="afficherForm"){
                $ObjectifIndicateur=$repository->find($id);
                $form = $this->createForm(new ObjectifIndicateurType(), $ObjectifIndicateur);
                $val="modifier";

            }else
                if ($val=="modifier"){} elseif ($val=="supprimer"){
                    $ObjectifIndicateur = new ObjectifIndicateur();
                    $form = $this->createForm(new ObjectifIndicateurType(), $ObjectifIndicateur);
                }else{
                    $ObjectifIndicateur = new ObjectifIndicateur();
                    $form = $this->createForm(new ObjectifIndicateurType(), $ObjectifIndicateur);
                    $val="ajouter";

                }
            $entiteSuppr = $em->getRepository('GplainteBundle:ObjectifIndicateur')->find($id);

            if ($val=="ajouter"){
                $ObjectifIndicateur = new ObjectifIndicateur();
                $form = $this->createForm(new ObjectifIndicateurType(), $ObjectifIndicateur);
                $formHandler = new ObjectifIndicateurHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $libelle=$data['libelle'];
                $recup=  $em->getRepository('GplainteBundle:ObjectifIndicateur')->findByLibelle($libelle);

                if (count($recup)== 0){

                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:ObjectifIndicateur');
                        $listObjectifIndicateur = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(72);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:ObjectifIndicateur:addObjectifIndicateur.html.twig',array('ObjectifIndicateur' => $form->createView(),'msg'=>1,
                            'list'=>$listObjectifIndicateur,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));

                    }

                } else {
                    $repository = $em->getRepository('GplainteBundle:ObjectifIndicateur');
                    $listObjectifIndicateur = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:ObjectifIndicateur:addObjectifIndicateur.html.twig',array('ObjectifIndicateur' => $form->createView(),'msgErr'=>1,
                        'list'=>$listObjectifIndicateur,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }


            if ($val=="modifier" && $id!=0){

                if( !$ObjectifIndicateur = $em->getRepository('GplainteBundle:ObjectifIndicateur')->find($id) )
                {
                    $repository = $em->getRepository('GplainteBundle:ObjectifIndicateur');
                    $listObjectifIndicateur = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:ObjectifIndicateur:addObjectifIndicateur.html.twig',array('ObjectifIndicateur' => $form->createView(),'msgErr'=>"Cet Objectif Indicateur n'existe pas",
                        'list'=>$listObjectifIndicateur,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

                $form = $this->createForm(new ObjectifIndicateurType(), $ObjectifIndicateur);
                $formHandler = new ObjectifIndicateurHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                if($formHandler->process())
                {
                    $repository = $em->getRepository('GplainteBundle:ObjectifIndicateur');
                    $listObjectifIndicateur = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(73);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->render('GplainteBundle:ObjectifIndicateur:addObjectifIndicateur.html.twig',array('ObjectifIndicateur' => $form->createView(),'msg'=>2,
                        'list'=>$listObjectifIndicateur,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }

            $repository = $em->getRepository('GplainteBundle:ObjectifIndicateur');
            $listObjectifIndicateur = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(75);
            $event= new SaveComplaintEvent($action,$user);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:ObjectifIndicateur:addObjectifIndicateur.html.twig',array('ObjectifIndicateur' => $form->createView(),

                'list'=>$listObjectifIndicateur,
                'val'=>$val,
                'id'=>$id,
                'suppr'=>$entiteSuppr
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


    public function showNiveauAction()
    {
        return array(
                // ...
            );
    }


    public function deleteObjectifAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em = $this->getDoctrine()->getManager();

            $user = $this->container->get('security.context')->getToken()->getUser();

            $entity = $em->getRepository('GplainteBundle:ObjectifIndicateur')->find($id);

            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_ObjectifIndicateur',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();
                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(74);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);
                    return $this->redirect($this->generateUrl('gplainte_add_objectif_indicateur',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){

                return $this->redirect($this->generateUrl('gplainte_add_objectif_indicateur',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:ObjectifIndicateur');
            $listObjectifIndicateur = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $ObjectifIndicateur = new ObjectifIndicateur();
            $form = $this->createForm(new ObjectifIndicateurType(), $ObjectifIndicateur);

            return $this->render('GplainteBundle:ObjectifIndicateur:addObjectifIndicateur.html.twig', array(
                'ObjectifIndicateur' => $form->createView(),
                'list'=>$listObjectifIndicateur,
                'val'=>$val,
                'id'=>$id
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }


    }


}
