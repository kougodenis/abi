<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GPlainte\GPlainteBundle\Entity\Agence;
use GPlainte\GPlainteBundle\Form\AgenceType;
use GPlainte\GPlainteBundle\Form\AgenceHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

class AgenceController extends Controller
{

    public function addAgenceAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Agence');
            $institution=$em->getRepository('GplainteBundle:Institution')->findAll();

//            A etudier plus tard
            $user = $this->container->get('security.context')->getToken()->getUser();
//            $inst=$user->getAgent()->getService()->getInstitution();

            $inst= $institution[0]->getId();
            $inst=$em->getRepository('GplainteBundle:Institution')->findOneById($inst);

//        var_dump($user->getAgent()->getService()->getInstitution()->getId()); die();
            $listAgence = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            if ($val=="afficherForm"){
                $Agence=$repository->find($id);
                $form = $this->createForm(new AgenceType(), $Agence);
                $val="modifier";

            }else
                if ($val=="modifier"){} elseif ($val=="supprimer"){
                    $Agence = new Agence();
                    $form = $this->createForm(new AgenceType(), $Agence);
                }else{
                    $Agence = new Agence();
                    $form = $this->createForm(new AgenceType(), $Agence);
                    $val="ajouter";

                }
            $entiteSuppr = $em->getRepository('GplainteBundle:Agence')->find($id);

            if ($val=="ajouter"){
                $Agence = new Agence();
                $form = $this->createForm(new AgenceType(), $Agence);
                $formHandler = new AgenceHandler($form, $this->get('request'), $this->getDoctrine()->getManager(),$inst);
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $libelle=$data['libelle'];
                $recup=  $em->getRepository('GplainteBundle:Agence')->findByLibelle($libelle);

                if (count($recup)== 0){

                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:Agence');
                        $listAgence = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(32);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Agence:addAgence.html.twig',array('Agence' => $form->createView(),'msg'=>1,
                            'list'=>$listAgence,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));

                    }

                } else {
                    $repository = $em->getRepository('GplainteBundle:Agence');
                    $listAgence = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Agence:addAgence.html.twig',array('Agence' => $form->createView(),'msgErr'=>1,
                        'list'=>$listAgence,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }


            if ($val=="modifier" && $id!=0){
                if( !$Agence = $em->getRepository('GplainteBundle:Agence')->find($id) )
                {

                    $repository = $em->getRepository('GplainteBundle:Agence');
                    $listAgence = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Agence:addAgence.html.twig',array('Agence' => $form->createView(),'msgErr'=>"Cette Agence n'existe pas",
                        'list'=>$listAgence,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

                $form = $this->createForm(new AgenceType(), $Agence);
                $formHandler = new AgenceHandler($form, $this->get('request'), $this->getDoctrine()->getManager(),$inst);

                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $libelle=$data['libelle'];
                $recup=  $em->getRepository('GplainteBundle:Agence')->findByLibelle($libelle);

//                if (count($recup)== 0){
                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:Agence');
                        $listAgence = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(33);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Agence:addAgence.html.twig',array('Agence' => $form->createView(),'msg'=>2,
                            'list'=>$listAgence,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }
//                }else {
//
//                    $repository = $em->getRepository('GplainteBundle:Agence');
//                    $listAgence = $repository->findBy(
//                        array(),
//                        array('id'=>'DESC')
//                    );
//                    return $this->render('GplainteBundle:Agence:addAgence.html.twig',array('Agence' => $form->createView(),'msgErr'=>1,
//                        'list'=>$listAgence,
//                        'val'=>$val,
//                        'id'=>$id,
//                        'suppr'=>$entiteSuppr
//                    ));
//                }
            }

            $repository = $em->getRepository('GplainteBundle:Agence');
            $listAgence = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(35);
            $event= new SaveComplaintEvent($action,$user);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:Agence:addAgence.html.twig',array('Agence' => $form->createView(),

                'list'=>$listAgence,
                'val'=>$val,
                'id'=>$id,
                'suppr'=>$entiteSuppr
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


    public function showNiveauAction()
    {
        return array(
                // ...
            );
    }


    public function deleteAgenceAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GplainteBundle:Agence')->find($id);
            $userEvent=$this->container->get('security.context')->getToken()->getUser();


            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_Agence',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(34);
                    $event= new SaveComplaintEvent($action,$userEvent);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->redirect($this->generateUrl('gplainte_add_Agence',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){

                return $this->redirect($this->generateUrl('gplainte_add_Agence',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:Agence');
            $listAgence = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $Agence = new Agence();
            $form = $this->createForm(new AgenceType(), $Agence);

            return $this->render('GplainteBundle:Agence:addAgence.html.twig', array(
                'Agence' => $form->createView(),
                'list'=>$listAgence,
                'val'=>$val,
                'id'=>$id
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


}
