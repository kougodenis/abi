<?php
/**
 * Created by PhpStorm.
 * User: Maurice
 * Date: 24/05/2016
 * Time: 20:21
 */


namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\Validator\Constraints\DateTime;

class DashboardController extends Controller {

    public function showAction(){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {

        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('translator');
        $repository = $em->getRepository('GplainteBundle:Plainte');
        $repository3 = $em->getRepository('GplainteBundle:Affecter');

//        calcul du taux de résolution des plaintes ordinaire
        $nombrePlainteResolue = $repository3->getNombrePlainteResolue();
        $nombrePlainteEnCoursTraitement=$repository3->getNombrePlainteEnCoursTraitement();
        $nombrePlainteTempsMoyenTraitement=$repository3->getTempsMoyenTraitementPlainte();

        $nombreNaturePlainte=$repository->getNombreNaturePlainte();
        $nombrePlainteBureau=$repository->getNombrePlainteParBureau();
        $nombrePlainteSelonGenre=$repository->getNombrePlainteParGenre();



        $ob = new Highchart();
        $ob->chart->renderTo('pie1');
        $ob->title->text($translator->trans('Plaintes recues selon le genre'));
//					$ob2->chart->style(array('fontFamily'=>'Lucida Grande'));
        $ob->plotOptions->pie(array(
            'allowPointSelect' => true,
            'cursor' => 'pointer',
            'dataLabels' => array('enabled' => true),
            'showInLegend' => true
        ));
        $ob->credits->enabled(false);
        for ($i=0;$i<count($nombrePlainteSelonGenre);$i++){
            $data1[]=array('name' => $nombrePlainteSelonGenre[$i]["sexe"], 'y' => intval($nombrePlainteSelonGenre[$i]["nbre"]));
        }

        $ob->series(array(array('type' => 'pie', 'name' => $translator->trans('Plaintes recues selon le genre'), 'data' => $data1)));


        $ob1 = new Highchart();
        $ob1->chart->renderTo('pie');
        $ob1->title->text($translator->trans('Plaintes recues par points de services'));
//					$ob2->chart->style(array('fontFamily'=>'Lucida Grande'));
        $ob1->plotOptions->pie(array(
            'allowPointSelect' => true,
            'cursor' => 'pointer',
            'dataLabels' => array('enabled' => true),
            'showInLegend' => true
        ));
        $ob1->credits->enabled(false);
        for ($i=0;$i<count($nombrePlainteBureau);$i++){
            $data2[]=array('name' => $nombrePlainteBureau[$i]["libelle"], 'y' => intval($nombrePlainteBureau[$i]["nbre"]));
        }

        $ob1->series(array(array('type' => 'pie', 'name' => $translator->trans('Plaintes recues par points de services'), 'data' => $data2)));



        $ob2 = new Highchart();
        $ob2->chart->renderTo('pie0');
        $ob2->title->text($translator->trans('Plaintes recues par objets plainte'));
//					$ob2->chart->style(array('fontFamily'=>'Lucida Grande'));
        $ob2->plotOptions->pie(array(
            'allowPointSelect' => true,
            'cursor' => 'pointer',
            'dataLabels' => array('enabled' => true),
            'showInLegend' => true
        ));
        $ob2->credits->enabled(false);
            for ($i=0;$i<count($nombreNaturePlainte);$i++){
                $data[]=array('name' => $nombreNaturePlainte[$i]["libelle"], 'y' => intval($nombreNaturePlainte[$i]["nbre"]));

            }


        $ob2->series(array(array('type' => 'pie', 'name' => $translator->trans('Plaintes recues par objets plainte'), 'data' => $data)));

        $logins = $em->getRepository('UserBundle:Login')->findBy(
            array(),
            array('id'=>'desc'),
            11,
            0
        );

        $dateNotification= new \DateTime();

//        var_dump($dateNotification->format("j")); die();

        return $this->render("GplainteBundle::dashboard.html.twig",array("plainteresolue"=>$nombrePlainteResolue["nbre"],"plainteEnCoursTraitement"=>$nombrePlainteEnCoursTraitement["nbre"],
            "tempsmoyen"=>round($nombrePlainteTempsMoyenTraitement["jr"],0),
            'graphe1'=>$ob2,
            'graphe2'=>$ob1,
            'graphe3'=>$ob,
            'journal'=>$logins,
            'datesenderror'=>$dateNotification->format("j")));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

    public function NbreRejetEnvoiFichierErreurAction(){
        $path = substr($this->get('kernel')->getRootDir(), 0, -4) . '/src/GPlainte/GPlainteBundle/Journal/';
        $filename = 'param.json';

        $params = json_decode(file_get_contents($path . '' . $filename));
        var_dump($params); die();

        $jsondata = json_encode(array('count' => 0));
        file_put_contents($path . '' . $filename, $jsondata);

        return $this->redirect($this->generateUrl("gplainte_dashboard"));

    }
}