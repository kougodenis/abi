<?php

namespace GPlainte\GPlainteBundle\Controller;

use GPlainte\GPlainteBundle\Form\AgentAvatarType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GPlainte\GPlainteBundle\Entity\Agent;
use GPlainte\GPlainteBundle\Form\AgentType;
use GPlainte\GPlainteBundle\Form\AgentHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

class AgentController extends Controller
{

    public function showAgentAction()
    {




    }


    public function addAgentAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Agent');
            $user = $this->container->get('security.context')->getToken()->getUser();

            $listagent = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            if ($val=="afficherForm"){
                $agent=$repository->find($id);
                $form = $this->createForm(new AgentType(), $agent);
                $val="modifier";

            }else
                if ($val=="modifier"){} elseif ($val=="supprimer"){
                    $agent = new Agent();
                    $form = $this->createForm(new AgentType(), $agent);
                }else{
                    $agent = new Agent();
                    $form = $this->createForm(new AgentType(), $agent);
                    $val="ajouter";
                }
            $entiteSuppr = $em->getRepository('GplainteBundle:Agent')->find($id);

            if ($val=="ajouter"){
                $agent = new Agent();
                $form = $this->createForm(new AgentType(), $agent);
                $formHandler = new AgentHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $nom=$data['nom'];

                $prenom=$data['prenom'];
                if (isset($data['chef'])){
                    $chef=$data['chef'];
                }
                if (isset($data['dg'])){
                    $dg=$data['dg'];
                }
                if (isset($data['service'])){
                    $service=$data['service'];
                }
                if (isset($data['email'])){
                    $email=$data['email'];
                }
                if (isset($data['telephone'])){
                    $telephone=$data['telephone'];
                }

                $recup=  $em->getRepository('GplainteBundle:Agent')->findBy(array('nom'=>$nom,'prenom'=>$prenom));

                if (isset($telephone) && $telephone!=""){
                    $contact=  $em->getRepository('GplainteBundle:Agent')->findByTelephone($telephone);
                    if (count($contact)>0){
                        $repository = $em->getRepository('GplainteBundle:Agent');
                        $listagent = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );
                        return $this->render('GplainteBundle:Agent:addAgent.html.twig',array('agent' => $form->createView(),'msgErr'=>4,
                            'list'=>$listagent,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }
                }

                if (isset($email) && $email!=""){
                    $contact=  $em->getRepository('GplainteBundle:Agent')->findByEmail($email);
                    if (count($contact)>0){
                        $repository = $em->getRepository('GplainteBundle:Agent');
                        $listagent = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );
                        return $this->render('GplainteBundle:Agent:addAgent.html.twig',array('agent' => $form->createView(),'msgErr'=>5,
                            'list'=>$listagent,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }
                }

                if (isset($chef) && $chef==1){
                    $chefService=  $em->getRepository('GplainteBundle:Agent')->findBy(array('service'=>$service,'chef'=>$chef));
                    if (count($chefService)>0){
                        $repository = $em->getRepository('GplainteBundle:Agent');
                        $listagent = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );
                        return $this->render('GplainteBundle:Agent:addAgent.html.twig',array('agent' => $form->createView(),'msgErr'=>2,
                            'list'=>$listagent,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }
                }

                if (isset($dg) && $dg==1){
                    $chefService=  $em->getRepository('GplainteBundle:Agent')->findBy(array('dg'=>$dg));
                    if (count($chefService)>0){
                        $repository = $em->getRepository('GplainteBundle:Agent');
                        $listagent = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );
                        return $this->render('GplainteBundle:Agent:addAgent.html.twig',array('agent' => $form->createView(),'msgErr'=>3,
                            'list'=>$listagent,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }
                }


                if (count($recup)== 0){

                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:Agent');
                        $listagent = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(48);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Agent:addAgent.html.twig',array('agent' => $form->createView(),'msg'=>1,
                            'list'=>$listagent,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));

                    }

                } else {
                    $repository = $em->getRepository('GplainteBundle:Agent');
                    $listagent = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Agent:addAgent.html.twig',array('agent' => $form->createView(),'msgErr'=>1,
                        'list'=>$listagent,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }


            if ($val=="modifier" && $id!=0){

                if( !$agent = $em->getRepository('GplainteBundle:Agent')->find($id) )
                {
                    $repository = $em->getRepository('GplainteBundle:Agent');
                    $listagent = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Agent:addAgent.html.twig',array('agent' => $form->createView(),'msgErr'=>"Cet agent n'existe pas",
                        'list'=>$listagent,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

                $form = $this->createForm(new AgentType(), $agent);
                $formHandler = new AgentHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                if($formHandler->process())
                {
                    $repository = $em->getRepository('GplainteBundle:Agent');
                    $listagent = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(49);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->render('GplainteBundle:Agent:addAgent.html.twig',array('agent' => $form->createView(),'msg'=>2,
                        'list'=>$listagent,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }


            $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(51);
            $event= new SaveComplaintEvent($action,$user);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:Agent:addAgent.html.twig',array('agent' => $form->createView(),
                'list'=>$listagent,
                'val'=>$val,
                'id'=>$id,
                'suppr'=>$entiteSuppr
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }


    }

    public function modificationAvatarAction($val){
        $em = $this->getDoctrine()->getManager();
        $userEvent = $this->container->get('security.context')->getToken()->getUser();
        $user = $userEvent->getAgent();
        $entity = $em->getRepository('GplainteBundle:Agent')->find($user->getId());
        $form = $this->createForm(new AgentAvatarType(), $entity);

        if ($val==1){

            $entity = $em->getRepository('GplainteBundle:Agent')->find($user->getId());
            $form = $this->createForm(new AgentAvatarType(), $entity);
            $formHandler = new AgentHandler($form, $this->get('request'), $this->getDoctrine()->getManager());

            if($formHandler->process())
            {
                $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(84);
                $event= new SaveComplaintEvent($action,$userEvent);
                $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                return $this->render('GplainteBundle:Agent:avatar.html.twig',array('agent' => $form->createView(),
                    'list'=>$entity,

                ));
            }
        }

        return $this->render('GplainteBundle:Agent:avatar.html.twig',array('agent' => $form->createView(),
            'list'=>$entity,

        ));

    }


    public function deleteAgentAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GplainteBundle:Agent')->find($id);
            $user = $this->container->get('security.context')->getToken()->getUser();


            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_agent',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(50);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->redirect($this->generateUrl('gplainte_add_agent',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){

                return $this->redirect($this->generateUrl('gplainte_add_agent',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:Agent');
            $listagent = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $agent = new Agent();
            $form = $this->createForm(new AgentType(), $agent);

            return $this->render('GplainteBundle:Agent:addAgent.html.twig', array(
                'service' => $form->createView(),
                'list'=>$listagent,
                'val'=>$val,
                'id'=>$id
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }



    }

}
