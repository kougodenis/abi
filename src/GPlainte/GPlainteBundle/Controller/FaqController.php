<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GPlainte\GPlainteBundle\Entity\Faq;
use GPlainte\GPlainteBundle\Form\FaqType;
use GPlainte\GPlainteBundle\Form\FaqHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;
use GPlainte\GPlainteBundle\Journal\ValuesEvent;

class FaqController extends Controller
{

    public function addFaqAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){

            $userEvent = $this->container->get('security.context')->getToken()->getUser();

            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Faq');

            $listfaq = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            if ($val=="afficherForm"){
                $faq=$repository->find($id);
                $form = $this->createForm(new FaqType(), $faq);
                $val="modifier";

            }else
                if ($val=="modifier"){} elseif ($val=="supprimer"){
                    $faq = new Faq();
                    $form = $this->createForm(new FaqType(), $faq);
                }else{
                    $faq = new Faq();
                    $form = $this->createForm(new FaqType(), $faq);
                    $val="ajouter";
                }
            $entiteSuppr = $em->getRepository('GplainteBundle:Faq')->find($id);


            if ($val=="ajouter"){
                $faq = new Faq();
                $form = $this->createForm(new FaqType(), $faq);
                $formHandler = new FaqHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $libelle=$data['question'];
                $recup=  $em->getRepository('GplainteBundle:Faq')->findByQuestion($libelle);

                if (count($recup)== 0){

                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:Faq');
                        $listfaq = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );
                        $action=$em->getRepository('UserBundle:DataEvent')->find(23);
                        $event= new SaveComplaintEvent($action,$userEvent);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Faq:addFaq.html.twig',array('faq' => $form->createView(),'msg'=>1,
                            'list'=>$listfaq,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));

                    }

                } else {
                    $repository = $em->getRepository('GplainteBundle:Faq');
                    $listfaq = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Faq:addFaq.html.twig',array('faq' => $form->createView(),'msgErr'=>1,
                        'list'=>$listfaq,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }


            if ($val=="modifier" && $id!=0){

                if( !$faq = $em->getRepository('GplainteBundle:Faq')->find($id) )
                {
                    $repository = $em->getRepository('GplainteBundle:Faq');
                    $listfaq = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Faq:addFaq.html.twig',array('faq' => $form->createView(),'msgErr'=>"Cette FAQ n'existe pas",
                        'list'=>$listfaq,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

                $form = $this->createForm(new FaqType(), $faq);
                $formHandler = new FaqHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                if($formHandler->process())
                {
                    $repository = $em->getRepository('GplainteBundle:Faq');
                    $listfaq = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );

                    $action=$em->getRepository('UserBundle:DataEvent')->find(24);
                    $event= new SaveComplaintEvent($action,$userEvent);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->render('GplainteBundle:Faq:addFaq.html.twig',array('faq' => $form->createView(),'msg'=>2,
                        'list'=>$listfaq,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }

            return $this->render('GplainteBundle:Faq:addFaq.html.twig',array('faq' => $form->createView(),
                'list'=>$listfaq,
                'val'=>$val,
                'id'=>$id,
                'suppr'=>$entiteSuppr
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }


    }


    public function deleteFaqAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $userEvent = $this->container->get('security.context')->getToken()->getUser();

            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GplainteBundle:Faq')->find($id);

            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_faq',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();

                    $action=$em->getRepository('UserBundle:DataEvent')->find(25);
                    $event= new SaveComplaintEvent($action,$userEvent);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->redirect($this->generateUrl('gplainte_add_faq',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){
                return $this->redirect($this->generateUrl('gplainte_add_faq',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:Faq');
            $listfaq = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $faq = new Faq();
            $form = $this->createForm(new FaqType(), $faq);

            return $this->render('GplainteBundle:Faq:addFaq.html.twig', array(
                'faq' => $form->createView(),
                'list'=>$listfaq,
                'val'=>$val,
                'id'=>$id
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


    public function listFaqAction(){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){

            $userEvent = $this->container->get('security.context')->getToken()->getUser();

            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Faq');

            $listfaq = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $action=$em->getRepository('UserBundle:DataEvent')->find(26);
            $event= new SaveComplaintEvent($action,$userEvent);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:Faq:listFaq.html.twig', array(
                'list'=>$listfaq,
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

}
