<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GPlainte\GPlainteBundle\Entity\Satisfaction;
use GPlainte\GPlainteBundle\Form\SatisfactionType;
use GPlainte\GPlainteBundle\Form\SatisfactionHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;
use GPlainte\GPlainteBundle\Journal\ValuesEvent;

class SatisfactionController extends Controller
{

    public function ListSatisfactionAction()
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){

            $userEvent = $this->container->get('security.context')->getToken()->getUser();

            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Affecter');

            $listesatisfaction=$repository->getListeSatisfaction();

            $action=$em->getRepository('UserBundle:DataEvent')->find(16);
            $event= new SaveComplaintEvent($action,$userEvent);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:Satisfaction:showSatisfaction.html.twig', array(
                'list'=>$listesatisfaction,
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }


    }

    
    public function addSatisfactionAction($val,$action,$notification,$plainte)
    {

        $userEvent = $this->container->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();
        $satisfaction = new Satisfaction();
        $form = $this->createForm(new SatisfactionType(), $satisfaction);

        if ($val=="ajouter") {
            $request = $this->getRequest();
            $data = $request->request->get($form->getName());
            $notificationObject=$em->getRepository("GplainteBundle:Notification")->find($notification);

            if($request->getMethod()=='POST' )
            {
                $form->handleRequest($request);
                if( $form->isValid() )
                {
                    $notificationObject->setSatisfaction($satisfaction);

                    $em->persist($satisfaction);
                    $em->flush();

                    $action=$em->getRepository('UserBundle:DataEvent')->find(15);
                    $event= new SaveComplaintEvent($action,$userEvent);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->redirect($this->generateUrl("gplainte_list_notifier",array("id"=>1)));

                }
            }

        }

        if ($notification!=0 and $plainte!=0){
            $notification=$em->getRepository("GplainteBundle:Notification")->findOneById($notification);
            $plainte=$em->getRepository("GplainteBundle:Plainte")->findOneById($plainte);
            $action=$em->getRepository("GplainteBundle:Action")->findOneById($action);

        }else{
            $action=NULL;
            $plainte=NULL;
            $notification=NULL;
        }
        return $this->render('GplainteBundle:Satisfaction:addSatisfaction.html.twig', array(
            'satisfaction' => $form->createView(),
            'notification'=>$notification,
            'plainte'=>$plainte,
            'action'=>$action
        ));
    }



    public function updateSatisfactionAction($val,$action,$notification,$plainte,$satisfaction)
    {
        $userEvent = $this->container->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();
        $satisfaction = $em->getRepository("GplainteBundle:Satisfaction")->find($satisfaction);
        $form = $this->createForm(new SatisfactionType(), $satisfaction);

        if ($val=="modifier") {
            $request = $this->getRequest();
            $data = $request->request->get($form->getName());
            $notificationObject=$em->getRepository("GplainteBundle:Notification")->find($notification);

            if($request->getMethod()=='POST' )
            {
                $form->handleRequest($request);
                if( $form->isValid() )
                {
                    $notificationObject->setSatisfaction($satisfaction);
                    $satisfaction->setVerifier(1);
                    $em->persist($satisfaction);
                    $em->flush();

                    $action=$em->getRepository('UserBundle:DataEvent')->find(17);
                    $event= new SaveComplaintEvent($action,$userEvent);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->redirect($this->generateUrl("gplainte_list_satisfaction",array("id"=>1)));

                }
            }

        }

        if ($notification!=0 and $plainte!=0){
            $notification=$em->getRepository("GplainteBundle:Notification")->findOneById($notification);
            $plainte=$em->getRepository("GplainteBundle:Plainte")->findOneById($plainte);
            $action=$em->getRepository("GplainteBundle:Action")->findOneById($action);

        }else{
            $action=NULL;
            $plainte=NULL;
            $notification=NULL;
        }
        return $this->render('GplainteBundle:Satisfaction:updateSatisfaction.html.twig', array(
            'satisfaction' => $form->createView(),
            'notification'=>$notification,
            'idsatisfaction'=>$satisfaction,
            'plainte'=>$plainte,
            'action'=>$action
        ));
    }




    public function deleteSatisfactionAction()
    {
        return array(
                // ...
            );    }

}
