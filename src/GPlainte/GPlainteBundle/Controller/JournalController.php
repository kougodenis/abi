<?php
/**
 * Created by PhpStorm.
 * User: Maurice
 * Date: 15/08/2016
 * Time: 20:44
 */

namespace GPlainte\GPlainteBundle\Controller;
use GPlainte\GPlainteBundle\Journal\ValuesEvent;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class JournalController extends Controller {

    public function showJournalAction($val)
    {

        $em=$this->getDoctrine()->getManager();
        if ($val==="vrai"){

//            $date=new \DateTime($_POST['datejournal']);
            $date=$_POST['datejournal'];
            $op1=$_POST['op1'];
            $op2=$_POST['op2'];
            $explode=explode(' ',$date);
            $date1=$explode[0].' 00:00:00';
            $logins = $em->getRepository('UserBundle:Login')->getJournaux($date1,$date,$_POST['identifiants'],$_POST['activite'],$op1,$op2);

        }else{
            $logins = $em->getRepository('UserBundle:Login')->findBy(
                array(),
                array('id'=>'desc'),
                30,
                0
            );
        }

        $users=$em->getRepository('UserBundle:User')->findAll();
        $typesActions=$em->getRepository('UserBundle:DataEvent')->findAll();;

//         var_dump($typesActions); die();
        return $this->render('GplainteBundle:Journal:journal.html.twig', array('list' => $logins, 'users'=>$users,'types'=>$typesActions));
    }
}