<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GPlainte\GPlainteBundle\Entity\typeNiveau;
use GPlainte\GPlainteBundle\Form\typeNiveauType;
use GPlainte\GPlainteBundle\Form\typeNiveauHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

class typeNiveauController extends Controller
{

    public function addtypeNiveauAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $user = $this->container->get('security.context')->getToken()->getUser();

            $repository = $em->getRepository('GplainteBundle:typeNiveau');

            $listtypeNiveau = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            if ($val=="afficherForm"){
                $typeniveau=$repository->find($id);
                $form = $this->createForm(new typeNiveauType(), $typeniveau);
                $val="modifier";

            }else
                if ($val=="modifier"){} elseif ($val=="supprimer"){
                    $typeniveau = new typeNiveau();
                    $form = $this->createForm(new typeNiveauType(), $typeniveau);
                }else{
                    $typeniveau = new typeNiveau();
                    $form = $this->createForm(new typeNiveauType(), $typeniveau);
                    $val="ajouter";
                }
            $entiteSuppr = $em->getRepository('GplainteBundle:typeNiveau')->find($id);


            if ($val=="ajouter"){
                $typeNiveau = new typeNiveau();
                $form = $this->createForm(new typeNiveauType(), $typeNiveau);
                $formHandler = new typeNiveauHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $libelle=$data['libelle'];
                $recup=  $em->getRepository('GplainteBundle:typeNiveau')->findByLibelle($libelle);

                if (count($recup)== 0){

                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:typeNiveau');
                        $listtypeNiveau = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(68);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);


//                    return $this->render('GplainteBundle:typeNiveau:addtypeNiveau.html.twig',array('typeniveau' => $form->createView(),'msg'=>1,
//                        'list'=>$listtypeNiveau,
//                'val2'=>$val,
//                'id'=>$id,
//                'suppr'=>$entiteSuppr
//                    ));

                        return $this->redirect($this->generateUrl("gplainte_add_niveau",array('msg'=>5)));

                    }

                } else {
                    $repository = $em->getRepository('GplainteBundle:typeNiveau');
                    $listtypeNiveau = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:typeNiveau:addtypeNiveau.html.twig',array('typeniveau' => $form->createView(),'msgErr'=>1,
                        'list'=>$listtypeNiveau,
                        'val2'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }

            if ($val=="modifier" && $id!=0){

                if( !$typeniveau = $em->getRepository('GplainteBundle:typeNiveau')->find($id) )
                {
                    $repository = $em->getRepository('GplainteBundle:typeNiveau');
                    $listtypeniveau = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:typeNiveau:addtypeNiveau.html.twig',array('typeniveau' => $form->createView(),'msgErr'=>"Cette FAQ n'existe pas",
                        'list'=>$listtypeniveau,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

                $form = $this->createForm(new typeNiveauType(), $typeniveau);
                $formHandler = new typeNiveauHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                if($formHandler->process())
                {
                    $repository = $em->getRepository('GplainteBundle:typeNiveau');
                    $listtypeniveau = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(69);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);


                    return $this->render('GplainteBundle:typeNiveau:addtypeNiveau.html.twig',array('typeniveau' => $form->createView(),'msg'=>2,
                        'list'=>$listtypeniveau,
                        'val2'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }

            $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(71);
            $event= new SaveComplaintEvent($action,$user);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:typeNiveau:addtypeNiveau.html.twig',array('typeniveau' => $form->createView(),
                'list'=>$listtypeNiveau,
                'val2'=>$val,
                'id'=>$id,
                'suppr'=>$entiteSuppr
            ));

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


    public function showtypeNiveauAction()
    {
        return array(
                // ...
            );
    }


    public function deleteTypeNiveauAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em = $this->getDoctrine()->getManager();
            $user = $this->container->get('security.context')->getToken()->getUser();

            $entity = $em->getRepository('GplainteBundle:typeNiveau')->find($id);

            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_typeniveau',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(70);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);


                    return $this->redirect($this->generateUrl('gplainte_add_typeniveau',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){

                return $this->redirect($this->generateUrl('gplainte_add_typeniveau',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:typeNiveau');
            $listtypeniveau = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $typeniveau = new typeNiveau();
            $form = $this->createForm(new typeNiveauType(), $typeniveau);

            return $this->render('GplainteBundle:typeNiveau:addtypeNiveau.html.twig', array(
                'typeniveau' => $form->createView(),
                'list'=>$listtypeniveau,
                'val'=>$val,
                'id'=>$id
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


}
