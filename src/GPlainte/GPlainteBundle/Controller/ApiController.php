<?php
/**
 * Created by PhpStorm.
 * User: Maurice
 * Date: 28/03/2016
 * Time: 18:15
 */

namespace GPlainte\GPlainteBundle\Controller;

use GPlainte\GPlainteBundle\Entity\Mobileplainte;
use GPlainte\GPlainteBundle\Entity\PlainteMobile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use GPlainte\GPlainteBundle\Entity\Denonciation;

class ApiController extends Controller {
    public function getClientsAction(){
        $em=$this->getDoctrine()->getManager();
        $client = $em->getRepository('GplainteBundle:Client')->findAll();
//        var_dump(JsonResponse($client)); die();

        foreach ($client as $client) {
            $listClient[]=array('id'=>$client->getId(),'nom'=>$client->getNom()." ".$client->getPrenom());
        }

        return new JsonResponse($listClient);
    }

    public function getObjetsAction(){
        $em=$this->getDoctrine()->getManager();
        $niveau = $em->getRepository('GplainteBundle:Niveau')->findAll();

//        $this->container->get('')

        foreach ($niveau as $niveau) {
            $listNiveau[]=array('id'=>$niveau->getId(),'libelle'=>$niveau->getLibelle());
        }

        return new JsonResponse($listNiveau);
    }

    public function plainteAction(){

        $request=  $this->getRequest();
        $em=$this->getDoctrine()->getManager();
        $niveauRepository = $em->getRepository('GplainteBundle:Niveau');
        $typePlainteRepository = $em->getRepository('GplainteBundle:Choix');

//        if (isset($_POST['nom']) && $_POST['nom']!=""){
        $nom=$request->request->get("nom");
        $description=$request->request->get("description");
        $objet=$request->request->get("objet");
        $agence=$request->request->get("agence");
        $autresobjet=$request->request->get("autreObjet");
        $telephone=$request->request->get("telephone");
        $structure=$request->request->get("structure");
        $etatClient=$request->request->get("client");
        $objetNiveau=$niveauRepository->findOneByLibelle($objet);
        $typePlainte=$typePlainteRepository->findOneById($objetNiveau->getChoix()->getId());


        if ($etatClient==true){
            $etatClient=1;
        }elseif ($etatClient==false){
            $etatClient=0;
        }

        $plainte= new Mobileplainte();
        $plainte->setAutreobjet($autresobjet);
        $plainte->setTelephone($telephone);
        $plainte->setNiveau($objetNiveau);
        $plainte->setNom($nom);
        $plainte->setDescription($description);
        $plainte->setChoix($typePlainte);
        $plainte->setSfd($structure);
        $plainte->setBureau($agence);
        $plainte->setEtatclient($etatClient);
        $plainte->setEtat(0);
        $em->persist($plainte);
        $em->flush();

        $plainte = $em->getRepository('GplainteBundle:Mobileplainte')->getLastInsertDenonciation();
        $idPlainte=$plainte["code"];
        $reponse="Votre preoccupation a ete enregistree avec succes a ".$structure." sous le numero ". $idPlainte.", nous vous recontacterons pour en savoir plus.". " Merci d avoir utilise SATIS Mobile.";

        return new JsonResponse($reponse);

    }


    public function plaintemobileAction(){

        $request=  $this->getRequest();
        $em=$this->getDoctrine()->getManager();
        $plaintemobileRepository = $em->getRepository('GplainteBundle:PlainteMobile');

        $nom=$request->request->get("nomcomplet");
        $description=$request->request->get("contenu");
        $raisonsociale=$request->request->get("raisonsociale");
        $institution=$request->request->get("institution");
        $idplainte=$request->request->get("idplainte");
        $telephone=$request->request->get("telephone");
        $type=$request->request->get("type");
        $uid=$request->request->get("uid");
        $fichier=$request->request->get("fichier");
        $extension=pathinfo($fichier,PATHINFO_EXTENSION);


//        if ($etatClient==true){
//            $etatClient=1;
//        }elseif ($etatClient==false){
//            $etatClient=0;
//        }

        $plainte= new PlainteMobile();
        $plainte->setInstitution($institution);
        $plainte->setNomcomplet($nom);
        $plainte->setContenu($description);
        $plainte->setIdplainte($idplainte);
        $plainte->setRaisonsociale($raisonsociale);
        $plainte->setTelephone($telephone);
        $plainte->setUid($uid);
        $plainte->setType($type);
        $plainte->setNomfichier($fichier);
        $plainte->setFormatfichier($extension);
        $plainte->setValidation(false);
        $em->persist($plainte);
        $em->flush();

//        $plainte = $em->getRepository('GplainteBundle:Mobileplainte')->getLastInsertDenonciation();
//        $idPlainte=$plainte["code"];

        $reponse="Votre preoccupation a ete enregistree avec succes ".$extension;
        return new JsonResponse($reponse);

    }


    public function setPlainteMobileAction($id,$type,$etat){
        $em=$this->getDoctrine()->getManager();
        $plainte=$em->getRepository("GplainteBundle:PlainteMobile")->findOneBy(array('id'=>$id,'type'=>$type));
//        var_dump($plainte); die();
        $plainte->setValidation($etat);
        $em->persist($plainte);
        $em->flush();
//        var_dump($plainte); die();

        $plainteMobile=$em->getRepository("GplainteBundle:PlainteMobile")->findBy(
            array('type'=>0,'validation'=>0),
            array('id'=>'DESC')
        );

        $denonciationMobile=$em->getRepository("GplainteBundle:PlainteMobile")->findBy(
            array('type'=>1,'validation'=>0),
            array('id'=>'DESC')
        );

        return $this->redirect($this->generateUrl('gplainte_add_plainte', array(
            'objet'=>$plainteMobile,
            'objetdenoncation'=>$denonciationMobile

        )));

    }
}
