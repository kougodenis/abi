<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPlainte\GPlainteBundle\Entity\Notification;
use GPlainte\GPlainteBundle\Form\NotificationType;
use GPlainte\GPlainteBundle\Form\NotificationType2;
use GPlainte\GPlainteBundle\Form\NotificationHandler;

use GPlainte\GPlainteBundle\Controller\ParametresController;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;
use GPlainte\GPlainteBundle\Journal\ValuesEvent;

class NotificationController extends Controller
{

    public function ListNotificationAction()
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Affecter');
            $userEvent = $this->container->get('security.context')->getToken()->getUser();

            $listenotification=$repository->getListeNotification();

            $action=$em->getRepository('UserBundle:DataEvent')->find(14);
            $event= new SaveComplaintEvent($action,$userEvent);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:Notification:showNotification.html.twig', array(
                'list'=>$listenotification,
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }


    }


    public function addNotificationAction($val,$action,$plainte)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){

            $userEvent = $this->container->get('security.context')->getToken()->getUser();

            $em = $this->getDoctrine()->getManager();
            $notification = new Notification();
            $form = $this->createForm(new NotificationType, $notification);

            if ($val=="ajouter") {
                $request = $this->getRequest();
                $data = $request->request->get($form->getName());
                $date=$data["date"].' '.date('H:i:s');
                // var_dump();
                //echo $date;
                //    die();
                $actionObject=$em->getRepository("GplainteBundle:Action")->find($action);

                if($request->getMethod()=='POST' )
                {
                    $form->handleRequest($request);
                    if( $form->isValid() )
                    {
//  --------------------- notification à l'appli mobile que la plainte a été traiter -------------------------------------
                        $affecterObject=$em->getRepository("GplainteBundle:Affecter")->findOneByAction($actionObject->getId());
                        $identifiantplaintemobile=$affecterObject->getPlainte()->getCodeplaintemobile();

                        if ($identifiantplaintemobile!=NULL){
                            $param=new ParametresController();
                            $param->notificationMobilePlainteTraiter($identifiantplaintemobile);
                        }
//------------------------------------------------------------------------------------------------------------------------
                        $actionObject->setNotifier($notification);
                        $actionObject->setNotification(1);
                        $notification->setDate(new \DateTime($date));
                        $em->persist($notification);
                        $em->flush();

                        $action=$em->getRepository('UserBundle:DataEvent')->find(13);
                        $event= new SaveComplaintEvent($action,$userEvent);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->redirect($this->generateUrl("gplainte_plainte_approuve",array("id"=>1)));

                    }
                }

            }

            if ($action!=0 and $plainte!=0){
                $action=$em->getRepository("GplainteBundle:Action")->findOneById($action);
                $plainte=$em->getRepository("GplainteBundle:Plainte")->findOneById($plainte);
            }else{
                $action=NULL;
                $plainte=NULL;
            }
            return $this->render('GplainteBundle:Notification:addNotification.html.twig', array(
                'notification' => $form->createView(),
                'action'=>$action,
                'plainte'=>$plainte
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }


    }


    /**
     * @Route("/deleteAgent")
     * @Template()
     */
    public function deleteAgentAction()
    {
        return array(
                // ...
            );
    }

}
