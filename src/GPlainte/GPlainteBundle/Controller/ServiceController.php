<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GPlainte\GPlainteBundle\Entity\Service;
use GPlainte\GPlainteBundle\Form\ServiceType;
use GPlainte\GPlainteBundle\Form\ServiceHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

class ServiceController extends Controller
{

    public function showServiceAction()
    {
        return array(
                // ...
            );    }


    public function addServiceAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Service');
            $user = $this->container->get('security.context')->getToken()->getUser();

           // $user = $this->container->get('security.context')->getToken()->getUser();
           // $inst=$user->getAgent()->getService()->getInstitution();

            $institution=$em->getRepository('GplainteBundle:Institution')->findAll();
            $inst= $institution[0]->getId();
            $inst=$em->getRepository('GplainteBundle:Institution')->findOneById($inst);

            $listservice = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            if ($val=="afficherForm"){
                $service=$repository->find($id);
                $form = $this->createForm(new ServiceType(), $service);
                $val="modifier";

            }else
                if ($val=="modifier"){} elseif ($val=="supprimer"){
                    $service = new Service();
                    $form = $this->createForm(new ServiceType(), $service);
                }else{
                    $service = new Service();
                    $form = $this->createForm(new ServiceType(), $service);
                    $val="ajouter";
                }
            $entiteSuppr = $em->getRepository('GplainteBundle:Service')->find($id);

            if ($val=="ajouter"){
                $service = new Service();
                $form = $this->createForm(new ServiceType(), $service);
                $formHandler = new ServiceHandler($form, $this->get('request'), $this->getDoctrine()->getManager(),$inst);
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $libelle=$data['libelle'];
                $recup=  $em->getRepository('GplainteBundle:Service')->findByLibelle($libelle);

                if (count($recup)== 0){

                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:Service');
                        $listservice = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(40);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Service:addService.html.twig',array('service' => $form->createView(),'msg'=>1,
                            'list'=>$listservice,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));

                    }

                } else {
                    $repository = $em->getRepository('GplainteBundle:Service');
                    $listservice = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Service:addService.html.twig',array('service' => $form->createView(),'msgErr'=>1,
                        'list'=>$listservice,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }


            if ($val=="modifier" && $id!=0){

                if( !$service = $em->getRepository('GplainteBundle:Service')->find($id) )
                {
                    $repository = $em->getRepository('GplainteBundle:Service');
                    $listservice = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Service:addService.html.twig',array('service' => $form->createView(),'msgErr'=>"Ce service n'existe pas",
                        'list'=>$listservice,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

                $form = $this->createForm(new ServiceType(), $service);
                $formHandler = new ServiceHandler($form, $this->get('request'), $this->getDoctrine()->getManager(),$inst);
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $libelle=$data['libelle'];
                $recup=  $em->getRepository('GplainteBundle:Service')->findByLibelle($libelle);
                if (count($recup)== 0){
                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:Service');
                        $listservice = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(41);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Service:addService.html.twig',array('service' => $form->createView(),'msg'=>2,
                            'list'=>$listservice,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }
                }else {
                    $repository = $em->getRepository('GplainteBundle:Service');
                    $listservice = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(43);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->render('GplainteBundle:Service:addService.html.twig',array('service' => $form->createView(),'msgErr'=>1,
                        'list'=>$listservice,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }



            return $this->render('GplainteBundle:Service:addService.html.twig',array('service' => $form->createView(),
                'list'=>$listservice,
                'val'=>$val,
                'id'=>$id,
                'suppr'=>$entiteSuppr
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


    public function deleteServiceAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GplainteBundle:Service')->find($id);
            $user = $this->container->get('security.context')->getToken()->getUser();


            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_service',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(42);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->redirect($this->generateUrl('gplainte_add_service',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){

                return $this->redirect($this->generateUrl('gplainte_add_service',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:Service');
            $listservice = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $service = new Service();
            $form = $this->createForm(new ServiceType(), $service);

            return $this->render('GplainteBundle:Service:addService.html.twig', array(
                'service' => $form->createView(),
                'list'=>$listservice,
                'val'=>$val,
                'id'=>$id
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }


    }

}
