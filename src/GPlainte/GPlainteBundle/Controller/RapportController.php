<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ob\HighchartsBundle\Highcharts\Highchart;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

//use mikehaertl\wkhtmlto\Pdf;

class RapportController extends Controller
{
    
    public function ParametresAction()
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Bureau');

            $listbureau=$repository->findAll();

            return $this->render('GplainteBundle:Rapport:parameters.html.twig', array(
                'bureau'=>$listbureau
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

    
    public function showAction()
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $translator = $this->get('translator');
            $userEvent=$this->container->get('security.context')->getToken()->getUser();

//            $request=$this->container->get(Request);
//            var_dump($request);

            $repository = $em->getRepository('GplainteBundle:Plainte');
            $repository2 = $em->getRepository('GplainteBundle:Bureau');
            $repository3 = $em->getRepository('GplainteBundle:Affecter');
            $repository4 = $em->getRepository('GplainteBundle:Notification');
            $repository5 = $em->getRepository('GplainteBundle:Satisfaction');
            $repository6 = $em->getRepository('GplainteBundle:Agence');

            $repositoryNormeIndicateur = $em->getRepository('GplainteBundle:NormeIndicateur');
            $ListeObjectifs = $repositoryNormeIndicateur->getNormeIndicateur();

            if (isset($_POST["debut"]) && isset($_POST["fin"])) {
                $datedebut = explode('-', $_POST["debut"]);
                $debut = $datedebut[2] . '-' . $datedebut[1] . '-' . $datedebut[0];
                $datefin = explode('-', $_POST["fin"]);
                $fin = $datefin[2] . '-' . $datefin[1] . '-' . $datefin[0];

                $d = date_format(new \DateTime($debut), 'd-m-Y');
                $f = date_format(new \DateTime($fin), 'd-m-Y');
//echo htmlentities($_POST["generale"]); die();
                $repositoryIndicateur = $em->getRepository('GplainteBundle:NormeIndicateur');
                $nbreJr = $repositoryIndicateur->findOneByMarqueur('efficacite');
                $nbreJrSatisfaction = $repositoryIndicateur->findOneByMarqueur('satisfaction');
                $nbreJrSensible = $repositoryIndicateur->findOneByMarqueur('sensible');

                $jourTraitementPlainteOrdinaire=$nbreJrSatisfaction->getJour();
                $jourTraitementPlainteSensible=$nbreJrSensible->getJour();
                $periode = $d . ' ' . $translator->trans('au') . ' ' . $f;
                if (htmlentities($_POST["generale"]) == 1) {
                    $listePervicesPlusSollicites = $repository3->getListeServicesLesPlusSollicites($debut, $fin);
                    $listePervicesPlusIndexes = $repository->getListeServicesLesPlusIndexes($debut, $fin);

                    $listeMesuresPreventives = $repository3->getListeMesurePreventive($debut, $fin);

                    $listeMesuresPreventivesParAgence = $repository3->getListeMesurePreventiveParAgence($debut, $fin);

                    $nombrePlainteParPeriode = $repository->getNombrePlainteParPeriode($debut, $fin);

                    $nombrePlainteSatisfaitParPeriode = $repository->getNombrePlainteSatisfaitParPeriode($debut, $fin);

                    $nombrePlainteTraiteDelais = $repository3->getNombrePlainteTraiteDelais($debut, $fin, $nbreJr->getJour());

//                    calcul du taux de résolution des plaintes sensibles
                    $nombrePlainteSensibleTraiteDelais = $repository3->getNombrePlainteSensibleEtOrdinaireTraiteDelais($debut, $fin, $jourTraitementPlainteSensible,1);
                    $nombrePlainteSensibleTraite = $repository3->getNombrePlainteSensibleEtOrdinaireTraite($debut, $fin,1);
                    if ($nombrePlainteSensibleTraite["nbre"] == 0) {
                        $tauxResolutionPlainteSensible = NULL;

                    } else {
                        $tauxResolutionPlainteSensible = array(
                            'valeur' => round(($nombrePlainteSensibleTraiteDelais["nbre"] / $nombrePlainteSensibleTraite["nbre"])*100,2)
                        );
                    }

//                    calcul du taux de résolution des plaintes ordinaire
                    $nombrePlainteOrdinaireTraiteDelais = $repository3->getNombrePlainteSensibleEtOrdinaireTraiteDelais($debut, $fin,$jourTraitementPlainteOrdinaire,0);
                    $nombrePlainteOrdinaireTraite = $repository3->getNombrePlainteSensibleEtOrdinaireTraite($debut, $fin,0);
                    if ($nombrePlainteOrdinaireTraite["nbre"] == 0) {
                        $tauxResolutionPlainteOrdinaire = NULL;

                    } else {
                        $tauxResolutionPlainteOrdinaire = array(
                            'valeur' => round(($nombrePlainteOrdinaireTraiteDelais["nbre"] / $nombrePlainteOrdinaireTraite["nbre"])*100,2)
                        );
                    }



                    $nombreplaintetraiteenretard = $repository3->getNombrePlainteTraiteEnRetard($debut, $fin, $nbreJr->getJour());
//            var_dump($nombrePlainteTraiteDelais); die();

                    $nbreNotification = $repository3->getNombreNotification($debut, $fin);

                    $nombrePlainteResteSansTraitement = $repository->getNombrePlainteResteSansTraitement($debut, $fin);

                    $nombrePlainteInsatisfait = $repository5->getNombrePlainteInsatisfait($debut, $fin);

                    $listeAgences = $repository6->findAll();

                    $NombrePlainteRelativeChefAgence = $repository->getNombrePlainteRelativeChefAgence($debut, $fin);

                    $nombrePlainteRelativeAgentCredit = $repository->getNombrePlainteRelativeAgentCredit($debut, $fin);

                    $nombrePlainteRelativeAgentAccueil = $repository->getNombrePlainteRelativeAgentAccueil($debut, $fin);

                    $nombrePlainteRelativeCaissiere = $repository->getNombrePlainteRelativeCaissiere($debut, $fin);

                    $nombrePlainteRelativeAutresAgent = $repository->getNombrePlainteRelativeAutresAgent($debut, $fin);

                    $frequenceReceptionPlainte = $repository->getNombreFrequenceReceptionPlainte($debut, $fin);

                    $nbrePlainteRecuG = $repository->getNbrePlainteRecueG($debut, $fin);

                    $nbrePlainteTraite = $repository->getNombrePlainteTraite($debut, $fin);

                    $nbrePlainteRecuPeriodeUnAn = $repository->getNbrePlainteRecuPeriodeUnAn($debut, $fin);

                    $nbrePlainteRecuPeriodeAncienClient = $repository->getNbrePlainteRecuPeriodeAncienClient($debut, $fin);

                    $natureplainterecurrente1 = $repository->getListeNaturePlainteRecurrente1($debut, $fin);

                    $natureplainterecurrente2 = $repository->getListeNaturePlainteRecurrente2($debut, $fin);

                    $natureplainterecurrente3 = $repository->getListeNaturePlainteRecurrente3($debut, $fin);


//            $nombrePlainteParObjetPlainte=$repository->getNombrePlainteParObjetPlainte();
                    $nombreClientContacteParSMS = $repository4->getNombreClientContacteParSMS();

                    $nombrePlainteSatisfaitParObjet = $repository->getNombrePlainteSatisfaitParObjetPlainte();

//                var_dump($nombrePlainteParPeriode); die();

                    if ($nombrePlainteParPeriode["nbre"] == 0) {
                        $nbreclientsatisfait = NULL;

                    } else {
                        $nbreclientsatisfait = array(
                            'valeur' => round(($nombrePlainteSatisfaitParPeriode["nbre"] / $nombrePlainteParPeriode["nbre"])*100,2)
                        );
                    }

                    if ($listePervicesPlusSollicites == null) {
                        $listePervicesPlusSollicites = null;
                    }

                    if ($listePervicesPlusIndexes == null) {
                        $listePervicesPlusIndexes = null;
                    }

//                  découpage de la serie par semaine
                    $nbreJour = date_diff(new \DateTime($debut), new \DateTime($fin));
                    if (isset($_POST['graphique']) and $_POST['graphique'] == 1) {
                        $denominateur = 7;
                        $intervalle = 'P7D';
                        $typeGraphique = 'column';
                        $libelleOrdonnee = $translator->trans('semaine (s) ');
                    } elseif (isset($_POST['graphique']) and $_POST['graphique'] == 2) {
                        $denominateur = 1;
                        $intervalle = 'P1D';
                        $typeGraphique = 'line';
                        $libelleOrdonnee = $translator->trans('jour (s) ');

                    } else {
                        $denominateur = 7;
                        $intervalle = 'P7D';
                        $typeGraphique = 'column';
                        $libelleOrdonnee = $translator->trans('semaine (s) ');
                    }
                    $nbreSemaine = round(($nbreJour->days / $denominateur), 0);
                    $i = 1;
                    $dateDebutTraitement = $debut;
//                    $valeurP=array();
//                    $periodeP=array();
                    while ($i <= $nbreSemaine):
                        $day = new \DateTime($dateDebutTraitement);
                        $day->add(new \DateInterval($intervalle));
                        $dayF = $day->format('Y-m-d');
                        $nbrePlainteRecuSemaine = $repository->getNbrePlainteRecueG($dateDebutTraitement, $dayF);
                        $valeurP[] = intval($nbrePlainteRecuSemaine["nbre"]);
                        $periodeP[] = $i;
                        $dateDebutTraitement = $dayF;
                        $i++;
                    endwhile;
//                    Générateur de graphiques plaintes reçues

                    $ob = new Highchart();
                    $ob->chart->renderTo('linechart');  // The #id of the div where to render the chart
                    $ob->chart->type($typeGraphique);
                    $ob->title->text($translator->trans('Nombre de plaintes reçues sur la période'));
//					$ob->chart->style(array('fontFamily'=>'Lucida Grande'));
                    $ob->xAxis->categories($periodeP);

                    $yData = array(
                        array(
                            'labels' => array(
                                'style' => array('color' => '#4572A7')
                            ),
                            'gridLineWidth' => 0,
                            'title' => array(
                                'text' => $translator->trans('Nombre de plantes reçues'),
                                'style' => array('color' => '#4572A7')
                            ),
                        ),
                    );

                    $valeur = intval($nbrePlainteRecuG["nbre"]);
//                   var_dump($valeurP); die();

                    $series = array(
                        array("name" => $libelleOrdonnee, "data" => $valeurP)
                    );

                    $ob->yAxis($yData);
                    $ob->credits->enabled(false);
                    $ob->credits->text("Satis");
                    $ob->credits->href("http://www.dmdsatis.com");
                    $ob->credits->position(array('align' => 'left', 'x' => 20));
                    $ob->legend->enabled(true);
                    $ob->series($series);

//                    var_dump($ob); die();

//                        Générateur graphiques % de plaintes resolues

                    $nombrePlainteTraiteDelaisUneSemaine = $repository3->getNombrePlainteTraiteDelais($debut, $fin, 7);
                    $nombrePlainteTraiteDelaisDeuxSemaine = $repository3->getNombrePlainteTraiteDelaisDeuxSemaines($debut, $fin);
                    $nombrePlainteTraiteDelaisQuatreSemaine = $repository3->getNombrePlainteTraiteDelaisMois($debut, $fin);
                    $nombrePlainteTraiteDelaisPlusQueQuatreSemaine = $repository3->getNombrePlainteTraiteDelaisSuperieurMois($debut, $fin);
//                        Calcul des pourcentages en fonction des plaintes reçues
                    if ($valeur != 0) {
                        $pourcentageNombrePlainteTraiteDelaisUneSemaine = round(($nombrePlainteTraiteDelaisUneSemaine["nbre"] / $valeur) * 100, 2);
                        $pourcentageNombrePlainteTraiteDelaisDeuxSemaine = round(($nombrePlainteTraiteDelaisDeuxSemaine["nbre"] / $valeur) * 100, 2);
                        $pourcentageNombrePlainteTraiteDelaisQuatreSemaine = round(($nombrePlainteTraiteDelaisQuatreSemaine["nbre"] / $valeur) * 100, 2);
                        $pourcentageNombrePlainteTraiteDelaisPlusQueQuatreSemaine = round(($nombrePlainteTraiteDelaisPlusQueQuatreSemaine["nbre"] / $valeur) * 100, 2);

                    } else {
                        $pourcentageNombrePlainteTraiteDelaisUneSemaine = 0;
                        $pourcentageNombrePlainteTraiteDelaisDeuxSemaine = 0;
                        $pourcentageNombrePlainteTraiteDelaisQuatreSemaine = 0;
                        $pourcentageNombrePlainteTraiteDelaisPlusQueQuatreSemaine = 0;
                    }
                    //

                    $ob2 = new Highchart();
                    $ob2->chart->renderTo('pie');
                    $ob2->title->text($translator->trans('Pourcentage des plaintes resolues'));
//					$ob2->chart->style(array('fontFamily'=>'Lucida Grande'));
                    $ob2->plotOptions->pie(array(
                        'allowPointSelect' => true,
                        'cursor' => 'pointer',
                        'dataLabels' => array('enabled' => false),
                        'showInLegend' => true
                    ));
                    $ob2->credits->enabled(false);
                    $data = array(
                        array('name' => '< 1 ' . $translator->trans('semaine'), 'y' => $pourcentageNombrePlainteTraiteDelaisUneSemaine),
                        array('name' => '< 2 ' . $translator->trans('semaines'), 'y' => $pourcentageNombrePlainteTraiteDelaisDeuxSemaine),
                        array('name' => '< 1 ' . $translator->trans('mois'), 'y' => $pourcentageNombrePlainteTraiteDelaisQuatreSemaine),
                        array('name' => '> 1 ' . $translator->trans('mois'), 'y' => $pourcentageNombrePlainteTraiteDelaisPlusQueQuatreSemaine),

                    );
                    $ob2->series(array(array('type' => 'pie', 'name' => $translator->trans('Pourcentage des plaintes resolues'), 'data' => $data)));

                    $nbrePlainteNonSatisfaiteAvecMention = $repository5->getNombrePlainteNonSatisfaitAvecMention();

                    $action=$em->getRepository('UserBundle:DataEvent')->find(28);
                    $event= new SaveComplaintEvent($action,$userEvent);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->render('GplainteBundle:Rapport:rapport.html.twig', array(
                        'periode' => $_POST["debut"] . ' ' . $this->get('translator')->trans('au') . ' ' . $_POST["fin"],
                        'nbreplainterecuG' => $nbrePlainteRecuG,
                        'nbreplaintetraite' => $nbrePlainteTraite,
                        'natureplainterecurrente1' => $natureplainterecurrente1,
                        'natureplainterecurrente2' => $natureplainterecurrente2,
                        'natureplainterecurrente3' => $natureplainterecurrente3,
                        'nombreplaintetraitedelais' => $nombrePlainteTraiteDelais,
                        'nombreplaintetraiteenretard' => $nombreplaintetraiteenretard,
                        'serviceleplusindexes' => $listePervicesPlusIndexes,
                        'nbreplainterecuperiodeunan' => $nbrePlainteRecuPeriodeUnAn,
                        'nbreplainteclientancien' => $nbrePlainteRecuPeriodeAncienClient,
                        'mesurespreventives' => $listeMesuresPreventives,
                        'serviceleplussollicite' => $listePervicesPlusSollicites,
                        'nbreclientsatisfait' => $nbreclientsatisfait,
                        'nbrenotification' => $nbreNotification,
//            'nombreClientContacteParSMS'=>$nombreClientContacteParSMS,
                        'nombreplainterestesanstraitement' => $nombrePlainteResteSansTraitement,
                        'nombrePlainteInsatisfait' => $nombrePlainteInsatisfait,
                        'nombreplaintesatisfaitparobjet' => $nombrePlainteSatisfaitParObjet,
//            'mesurepreventiveparagence'=>$listeMesuresPreventivesParAgence,
                        'nbreplainterelativechefagence' => $NombrePlainteRelativeChefAgence,
                        'nbreplainterelativeagentcredit' => $nombrePlainteRelativeAgentCredit,
                        'nbreplainterelativeagentaccueil' => $nombrePlainteRelativeAgentAccueil,
                        'nbreplainterelativecaissiere' => $nombrePlainteRelativeCaissiere,
                        'nbreplainterelativeautreagent' => $nombrePlainteRelativeAutresAgent,
                        'frequencereceptionplainte' => $frequenceReceptionPlainte,
                        'tauxResolutionPlainteSensible'=>$tauxResolutionPlainteSensible,
                        'tauxResolutionPlainteOrdinaire'=>$tauxResolutionPlainteOrdinaire,
                        'graphe' => $ob,
                        'graphe2' => $ob2,
                        'objectif' => $ListeObjectifs,
                        'etat' => htmlentities($_POST["generale"]),
                        'nbreplaintenonsatisfaiteavecmention'=>$nbrePlainteNonSatisfaiteAvecMention
                    ));

                } elseif (htmlentities($_POST["generale"]) == 2 and isset($_POST["bureau"]) and $_POST["bureau"] != "") {
                    $bureau = $_POST["bureau"];
                    $nbrePlainteRecuG = $repository->getNbrePlainteRecueG($debut, $fin);
                    foreach ($bureau as $k => $bureau):
                        $listeBureaux = $repository2->findOneById($bureau);
                        $nbreplainterecu[] = $repository->getNbrePlainteRecue($debut, $fin, $bureau);

                        $natureplainte[] = $repository->getNaturePlainte($debut, $fin, $bureau);

                        $nbreplainteactivitenonfinance[] = $repository->getNombrePlainteParObjetPlainte($debut, $fin, $bureau, 15);
                        $nbreplaintegestedeplace[] = $repository->getNombrePlainteParObjetPlainte($debut, $fin, $bureau, 12);

                        $nbreplainteharcelement[] = $repository->getNombrePlainteParObjetPlainte($debut, $fin, $bureau, 2);

                        $nbreplainteinteretmalcalcule[] = $repository->getNombrePlainteParObjetPlainte($debut, $fin, $bureau, 13);

                        $nbreplaintetracesserieadministrative[] = $repository->getNombrePlainteParObjetPlainte($debut, $fin, $bureau, 14);

                        $nbreplainteecartdelangage[] = $repository->getNombrePlainteParObjetPlainte($debut, $fin, $bureau, 11);

                        $frequencereceptionplaintebureau[] = $repository->getNombreFrequenceReceptionPlainteBureau($debut, $fin, $bureau);

                        $nombreplainteparperiodebureau = $repository->getNombrePlainteParPeriodeBureau($debut, $fin, $bureau);

                        $nombreplaintesatisfaitparperiodebureau = $repository->getNombrePlainteSatisfaitParPeriodeBureau($debut, $fin, $bureau);

                        $nombreplainterecuegbureau[] = $repository->getNbrePlainteRecueGBureau($debut, $fin, $bureau);

                        $nombreplaintetraitebureau[] = $repository->getNombrePlainteTraiteBureau($debut, $fin, $bureau);

                        $nombreplainterestesanstraitementbureau[] = $repository->getNombrePlainteResteSansTraitementBureau($debut, $fin, $bureau);



//                  calcul du taux de résolution des plaintes sensibles
                    $nombrePlainteSensibleTraiteDelais = $repository3->getNombrePlainteSensibleEtOrdinaireTraiteDelaisBureau($debut, $fin, $jourTraitementPlainteSensible,1,$bureau);
                    $nombrePlainteSensibleTraite = $repository3->getNombrePlainteSensibleEtOrdinaireTraiteBureau($debut, $fin,1,$bureau);
                    if ($nombrePlainteSensibleTraite["nbre"] == 0) {
                        $tauxResolutionPlainteSensible[] = NULL;

                    } else {
                        $tauxResolutionPlainteSensible[] = array(
                            'valeur' => round(($nombrePlainteSensibleTraiteDelais["nbre"] / $nombrePlainteSensibleTraite["nbre"])*100,2)
                        );
                    }

//                    calcul du taux de résolution des plaintes ordinaire
                    $nombrePlainteOrdinaireTraiteDelais = $repository3->getNombrePlainteSensibleEtOrdinaireTraiteDelaisBureau($debut, $fin,$jourTraitementPlainteOrdinaire,0,$bureau);
                    $nombrePlainteOrdinaireTraite = $repository3->getNombrePlainteSensibleEtOrdinaireTraiteBureau($debut, $fin,0,$bureau);
                    if ($nombrePlainteOrdinaireTraite["nbre"] == 0) {
                        $tauxResolutionPlainteOrdinaire[] = NULL;

                    } else {
                        $tauxResolutionPlainteOrdinaire[] = array(
                            'valeur' => round(($nombrePlainteOrdinaireTraiteDelais["nbre"] / $nombrePlainteOrdinaireTraite["nbre"])*100,2)
                        );
                    }



                        $nombreplaintetraitedelaisbureau[] = $repository3->getNombrePlainteTraiteDelaisBureau($debut, $fin, $bureau, $nbreJr->getJour());

                        $nombreplaintetraiteenretardbureau[] = $repository3->getNombrePlainteTraiteEnRetardBureau($debut, $fin, $bureau, $nbreJr->getJour());

                        $natureplainterecurrentebureau1[] = $repository->getListeNaturePlainteRecurrenteBureau1($debut, $fin, $bureau);

                        $natureplainterecurrentebureau2[] = $repository->getListeNaturePlainteRecurrenteBureau2($debut, $fin, $bureau);
                        $natureplainterecurrentebureau3[] = $repository->getListeNaturePlainteRecurrenteBureau3($debut, $fin, $bureau);

                        $nombreplainterelativechefagencebureau[] = $repository->getNombrePlainteRelativeChefAgenceBureau($debut, $fin, $bureau);
                        $nombreplainterelativeagentcreditbureau[] = $repository->getNombrePlainteRelativeAgentCreditBureau($debut, $fin, $bureau);

                        $nombreplainterelativeagentaccueilbureau[] = $repository->getNombrePlainteRelativeAgentAccueilBureau($debut, $fin, $bureau);
                        $nombreplainterelativecaissierebureau[] = $repository->getNombrePlainteRelativeCaissiereBureau($debut, $fin, $bureau);
                        $nombreplainterelativeautresagentbureau[] = $repository->getNombrePlainteRelativeAutresAgentBureau($debut, $fin, $bureau);

                        $nombreplainterecuperiodeunanbureau[] = $repository->getNbrePlainteRecuPeriodeUnAnBureau($debut, $fin, $bureau);
                        $nombreplainterecuperiodeancienclientbureau[] = $repository->getNbrePlainteRecuPeriodeAncienClientBureau($debut, $fin, $bureau);

                        $nombrenotificationbureau[] = $repository3->getNombreNotificationBureau($debut, $fin, $bureau);
                        $nombreplainteinsatisfaitbureau[] = $repository3->getNombrePlainteInsatisfaitBureau($debut, $fin, $bureau);

                        $listeservicesplusindexesbureau[] = $repository->getListeServicesLesPlusIndexesBureau($debut, $fin, $bureau);

                        $listeservicesplussollicitesbureau[] = $repository3->getListeServicesLesPlusSollicitesBureau($debut, $fin, $bureau);
//                    die("bg");

                        if ($nombreplainteparperiodebureau["nbre"] == 0) {
                            $nbreclientsatisfaitBureau[] = NULL;

                        } else {
                            $nbreclientsatisfaitBureau[] = array(
                                'valeur' => round(($nombreplaintesatisfaitparperiodebureau["nbre"] / $nombreplainteparperiodebureau["nbre"])*100,2)
                            );
                        }

                        $bureaux[] = array('bureau' => $listeBureaux->getLibelle());
                        $bureauxid[] = $listeBureaux->getId();

                    endforeach;
                    $valBureauId = implode('-', $bureauxid);


//                  découpage de la serie par semaine
                    $nbreJour = date_diff(new \DateTime($debut), new \DateTime($fin));
                    if (isset($_POST['graphique']) and $_POST['graphique'] == 1) {
                        $denominateur = 7;
                        $intervalle = 'P7D';
                        $typeGraphique = 'column';
                        $libelleOrdonnee = $translator->trans('semaine (s) ');
                    } elseif (isset($_POST['graphique']) and $_POST['graphique'] == 2) {
                        $denominateur = 1;
                        $intervalle = 'P1D';
                        $typeGraphique = 'line';
                        $libelleOrdonnee = $translator->trans('jour (s) ');

                    } else {
                        $denominateur = 7;
                        $intervalle = 'P7D';
                        $typeGraphique = 'column';
                        $libelleOrdonnee = $translator->trans('semaine (s) ');
                    }

                    $nbreSemaine = round(($nbreJour->days / $denominateur), 0);
                    $i = 1;
                    $dateDebutTraitement = $debut;
//                    $valeurP=array();
//                    $periodeP=array();
                    while ($i <= $nbreSemaine):
                        $day = new \DateTime($dateDebutTraitement);
                        $day->add(new \DateInterval($intervalle));
                        $dayF = $day->format('Y-m-d');
                        $nbrePlainteRecuSemaine = $repository->getNbrePlainteRecueG($dateDebutTraitement, $dayF);
                        $valeurP[] = intval($nbrePlainteRecuSemaine["nbre"]);
                        $periodeP[] = $i;
                        $dateDebutTraitement = $dayF;
                        $i++;
                    endwhile;


                    //                    Générateur de graphiques plaintes reçues

                    $ob = new Highchart();
                    $ob->chart->renderTo('linechart');  // The #id of the div where to render the chart
                    $ob->chart->type($typeGraphique);
                    $ob->title->text($translator->trans('Nombre de plaintes reçues sur la période'));
					$ob->chart->style(array('fontFamily'=>'Lucida Grande'));
                    $ob->xAxis->categories($periodeP);

                    $yData = array(
                        array(
                            'labels' => array(
                                'style' => array('color' => '#4572A7')
                            ),
                            'gridLineWidth' => 0,
                            'title' => array(
                                'text' => $translator->trans('Nombre de plantes reçues'),
                                'style' => array('color' => '#4572A7')
                            ),
                        ),
                    );

                    $valeur = intval($nbrePlainteRecuG["nbre"]);
                    $series = array(
                        array("name" => $libelleOrdonnee, "data" => $valeurP),
                    );

                    $ob->yAxis($yData);
                    $ob->credits->enabled(false);
                    $ob->credits->text("Satis");
                    $ob->credits->href("http://www.dmdsatis.com");
                    $ob->credits->position(array('align' => 'left', 'x' => 20));
                    $ob->legend->enabled(true);
                    $ob->series($series);

//                        Générateur graphiques % de plaintes resolues

                    $nombrePlainteTraiteDelaisUneSemaine = $repository3->getNombrePlainteTraiteDelais($debut, $fin, 7);
                    $nombrePlainteTraiteDelaisDeuxSemaine = $repository3->getNombrePlainteTraiteDelaisDeuxSemaines($debut, $fin);
                    $nombrePlainteTraiteDelaisQuatreSemaine = $repository3->getNombrePlainteTraiteDelaisMois($debut, $fin);
                    $nombrePlainteTraiteDelaisPlusQueQuatreSemaine = $repository3->getNombrePlainteTraiteDelaisSuperieurMois($debut, $fin);
//                        Calcul des pourcentages en fonction des plaintes reçues
                    if ($valeur != 0) {
                        $pourcentageNombrePlainteTraiteDelaisUneSemaine = round(($nombrePlainteTraiteDelaisUneSemaine["nbre"] / $valeur) * 100, 2);
                        $pourcentageNombrePlainteTraiteDelaisDeuxSemaine = round(($nombrePlainteTraiteDelaisDeuxSemaine["nbre"] / $valeur) * 100, 2);
                        $pourcentageNombrePlainteTraiteDelaisQuatreSemaine = round(($nombrePlainteTraiteDelaisQuatreSemaine["nbre"] / $valeur) * 100, 2);
                        $pourcentageNombrePlainteTraiteDelaisPlusQueQuatreSemaine = round(($nombrePlainteTraiteDelaisPlusQueQuatreSemaine["nbre"] / $valeur) * 100, 2);

                    } else {
                        $pourcentageNombrePlainteTraiteDelaisUneSemaine = 0;
                        $pourcentageNombrePlainteTraiteDelaisDeuxSemaine = 0;
                        $pourcentageNombrePlainteTraiteDelaisQuatreSemaine = 0;
                        $pourcentageNombrePlainteTraiteDelaisPlusQueQuatreSemaine = 0;
                    }
                    //


                    $ob2 = new Highchart();
                    $ob2->chart->renderTo('pie');
                    $ob2->title->text($translator->trans('Pourcentage des plaintes resolues'));
					$ob2->chart->style(array('fontFamily'=>'Lucida Grande'));
                    $ob2->plotOptions->pie(array(
                        'allowPointSelect' => true,
                        'cursor' => 'pointer',
                        'dataLabels' => array('enabled' => false),
                        'showInLegend' => true
                    ));
                    $ob2->credits->enabled(false);
                    $data = array(
                        array('name' => '< 1 ' . $translator->trans('semaine'), 'y' => $pourcentageNombrePlainteTraiteDelaisUneSemaine),
                        array('name' => '< 2 ' . $translator->trans('semaines'), 'y' => $pourcentageNombrePlainteTraiteDelaisDeuxSemaine),
                        array('name' => '< 1 ' . $translator->trans('mois'), 'y' => $pourcentageNombrePlainteTraiteDelaisQuatreSemaine),
                        array('name' => '> 1 ' . $translator->trans('mois'), 'y' => $pourcentageNombrePlainteTraiteDelaisPlusQueQuatreSemaine),

                    );
                    $ob2->series(array(array('type' => 'pie', 'name' => $translator->trans('Pourcentage des plaintes resolues'), 'data' => $data)));


//                var_dump($natureplainterecurrentebureau1); die();
                    $nbrePlainteNonSatisfaiteAvecMention = $repository5->getNombrePlainteNonSatisfaitAvecMention();

                    $action=$em->getRepository('UserBundle:DataEvent')->find(28);
                    $event= new SaveComplaintEvent($action,$userEvent);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->render('GplainteBundle:Rapport:rapport.html.twig', array(
                        'periode' => $_POST["debut"] . ' '.$translator->trans('au').' ' . $_POST["fin"],
                        'etat' => htmlentities($_POST["generale"]),
                        'bureaux' => $bureaux,
                        'nbrebureaux' => count($bureaux),
                        'nbreplainteactivitenonfinance' => $nbreplainteactivitenonfinance,
                        'nbreplaintegestedeplace' => $nbreplaintegestedeplace,
                        'nbreplainteecartdelangage' => $nbreplainteecartdelangage,
                        'nbreplainteharcelement' => $nbreplainteharcelement,
                        'nbreplainteinteretmalcalcule' => $nbreplainteinteretmalcalcule,
                        'nbreplaintetracesserieadministrative' => $nbreplaintetracesserieadministrative,
                        'nbreplainterecu' => $nbreplainterecu,
                        'frequencereceptionplaintebureau' => $frequencereceptionplaintebureau,
                        'nbreclientsatisfaitbureau' => $nbreclientsatisfaitBureau,
                        'nombreplainterecuegbureau' => $nombreplainterecuegbureau,
                        'nombreplaintetraitebureau' => $nombreplaintetraitebureau,
                        'nombreplainterestesanstraitementbureau' => $nombreplainterestesanstraitementbureau,
                        'nombreplaintetraitedelaisbureau' => $nombreplaintetraitedelaisbureau,
                        'nombreplaintetraiteenretardbureau' => $nombreplaintetraiteenretardbureau,
                        'natureplainterecurrentebureau1' => $natureplainterecurrentebureau1,
                        'natureplainterecurrentebureau2' => $natureplainterecurrentebureau2,
                        'natureplainterecurrentebureau3' => $natureplainterecurrentebureau3,
                        'nombreplainterelativechefagencebureau' => $nombreplainterelativechefagencebureau,
                        'nombreplainterelativeagentcreditbureau' => $nombreplainterelativeagentcreditbureau,
                        'nombreplainterelativeagentaccueilbureau' => $nombreplainterelativeagentaccueilbureau,
                        'nombreplainterelativecaissierebureau' => $nombreplainterelativecaissierebureau,
                        'nombreplainterelativeautresagentbureau' => $nombreplainterelativeautresagentbureau,
                        'nombreplainterecuperiodeunanbureau' => $nombreplainterecuperiodeunanbureau,
                        'nombreplainterecuperiodeancienclientbureau' => $nombreplainterecuperiodeancienclientbureau,
                        'nombrenotificationbureau' => $nombrenotificationbureau,
                        'nombreplainteinsatisfaitbureau' => $nombreplainteinsatisfaitbureau,
                        'servicesplusindexesbureau' => $listeservicesplusindexesbureau,
                        'servicesplussollicitesbureau' => $listeservicesplussollicitesbureau,
                        'tauxResolutionPlainteSensible'=>$tauxResolutionPlainteSensible,
                        'tauxResolutionPlainteOrdinaire'=>$tauxResolutionPlainteOrdinaire,
                        'nature' => $natureplainte,
                        'graphe' => $ob,
                        'graphe2' => $ob2,
                        'objectif' => $ListeObjectifs,
                        'bureauxid' => $valBureauId,
                        'etat' => htmlentities($_POST["generale"]),
                        'nbreplaintenonsatisfaiteavecmention'=>$nbrePlainteNonSatisfaiteAvecMention
                    ));


                }


            } else {
                return $this->redirect($this->generateUrl('gplainte_list_parameters_rapport'));
            }

            return $this->render('GplainteBundle:Rapport:rapport.html.twig', array(
                'periode' => $_POST["debut"] . ' '.$translator->trans('au').' ' . $_POST["fin"],
                'etat' => 3));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }


    }

    public function pdfAction($periode,$etat,$commentaire1,$commentaire2,$bureauxx)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $translator = $this->get('translator');
            $userEvent=$this->container->get('security.context')->getToken()->getUser();
            $repository = $em->getRepository('GplainteBundle:Plainte');
            $repository2 = $em->getRepository('GplainteBundle:Bureau');
            $repository3 = $em->getRepository('GplainteBundle:Affecter');
            $repository4 = $em->getRepository('GplainteBundle:Notification');
            $repository5 = $em->getRepository('GplainteBundle:Satisfaction');
            $repository6 = $em->getRepository('GplainteBundle:Agence');
            $repositoryNormeIndicateur=$em->getRepository('GplainteBundle:NormeIndicateur');
            $ListeObjectifs=$repositoryNormeIndicateur->getNormeIndicateur();

//var_dump("ereer"); die();
            if (isset($periode) && $periode!=0){
                $x=explode(' '.$translator->trans('au').' ',$periode);
                $debut=$x[0];
                $fin=$x[1];
                $datedebut=explode('-',$debut);
                $debut=$datedebut[2].'-'.$datedebut[1].'-'.$datedebut[0];
                $datefin=explode('-',$fin);
                $fin=$datefin[2].'-'.$datefin[1].'-'.$datefin[0];

//            echo $debut.' '.$fin; die();

                $d=date_format(new \DateTime($debut),'d-m-Y');
                $f=date_format(new \DateTime($fin),'d-m-Y');
                $now=date_format(new \DateTime(),'d/m/Y H:i:s');

                $repositoryIndicateur = $em->getRepository('GplainteBundle:NormeIndicateur');
                $nbreJr = $repositoryIndicateur->findOneByMarqueur('efficacite');
                $nbreJrSatisfaction = $repositoryIndicateur->findOneByMarqueur('satisfaction');
                $nbreJrSensible = $repositoryIndicateur->findOneByMarqueur('sensible');

                $jourTraitementPlainteOrdinaire=$nbreJrSatisfaction->getJour();
                $jourTraitementPlainteSensible=$nbreJrSensible->getJour();

                if ($etat==1){
//                $listePervicesPlusSollicites=$repository3->getListeServicesLesPlusSollicites($debut,$fin);
                    $listePervicesPlusIndexes=$repository->getListeServicesLesPlusIndexes($debut,$fin);

                    $listeMesuresPreventives=$repository3->getListeMesurePreventive($debut,$fin);
                    $listeMesuresPreventivesParAgence=$repository3->getListeMesurePreventiveParAgence($debut,$fin);

                    $nombrePlainteParPeriode=$repository->getNombrePlainteParPeriode($debut,$fin);
                    $nombrePlainteSatisfaitParPeriode=$repository->getNombrePlainteSatisfaitParPeriode($debut,$fin);

                    $nombrePlainteTraiteDelais=$repository3->getNombrePlainteTraiteDelais($debut,$fin,$nbreJr);
                    $nombreplaintetraiteenretard=$repository3->getNombrePlainteTraiteEnRetard($debut,$fin,$nbreJr);
//            var_dump($nombrePlainteTraiteDelais); die();

                    $nbreNotification=$repository3->getNombreNotification($debut,$fin);
                    $nombrePlainteResteSansTraitement=$repository->getNombrePlainteResteSansTraitement($debut,$fin);
                    $nombrePlainteInsatisfait=$repository5->getNombrePlainteInsatisfait($debut,$fin);
                    $listeAgences=$repository6->findAll();
                    $NombrePlainteRelativeChefAgence=$repository->getNombrePlainteRelativeChefAgence($debut,$fin);
                    $nombrePlainteRelativeAgentCredit=$repository->getNombrePlainteRelativeAgentCredit($debut,$fin);
                    $nombrePlainteRelativeAgentAccueil=$repository->getNombrePlainteRelativeAgentAccueil($debut,$fin);
                    $nombrePlainteRelativeCaissiere=$repository->getNombrePlainteRelativeCaissiere($debut,$fin);
                    $nombrePlainteRelativeAutresAgent=$repository->getNombrePlainteRelativeAutresAgent($debut,$fin);
                    $frequenceReceptionPlainte=$repository->getNombreFrequenceReceptionPlainte($debut,$fin);
                    $nbrePlainteRecuG=$repository->getNbrePlainteRecueG($debut,$fin);
                    $nbrePlainteTraite=$repository->getNombrePlainteTraite($debut,$fin);
                    $nbrePlainteRecuPeriodeUnAn=$repository->getNbrePlainteRecuPeriodeUnAn($debut,$fin);
                    $nbrePlainteRecuPeriodeAncienClient=$repository->getNbrePlainteRecuPeriodeAncienClient($debut,$fin);
                    $natureplainterecurrente1=$repository->getListeNaturePlainteRecurrente1($debut,$fin);
                    $natureplainterecurrente2=$repository->getListeNaturePlainteRecurrente2($debut,$fin);
                    $natureplainterecurrente3=$repository->getListeNaturePlainteRecurrente3($debut,$fin);

//                    calcul du taux de résolution des plaintes sensibles
                    $nombrePlainteSensibleTraiteDelais = $repository3->getNombrePlainteSensibleEtOrdinaireTraiteDelais($debut, $fin, $jourTraitementPlainteSensible,1);
                    $nombrePlainteSensibleTraite = $repository3->getNombrePlainteSensibleEtOrdinaireTraite($debut, $fin,1);
                    if ($nombrePlainteSensibleTraite["nbre"] == 0) {
                        $tauxResolutionPlainteSensible = NULL;

                    } else {
                        $tauxResolutionPlainteSensible = array(
                            'valeur' => round(($nombrePlainteSensibleTraiteDelais["nbre"] / $nombrePlainteSensibleTraite["nbre"])*100,2)
                        );
                    }

//                    calcul du taux de résolution des plaintes ordinaire
                    $nombrePlainteOrdinaireTraiteDelais = $repository3->getNombrePlainteSensibleEtOrdinaireTraiteDelais($debut, $fin,$jourTraitementPlainteOrdinaire,0);
                    $nombrePlainteOrdinaireTraite = $repository3->getNombrePlainteSensibleEtOrdinaireTraite($debut, $fin,0);
                    if ($nombrePlainteOrdinaireTraite["nbre"] == 0) {
                        $tauxResolutionPlainteOrdinaire = NULL;

                    } else {
                        $tauxResolutionPlainteOrdinaire = array(
                            'valeur' => round(($nombrePlainteOrdinaireTraiteDelais["nbre"] / $nombrePlainteOrdinaireTraite["nbre"])*100,2)
                        );
                    }

//            $nombrePlainteParObjetPlainte=$repository->getNombrePlainteParObjetPlainte();
                    $nombreClientContacteParSMS=$repository4->getNombreClientContacteParSMS();
                    $nombrePlainteSatisfaitParObjet=$repository->getNombrePlainteSatisfaitParObjetPlainte();

//                var_dump($nombrePlainteParPeriode); die();

                    if ($nombrePlainteParPeriode["nbre"]==0){
                        $nbreclientsatisfait=NULL;

                    }else {
                        $nbreclientsatisfait=array(
                            'valeur'=>$nombrePlainteSatisfaitParPeriode["nbre"]/$nombrePlainteParPeriode["nbre"]
                        );
                    }

//                if ($listePervicesPlusSollicites==null){
//                    $listePervicesPlusSollicites=null;
//                }

                    if ($listePervicesPlusIndexes==null){
                        $listePervicesPlusIndexes=null;
                    }


//die("fg");

//                    récupération des objectifs

                    $html=$this->renderView('GplainteBundle:Rapport:test.html.twig', array(
                        'periode'=>$d.' '.$translator->trans('au').' '.$f,
                        'nbreplainterecuG'=>$nbrePlainteRecuG,
                        'nbreplaintetraite'=>$nbrePlainteTraite,
                        'natureplainterecurrente1'=>$natureplainterecurrente1,
                        'natureplainterecurrente2'=>$natureplainterecurrente2,
                        'natureplainterecurrente3'=>$natureplainterecurrente3,
                        'nombreplaintetraitedelais'=>$nombrePlainteTraiteDelais,
                        'nombreplaintetraiteenretard'=>$nombreplaintetraiteenretard,
                        'serviceleplusindexes'=>$listePervicesPlusIndexes,
                        'nbreplainterecuperiodeunan'=>$nbrePlainteRecuPeriodeUnAn,
                        'nbreplainteclientancien'=>$nbrePlainteRecuPeriodeAncienClient,
                        'mesurespreventives'=>$listeMesuresPreventives,
//                    'serviceleplussollicite'=>$listePervicesPlusSollicites,
                        'nbreclientsatisfait'=>$nbreclientsatisfait,
                        'nbrenotification'=>$nbreNotification,
//            'nombreClientContacteParSMS'=>$nombreClientContacteParSMS,
                        'nombreplainterestesanstraitement'=>$nombrePlainteResteSansTraitement,
                        'nombrePlainteInsatisfait'=>$nombrePlainteInsatisfait,
                        'nombreplaintesatisfaitparobjet'=>$nombrePlainteSatisfaitParObjet,
//            'mesurepreventiveparagence'=>$listeMesuresPreventivesParAgence,
                        'nbreplainterelativechefagence'=>$NombrePlainteRelativeChefAgence,
                        'nbreplainterelativeagentcredit'=>$nombrePlainteRelativeAgentCredit,
                        'nbreplainterelativeagentaccueil'=>$nombrePlainteRelativeAgentAccueil,
                        'nbreplainterelativecaissiere'=>$nombrePlainteRelativeCaissiere,
                        'nbreplainterelativeautreagent'=>$nombrePlainteRelativeAutresAgent,
                        'frequencereceptionplainte'=>$frequenceReceptionPlainte,
                        'objectifs'=>$ListeObjectifs,
                        'tauxresolutionplaintesensible'=>$tauxResolutionPlainteSensible,
                        'tauxresolutionplainteordinaire'=>$tauxResolutionPlainteOrdinaire,

                        // 'etat'=>$etat,
                        'now'=>$now,
                        'commentaire1'=>$commentaire1,
                        'commentaire2'=>$commentaire2

                    ));

                   $img= new \Imagick();
                   $svg=  file_get_contents("uploads/graphique/linear.svg");
                   $img->readImageBlob($svg);
//				   $img->writeImage("uploads/graphique/linear.png");
//				    $img->clear();
//					$img->destroy();
                   $img->setImageFormat("png24");

                   $img2= new \Imagick();
                   $svg2=  file_get_contents("uploads/graphique/pie.svg");
                   $img2->readImageBlob($svg2);
				   //$img2->resizeImage(320,240,\Imagick::FILTER_LANCZOS,1);
//				   $img2->writeImage("uploads/graphique/pie.png");
//				   $img2->clear();
//    $img2->destroy();
                  $img2->setImageFormat("png24");
//echo "Bonjour"; die();
                 header('Content-Type: image/png');
				  
				 
//                   $img->writeimage("uploads/graphique/Blank_US_Map.png");
                    file_put_contents("uploads/graphique/linear.png", $img);
                    file_put_contents("uploads/graphique/pie.png", $img2);


                    try{

                    $html2pdf = new \HTML2PDF('L', 'A4', 'fr');
//                    $html2pdf->pdf->IncludeJS("print(true);");
                    $html2pdf->pdf->SetDisplayMode('real');
//                    $html2pdf->setTestTdInOnePage(false);
//                    $html2pdf->pdf->SetDisplayMode('fullpage');
                    $html2pdf->setDefaultFont('Arial');

                    $html2pdf->writeHTML($html);

                        $action=$em->getRepository('UserBundle:DataEvent')->find(29);
                        $event= new SaveComplaintEvent($action,$userEvent);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    $fichier = $html2pdf->Output('rapportSATIS.pdf','D');
//                    print_r($fichier); die();

                }catch(HTML2PDF_exception $e) {
                    echo $e;
                    exit;
                }


                }

//                and isset($_POST["bureau"]) and $_POST["bureau"]!=""
                elseif ($etat==2){
                    $bureaux=split('-',$bureauxx);
                    foreach ($bureaux as $k=>$bureau):
                        $listeBureaux=$repository2->findOneById($bureau);
                        $nbreplainterecu[]=$repository->getNbrePlainteRecue($debut,$fin,$bureau);
                        $natureplainte[]=$repository->getNaturePlainte($debut,$fin,$bureau);

                        $nbreplainteactivitenonfinance[]=$repository->getNombrePlainteParObjetPlainte($debut,$fin,$bureau,15);
                        $nbreplaintegestedeplace[]=$repository->getNombrePlainteParObjetPlainte($debut,$fin,$bureau,12);
                        $nbreplainteharcelement[]=$repository->getNombrePlainteParObjetPlainte($debut,$fin,$bureau,2);
                        $nbreplainteinteretmalcalcule[]=$repository->getNombrePlainteParObjetPlainte($debut,$fin,$bureau,13);
                        $nbreplaintetracesserieadministrative[]=$repository->getNombrePlainteParObjetPlainte($debut,$fin,$bureau,14);
                        $nbreplainteecartdelangage[]=$repository->getNombrePlainteParObjetPlainte($debut,$fin,$bureau,11);
                        $frequencereceptionplaintebureau[]=$repository->getNombreFrequenceReceptionPlainteBureau($debut,$fin,$bureau);
                        $nombreplainteparperiodebureau=$repository->getNombrePlainteParPeriodeBureau($debut,$fin,$bureau);
                        $nombreplaintesatisfaitparperiodebureau=$repository->getNombrePlainteSatisfaitParPeriodeBureau($debut,$fin,$bureau);
                        $nombreplainterecuegbureau[]=$repository->getNbrePlainteRecueGBureau($debut,$fin,$bureau);
                        $nombreplaintetraitebureau[]=$repository->getNombrePlainteTraiteBureau($debut,$fin,$bureau);
                        $nombreplainterestesanstraitementbureau[]=$repository->getNombrePlainteResteSansTraitementBureau($debut,$fin,$bureau);
                        $nombreplaintetraitedelaisbureau[]=$repository3->getNombrePlainteTraiteDelaisBureau($debut,$fin,$bureau,$nbreJr->getJour());
                        $nombreplaintetraiteenretardbureau[]=$repository3->getNombrePlainteTraiteEnRetardBureau($debut,$fin,$bureau,$nbreJr->getJour());
                        $natureplainterecurrentebureau1[]=$repository->getListeNaturePlainteRecurrenteBureau1($debut,$fin,$bureau);
                        $natureplainterecurrentebureau2[]=$repository->getListeNaturePlainteRecurrenteBureau2($debut,$fin,$bureau);
                        $natureplainterecurrentebureau3[]=$repository->getListeNaturePlainteRecurrenteBureau3($debut,$fin,$bureau);
                        $nombreplainterelativechefagencebureau[]=$repository->getNombrePlainteRelativeChefAgenceBureau($debut,$fin,$bureau);
                        $nombreplainterelativeagentcreditbureau[]=$repository->getNombrePlainteRelativeAgentCreditBureau($debut,$fin,$bureau);
                        $nombreplainterelativeagentaccueilbureau[]=$repository->getNombrePlainteRelativeAgentAccueilBureau($debut,$fin,$bureau);
                        $nombreplainterelativecaissierebureau[]=$repository->getNombrePlainteRelativeCaissiereBureau($debut,$fin,$bureau);
                        $nombreplainterelativeautresagentbureau[]=$repository->getNombrePlainteRelativeAutresAgentBureau($debut,$fin,$bureau);
                        $nombreplainterecuperiodeunanbureau[]=$repository->getNbrePlainteRecuPeriodeUnAnBureau($debut,$fin,$bureau);
                        $nombreplainterecuperiodeancienclientbureau[]=$repository->getNbrePlainteRecuPeriodeAncienClientBureau($debut,$fin,$bureau);
                        $nombrenotificationbureau[]=$repository3->getNombreNotificationBureau($debut,$fin,$bureau);
                        $nombreplainteinsatisfaitbureau[]=$repository3->getNombrePlainteInsatisfaitBureau($debut,$fin,$bureau);
                        $listeservicesplusindexesbureau[]=$repository->getListeServicesLesPlusIndexesBureau($debut,$fin,$bureau);
                        $listeservicesplussollicitesbureau[]=$repository3->getListeServicesLesPlusSollicitesBureau($debut,$fin,$bureau);

//                  calcul du taux de résolution des plaintes sensibles
                        $nombrePlainteSensibleTraiteDelais = $repository3->getNombrePlainteSensibleEtOrdinaireTraiteDelaisBureau($debut, $fin, $jourTraitementPlainteSensible,1,$bureau);
                        $nombrePlainteSensibleTraite = $repository3->getNombrePlainteSensibleEtOrdinaireTraiteBureau($debut, $fin,1,$bureau);
                        if ($nombrePlainteSensibleTraite["nbre"] == 0) {
                            $tauxResolutionPlainteSensible[] = NULL;

                        } else {
                            $tauxResolutionPlainteSensible[] = array(
                                'valeur' => round(($nombrePlainteSensibleTraiteDelais["nbre"] / $nombrePlainteSensibleTraite["nbre"])*100,2)
                            );
                        }

//                    calcul du taux de résolution des plaintes ordinaire
                        $nombrePlainteOrdinaireTraiteDelais = $repository3->getNombrePlainteSensibleEtOrdinaireTraiteDelaisBureau($debut, $fin,$jourTraitementPlainteOrdinaire,0,$bureau);
                        $nombrePlainteOrdinaireTraite = $repository3->getNombrePlainteSensibleEtOrdinaireTraiteBureau($debut, $fin,0,$bureau);
                        if ($nombrePlainteOrdinaireTraite["nbre"] == 0) {
                            $tauxResolutionPlainteOrdinaire[] = NULL;

                        } else {
                            $tauxResolutionPlainteOrdinaire[] = array(
                                'valeur' => round(($nombrePlainteOrdinaireTraiteDelais["nbre"] / $nombrePlainteOrdinaireTraite["nbre"])*100,2)
                            );
                        }

                        if ($nombreplainteparperiodebureau["nbre"]==0){
                            $nbreclientsatisfaitBureau[]=NULL;

                        }else {
                            $nbreclientsatisfaitBureau[]=array(
                                'valeur'=>round(($nombreplaintesatisfaitparperiodebureau["nbre"]/$nombreplainteparperiodebureau["nbre"])*100,2)
                            );
                        }
                        $bureauxw[]=array('bureau'=>$listeBureaux->getLibelle());

                    endforeach;
//                var_dump($bureauxw); die();

                    $html=$this->renderView('GplainteBundle:Rapport:pdf.html.twig', array(
                        'periode'=>$d.' '.$translator->trans('au').' '.$f,
                        'etat'=>$etat,
                        'bureaux'=>$bureauxw,
                        'nbrebureaux'=>count($bureauxw),
                        'nbreplainteactivitenonfinance'=>$nbreplainteactivitenonfinance,
                        'nbreplaintegestedeplace'=>$nbreplaintegestedeplace,
                        'nbreplainteecartdelangage'=>$nbreplainteecartdelangage,
                        'nbreplainteharcelement'=>$nbreplainteharcelement,
                        'nbreplainteinteretmalcalcule'=>$nbreplainteinteretmalcalcule,
                        'nbreplaintetracesserieadministrative'=>$nbreplaintetracesserieadministrative,
                        'nbreplainterecu'=>$nbreplainterecu,
                        'frequencereceptionplaintebureau'=>$frequencereceptionplaintebureau,
                        'nbreclientsatisfaitbureau'=>$nbreclientsatisfaitBureau,
                        'nombreplainterecuegbureau'=>$nombreplainterecuegbureau,
                        'nombreplaintetraitebureau'=>$nombreplaintetraitebureau,
                        'nombreplainterestesanstraitementbureau'=>$nombreplainterestesanstraitementbureau,
                        'nombreplaintetraitedelaisbureau'=>$nombreplaintetraitedelaisbureau,
                        'nombreplaintetraiteenretardbureau'=>$nombreplaintetraiteenretardbureau,
                        'natureplainterecurrentebureau1'=>$natureplainterecurrentebureau1,
                        'natureplainterecurrentebureau2'=>$natureplainterecurrentebureau2,
                        'natureplainterecurrentebureau3'=>$natureplainterecurrentebureau3,
                        'nombreplainterelativechefagencebureau'=>$nombreplainterelativechefagencebureau,
                        'nombreplainterelativeagentcreditbureau'=>$nombreplainterelativeagentcreditbureau,
                        'nombreplainterelativeagentaccueilbureau'=>$nombreplainterelativeagentaccueilbureau,
                        'nombreplainterelativecaissierebureau'=>$nombreplainterelativecaissierebureau,
                        'nombreplainterelativeautresagentbureau'=>$nombreplainterelativeautresagentbureau,
                        'nombreplainterecuperiodeunanbureau'=>$nombreplainterecuperiodeunanbureau,
                        'nombreplainterecuperiodeancienclientbureau'=>$nombreplainterecuperiodeancienclientbureau,
                        'nombrenotificationbureau'=>$nombrenotificationbureau,
                        'nombreplainteinsatisfaitbureau'=>$nombreplainteinsatisfaitbureau,
                        'servicesplusindexesbureau'=>$listeservicesplusindexesbureau,
                        'servicesplussollicitesbureau'=>$listeservicesplussollicitesbureau,
                        'nature'=>$natureplainte,
                        'tauxresolutionplainteordinaire'=>$tauxResolutionPlainteOrdinaire,
                        'tauxresolutionplaintesensible'=>$tauxResolutionPlainteSensible,
                        'now'=>$now,
                        'objectifs'=>$ListeObjectifs,
                        'commentaire1'=>$commentaire1,
                        'commentaire2'=>$commentaire2
                    ));


                    $img= new \Imagick();
                    $svg=  file_get_contents("uploads/graphique/linear.svg");
                    $img->readimageblob($svg);
                    $img->setimageformat("png24");

                    $img2= new \Imagick();
                    $svg2=  file_get_contents("uploads/graphique/pie.svg");
                    $img2->readimageblob($svg2);
                    $img2->setimageformat("png24");

                    header('Content-Type: image/png');
                    file_put_contents("uploads/graphique/linear.png", $img);
                    file_put_contents("uploads/graphique/pie.png", $img2);


                    try{

                        $html2pdf = new \HTML2PDF('L', 'A4', 'fr');
//                    $html2pdf->pdf->IncludeJS("print(true);");
                        $html2pdf->pdf->SetDisplayMode('real');
                        $html2pdf->setDefaultFont('Arial');
                        $html2pdf->writeHTML($html);
                        $fichier = $html2pdf->Output('rapportSATIS'.'.pdf','D');

                        $action=$em->getRepository('UserBundle:DataEvent')->find(29);
                        $event= new SaveComplaintEvent($action,$userEvent);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        print_r($fichier); die();

                    }catch(HTML2PDF_exception $e) {
                        echo $e;
                        exit;
                    }


                }


            }

            return $this->render('GplainteBundle:Rapport:rapport.html.twig', array(
                'periode'=>$_POST["debut"].' '.$translator->trans('au').' '.$_POST["fin"],
                'etat'=>3));


            $html=$this->renderView("GplainteBundle:Rapport:pdf.html.twig",array('debut'=>$debut,'fin'=>$fin));
            $html2pdf = new \HTML2PDF('P','A4','fr');
            $html2pdf->pdf->SetDisplayMode('real');
            $html2pdf->writeHTML($html, isset($_GET['vuehtml']));
            $fichier = $html2pdf->Output('rapport'.'.pdf');
            print_r($fichier); die();

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }



    }
    public function deleteClientAction()
    {
        return array(
                // ...
            );    }

    public function createFileAction(){
		//var_dump($_POST['inputSvg2']); die();
        file_put_contents("uploads/graphique/linear.svg", $_POST['inputSvg']);
        file_put_contents("uploads/graphique/pie.svg", $_POST['inputSvg2']);
        $periode=$_POST['periode'];
        $etat=$_POST['etat'];


        if (isset($_POST["motif_retard2"]) and $_POST["motif_retard2"]!=''){
            $commentaire1=$_POST["motif_retard2"];
        }else{
            $commentaire1=0;
        }

        if (isset($_POST["motif_synthese_generale2"]) and $_POST["motif_synthese_generale2"]!=''){
            $commentaire2=$_POST["motif_synthese_generale2"];
        }else{
            $commentaire2=0;
        }

       if (isset($_POST["bureau"]) and $_POST["bureau"]!=''){
           $bureaux=$_POST["bureau"];
       }else{
           $bureaux=0;
       }

//        $this->pdf($periode,$etat);
        return $this->redirect($this->generateUrl('gplainte_edit_rapport_pdf',array('periode'=>$periode,'etat'=>$etat,'commentaire1'=>$commentaire1,'commentaire2'=>$commentaire2,'bureauxx'=>$bureaux)));
//        return new Response(" ");
    }

}

