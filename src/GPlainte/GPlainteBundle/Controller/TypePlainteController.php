<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GPlainte\GPlainteBundle\Entity\Choix;
use GPlainte\GPlainteBundle\Form\ChoixType;
use GPlainte\GPlainteBundle\Form\ChoixHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

class TypePlainteController extends Controller
{

     public function addChoixAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $user = $this->container->get('security.context')->getToken()->getUser();

            $repository = $em->getRepository('GplainteBundle:Choix');

            $listchoix = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            if ($val=="afficherForm"){
                $choix=$repository->find($id);
                $form = $this->createForm(new ChoixType(), $choix);
                $val="modifier";

            }else
                if ($val=="modifier"){} elseif ($val=="supprimer"){
                    $choix = new Choix();
                    $form = $this->createForm(new ChoixType(), $choix);
                }else{
                    $choix = new Choix();
                    $form = $this->createForm(new ChoixType(), $choix);
                    $val="ajouter";
                }
            $entiteSuppr = $em->getRepository('GplainteBundle:Choix')->find($id);

            if ($val=="ajouter"){
                $choix = new Choix();
                $form = $this->createForm(new ChoixType(), $choix);
                $formHandler = new ChoixHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $nom=$data['libelle'];
                $recup=  $em->getRepository('GplainteBundle:Choix')->findByLibelle($nom);



                if (count($recup)== 0){

                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:Choix');
                        $listchoix = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(60);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:TypePlainte:addTypePlainte.html.twig',array('choix' => $form->createView(),'msg'=>1,
                            'list'=>$listchoix,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));

                    }

                } else {
                    $repository = $em->getRepository('GplainteBundle:Choix');
                    $listchoix = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:TypePlainte:addTypePlainte.html.twig',array('choix' => $form->createView(),'msgErr'=>1,
                        'list'=>$listchoix,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }


            if ($val=="modifier" && $id!=0){

                if( !$choix = $em->getRepository('GplainteBundle:Choix')->find($id) )
                {
                    $repository = $em->getRepository('GplainteBundle:Choix');
                    $listchoix = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:TypePlainte:addTypePlainte.html.twig',array('choix' => $form->createView(),'msgErr'=>"Ce type plainte n'existe pas",
                        'list'=>$listchoix,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

                $form = $this->createForm(new ChoixType(), $choix);
                $formHandler = new ChoixHandler($form, $this->get('request'), $this->getDoctrine()->getManager());

                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $nom=$data['libelle'];
                $recup=  $em->getRepository('GplainteBundle:Choix')->findByLibelle($nom);

                if (count($recup)== 0){

                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:Choix');
                        $listchoix = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(61);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:TypePlainte:addTypePlainte.html.twig',array('choix' => $form->createView(),'msg'=>2,
                            'list'=>$listchoix,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }
                }else {
                    $repository = $em->getRepository('GplainteBundle:Choix');
                    $listchoix = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:TypePlainte:addTypePlainte.html.twig',array('choix' => $form->createView(),'msgErr'=>1,
                        'list'=>$listchoix,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

            }

            $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(63);
            $event= new SaveComplaintEvent($action,$user);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:TypePlainte:addTypePlainte.html.twig',array('choix' => $form->createView(),
                'list'=>$listchoix,
                'val'=>$val,
                'id'=>$id,
                'suppr'=>$entiteSuppr
            ));

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

   
    }


    public function deleteChoixAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GplainteBundle:Choix')->find($id);
            $user = $this->container->get('security.context')->getToken()->getUser();

//echo "Bonjour"; die();
            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_choix',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(62);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->redirect($this->generateUrl('gplainte_add_choix',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){

                return $this->redirect($this->generateUrl('gplainte_add_choix',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:Choix');
            $listchoix = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $choix = new Choix();
            $form = $this->createForm(new ChoixType(), $choix);

            return $this->render('GplainteBundle:TypePlainte:addTypePlainte.html.twig', array(
                'choix' => $form->createView(),
                'list'=>$listchoix,
                'val'=>$val,
                'id'=>$id
            ));

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

}
