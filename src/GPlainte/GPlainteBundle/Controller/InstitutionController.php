<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GPlainte\GPlainteBundle\Entity\Institution;
use GPlainte\GPlainteBundle\Form\InstitutionType;
use GPlainte\GPlainteBundle\Form\InstitutionHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

class InstitutionController extends Controller
{

    public function showInstitutionAction()
    {
        return array(
                // ...
            );    }

    
    public function addInstitutionAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Institution');
            $userEvent=$this->container->get('security.context')->getToken()->getUser();

            $listinstitution = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            if ($val=="afficherForm"){
                $institution=$repository->find($id);
                $form = $this->createForm(new InstitutionType(), $institution);
                $val="modifier";

            }else
                if ($val=="modifier"){} elseif ($val=="supprimer"){
                    $institution = new Institution();
                    $form = $this->createForm(new InstitutionType(), $institution);
                }else{
                    $institution = new Institution();
                    $form = $this->createForm(new InstitutionType(), $institution);
                    $val="ajouter";
                }
            $entiteSuppr = $em->getRepository('GplainteBundle:Institution')->find($id);


            if ($val=="ajouter"){
                $institution = new Institution();
                $form = $this->createForm(new InstitutionType(), $institution);
                $formHandler = new InstitutionHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $denominationSociale=$data['denominationSociale'];
                $recup=  $em->getRepository('GplainteBundle:Institution')->findByDenominationSociale($denominationSociale);
                $nbre=  $em->getRepository('GplainteBundle:Institution')->findAll();
//        var_dump($data['file']); die();
//         and $nbre==0
                if (count($nbre)==0){
                    if (count($recup)== 0){

                        if($formHandler->process())
                        {
                            $repository = $em->getRepository('GplainteBundle:Institution');
                            $listinstitution = $repository->findBy(
                                array(),
                                array('id'=>'DESC')
                            );
                            return $this->render('GplainteBundle:Institution:addInstitution.html.twig',array('institution' => $form->createView(),'msg'=>1,
                                'list'=>$listinstitution,
                                'val'=>$val,
                                'id'=>$id,
                                'suppr'=>$entiteSuppr
                            ));

                        }

                    } else {
                        $repository = $em->getRepository('GplainteBundle:Institution');
                        $listinstitution = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );
                        return $this->render('GplainteBundle:Institution:addInstitution.html.twig',array('institution' => $form->createView(),'msgErr'=>1,
                            'list'=>$listinstitution,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }
                }

            }

            if ($val=="modifier" && $id!=0){

                if( !$institution = $em->getRepository('GplainteBundle:Institution')->find($id) )
                {
                    $repository = $em->getRepository('GplainteBundle:Institution');
                    $listinstitution = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Institution:addInstitution.html.twig',array('institution' => $form->createView(),'msgErr'=>"Ce service n'existe pas",
                        'list'=>$listinstitution,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

                $form = $this->createForm(new InstitutionType(), $institution);
                $formHandler = new InstitutionHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                if($formHandler->process())
                {
                    $repository = $em->getRepository('GplainteBundle:Institution');
                    $listinstitution = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(31);
                    $event= new SaveComplaintEvent($action,$userEvent);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->render('GplainteBundle:Institution:addInstitution.html.twig',array('institution' => $form->createView(),'msg'=>2,
                        'list'=>$listinstitution,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }

            return $this->render('GplainteBundle:Institution:addInstitution.html.twig',array('institution' => $form->createView(),
                'list'=>$listinstitution,
                'val'=>$val,
                'id'=>$id,
                'suppr'=>$entiteSuppr
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


    public function deleteInstitutionAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GplainteBundle:Institution')->find($id);

            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_institution',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();
                    return $this->redirect($this->generateUrl('gplainte_add_institution',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){

                return $this->redirect($this->generateUrl('gplainte_add_institution',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:Institution');
            $listinstitution = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $institution = new Institution();
            $form = $this->createForm(new InstitutionType(), $institution);

            return $this->render('GplainteBundle:Institution:addInstitution.html.twig', array(
                'institution' => $form->createView(),
                'list'=>$listinstitution,
                'val'=>$val,
                'id'=>$id
            ));

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

}
