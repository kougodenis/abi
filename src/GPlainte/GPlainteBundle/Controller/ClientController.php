<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use GPlainte\GPlainteBundle\Entity\Client;
use GPlainte\GPlainteBundle\Form\ClientType;
use GPlainte\GPlainteBundle\Form\ClientHandler;
use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

class ClientController extends Controller
{

    public function showAgentAction()
    {




    }


    /**
     * @param $val
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addClientAction($val,$id)
    {

        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Client');
            $user = $this->container->get('security.context')->getToken()->getUser();


            $listclient = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            if ($val=="AjouterClient"){

                $client = new Client();
                $form = $this->createForm(new ClientType(), $client);
                $formHandler = new ClientHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $nom=$data['nom'];
                $prenom=$data['prenom'];
                $numdossier=$data['numDossier'];
                $telephone1=$data['telephone1'];
                $recup=  $em->getRepository('GplainteBundle:Client')->findBy(array('nom'=>$nom,'prenom'=>$prenom));
                $recupTelephone=  $em->getRepository('GplainteBundle:Client')->findByTelephone1($telephone1);
                $recupNumdossier=  $em->getRepository('GplainteBundle:Client')->findByNumDossier($numdossier);

                if (count($recupTelephone)!= 0){
                    $repository = $em->getRepository('GplainteBundle:Client');
                    $listclient = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Client:addClient.html.twig',array('client' => $form->createView(),'msgErr'=>2,
                        'list'=>$listclient,
                        'val'=>$val,
                        'id'=>$id,

                    ));
                }elseif (count($recupNumdossier)!= 0) {
                    $repository = $em->getRepository('GplainteBundle:Client');
                    $listclient = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Client:addClient.html.twig',array('client' => $form->createView(),'msgErr'=>3,
                        'list'=>$listclient,
                        'val'=>$val,
                        'id'=>$id,

                    ));
                }elseif (count($recup)!= 0){
                    return $this->redirect($this->generateUrl('gplainte_add_client', array('msgErr'=>1)));
                }else{
                    if ($formHandler->process()) {

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(52);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->redirect($this->generateUrl('gplainte_add_plainte'));
                    }
                }

            }
            if ($val=="afficherForm"){
                $client=$repository->find($id);
                $form = $this->createForm(new ClientType(), $client);
                $val="modifier";

            }else
                if ($val=="modifier"){} elseif ($val=="supprimer"){
                    $client = new Client();
                    $form = $this->createForm(new ClientType(), $client);
                }else{
                    $client = new Client();
                    $form = $this->createForm(new ClientType(), $client);
                    $val="ajouter";
                }
            $entiteSuppr = $em->getRepository('GplainteBundle:Client')->find($id);

            if ($val=="ajouter"){

                $client = new Client();
                $form = $this->createForm(new ClientType(), $client);
                $formHandler = new ClientHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $nom=$data['nom'];
                $prenom=$data['prenom'];
//                $recup=  $em->getRepository('GplainteBundle:Client')->findBy(array('nom'=>$nom,'prenom'=>$prenom));

                $numdossier=$data['numDossier'];
                $telephone1=$data['telephone1'];
                $recup=  $em->getRepository('GplainteBundle:Client')->findBy(array('nom'=>$nom,'prenom'=>$prenom));
                $recupTelephone=  $em->getRepository('GplainteBundle:Client')->findByTelephone1($telephone1);
                $recupNumdossier=  $em->getRepository('GplainteBundle:Client')->findByNumDossier($numdossier);


                if (count($recupTelephone)!= 0){

                    $repository = $em->getRepository('GplainteBundle:Client');
                    $listclient = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Client:addClient.html.twig',array('client' => $form->createView(),'msgErr'=>2,
                        'list'=>$listclient,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }elseif (count($recupNumdossier)!= 0) {
                    $repository = $em->getRepository('GplainteBundle:Client');
                    $listclient = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Client:addClient.html.twig',array('client' => $form->createView(),'msgErr'=>3,
                        'list'=>$listclient,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }elseif (count($recup)!= 0){
                    $repository = $em->getRepository('GplainteBundle:Client');
                    $listclient = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Client:addClient.html.twig',array('client' => $form->createView(),'msgErr'=>1,
                        'list'=>$listclient,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }else{
                    if ($formHandler->process()) {
                        $repository = $em->getRepository('GplainteBundle:Client');
                        $listclient = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(52);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Client:addClient.html.twig',array('client' => $form->createView(),'msg'=>1,
                            'list'=>$listclient,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }
                }


            }


            if ($val=="modifier" && $id!=0){

                if( !$client = $em->getRepository('GplainteBundle:Client')->find($id) )
                {
                    $repository = $em->getRepository('GplainteBundle:Client');
                    $listclient = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Client:addClient.html.twig',array('client' => $form->createView(),'msgErr'=>"Ce client n'existe pas",
                        'list'=>$listclient,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

                $form = $this->createForm(new ClientType(), $client);
                $formHandler = new ClientHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $nom=$data['nom'];
                $prenom=$data['prenom'];
                $recup=  $em->getRepository('GplainteBundle:Client')->findBy(array('nom'=>$nom,'prenom'=>$prenom));
//            $recup2=  $em->getRepository('GplainteBundle:Client')->findByPrenom($prenom);


                if (count($recup)== 1){
                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:Client');
                        $listclient = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(53);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Client:addClient.html.twig',array('client' => $form->createView(),'msg'=>2,
                            'list'=>$listclient,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }
                }
            }

            $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(55);
            $event= new SaveComplaintEvent($action,$user);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:Client:addClient.html.twig',array('client' => $form->createView(),
                'list'=>$listclient,
                'val'=>$val,
                'id'=>$id,
                'suppr'=>$entiteSuppr
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }


    }


    public function searchClientAction($id){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();

            if (isset($_GET['idp']) && $_GET['idp']!=0){
                $repository = $em->getRepository('GplainteBundle:Client')->findOneById($_GET['idp']);
                $telephone=$repository->getTelephone1();
                $numdossier=$repository->getNumDossier();
                $tab=array('tel'=>$telephone,'num'=>$numdossier);
                $tab=$telephone.'/'.$numdossier;

                return new Response($tab);}
            else{
                $tab=''.'/'.'';
                return new Response($tab);
            }
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

    public function searchObjetAction($id){

        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $niveaux = $em->getRepository('GplainteBundle:Niveau')->findByChoix($_GET['idp']);
//        echo $niveaux[0]->getLibelle(); die();
            $options='';
            foreach($niveaux as $niveaux):
                $options.="<option value=".$niveaux->getId().">".$niveaux->getLibelle()."</option>";
            endforeach;
//        var_dump($options); die();
            return new Response($options);
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


    public function addImportClientAction($val)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Client');
            $userEvent = $this->container->get('security.context')->getToken()->getUser();

            $client = new Client();

            if ($val==='import'){
                $request=$this->getRequest();
                $file=$request->files->get('excel');
                $excelObj = $this->get('phpexcel')->createPHPExcelObject($file);
                $excelObj->setActiveSheetIndex(0);

                $sheetData = $excelObj->getActiveSheet()->toArray(null,true,true,true);
                $nb_ligne = count($sheetData);
//            echo $nb_ligne; die();
                if (isset($sheetData[3]['K']) && $sheetData[3]['K']=='satis') {
                    for( $q=4; $q <= $nb_ligne; $q++ ){

                        $nom=$sheetData[$q]['B'];
                        $prenom=$sheetData[$q]['C'];
                        $raisonsocial=$sheetData[$q]['D'];
                        $email=$sheetData[$q]['E'];
                        $numdossier=$sheetData[$q]['F'];
                        $ville=$sheetData[$q]['G'];
                        $quartier=$sheetData[$q]['H'];
                        $telephone=$sheetData[$q]['I'];
                        $xx[]=$telephone;
                        $recup=  $em->getRepository('GplainteBundle:Client')->findBy(array('nom'=>$nom,'prenom'=>$prenom));
                        if (count($recup)==0 && $numdossier!=null && $nom!=null && $prenom!=null && $ville!=null && $quartier!=null && $telephone!=null){
                            $client->setNom($nom);
                            $client->setPrenom($prenom);
                            $client->setRaisonSociale($raisonsocial);
                            $client->setEmail($email);
                            $client->setNumDossier($numdossier);
                            $client->setVille($ville);
                            $client->setQuartier($quartier);
                            $client->setTelephone1($telephone);
//                    $xxx[]=$prenom;

                            $em->persist($client);

                        }
                        $em->flush();
                        $em->clear();

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(83);
                        $event= new SaveComplaintEvent($action,$userEvent);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    }
                }else{
                    return $this->render('GplainteBundle:Client:import.html.twig', array(
                        'msgErr'=>'Veuillez selectionner la bonne feuille'
                    ));
                }
//            var_dump($xx); die();




//var_dump($xxx); die();


                return $this->render('GplainteBundle:Client:import.html.twig', array(
                    'msg'=>'Enregistrement effectué avec succès'
                ));
            }

            return $this->render('GplainteBundle:Client:import.html.twig', array(
            ));

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

    public function deleteAgentAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em = $this->getDoctrine()->getManager();
            $user = $this->container->get('security.context')->getToken()->getUser();

            $entity = $em->getRepository('GplainteBundle:Agent')->find($id);

            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_agent',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(54);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->redirect($this->generateUrl('gplainte_add_agent',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){

                return $this->redirect($this->generateUrl('gplainte_add_agent',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:Agent');
            $listagent = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $agent = new Agent();
            $form = $this->createForm(new AgentType(), $agent);

            return $this->render('GplainteBundle:Agent:addAgent.html.twig', array(
                'service' => $form->createView(),
                'list'=>$listagent,
                'val'=>$val,
                'id'=>$id
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }



    }

}
