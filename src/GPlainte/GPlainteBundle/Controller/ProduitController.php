<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GPlainte\GPlainteBundle\Entity\Produit;
use GPlainte\GPlainteBundle\Form\ProduitType;
use GPlainte\GPlainteBundle\Form\ProduitHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

class ProduitController extends Controller
{

    public function showProduitAction()
    {
        return array(
                // ...
            );    }


    public function addProduitAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Produit');
            $user = $this->container->get('security.context')->getToken()->getUser();


            $listproduit = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            if ($val=="afficherForm"){
                $produit=$repository->find($id);
                $form = $this->createForm(new ProduitType(), $produit);
                $val="modifier";

            }else
                if ($val=="modifier"){} elseif ($val=="supprimer"){
                    $produit = new Produit();
                    $form = $this->createForm(new ProduitType(), $produit);
                }else{
                    $produit = new Produit();
                    $form = $this->createForm(new ProduitType(), $produit);
                    $val="ajouter";
                }
            $entiteSuppr = $em->getRepository('GplainteBundle:Produit')->find($id);

            if ($val=="ajouter"){
                $produit = new Produit();
                $form = $this->createForm(new ProduitType(), $produit);
                $formHandler = new ProduitHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $nom=$data['libelle'];
                $recup=  $em->getRepository('GplainteBundle:Produit')->findByLibelle($nom);



                if (count($recup)== 0){

                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:Produit');
                        $listproduit = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(56);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Produit:addProduit.html.twig',array('produit' => $form->createView(),'msg'=>1,
                            'list'=>$listproduit,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));

                    }

                } else {
                    $repository = $em->getRepository('GplainteBundle:Produit');
                    $listproduit = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Produit:addProduit.html.twig',array('produit' => $form->createView(),'msgErr'=>1,
                        'list'=>$listproduit,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }


            if ($val=="modifier" && $id!=0){

                if( !$produit = $em->getRepository('GplainteBundle:Produit')->find($id) )
                {
                    $repository = $em->getRepository('GplainteBundle:Produit');
                    $listproduit = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Produit:addProduit.html.twig',array('produit' => $form->createView(),'msgErr'=>"Cet produit n'existe pas",
                        'list'=>$listproduit,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

                $form = $this->createForm(new ProduitType(), $produit);
                $formHandler = new ProduitHandler($form, $this->get('request'), $this->getDoctrine()->getManager());

                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $nom=$data['libelle'];
                $recup=  $em->getRepository('GplainteBundle:Produit')->findByLibelle($nom);

                if (count($recup)== 0){

                    if($formHandler->process())
                    {
                        $repository = $em->getRepository('GplainteBundle:Produit');
                        $listproduit = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(57);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Produit:addProduit.html.twig',array('produit' => $form->createView(),'msg'=>2,
                            'list'=>$listproduit,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));
                    }
                }else {
                    $repository = $em->getRepository('GplainteBundle:Produit');
                    $listproduit = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Produit:addProduit.html.twig',array('produit' => $form->createView(),'msgErr'=>1,
                        'list'=>$listproduit,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

            }

            $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(59);
            $event= new SaveComplaintEvent($action,$user);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:Produit:addProduit.html.twig',array('produit' => $form->createView(),
                'list'=>$listproduit,
                'val'=>$val,
                'id'=>$id,
                'suppr'=>$entiteSuppr
            ));

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

   
    }


    public function deleteProduitAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GplainteBundle:Produit')->find($id);
            $user = $this->container->get('security.context')->getToken()->getUser();

            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_produit',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();
                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(58);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);
                    return $this->redirect($this->generateUrl('gplainte_add_produit',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){

                return $this->redirect($this->generateUrl('gplainte_add_produit',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:Produit');
            $listproduit = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $produit = new Produit();
            $form = $this->createForm(new ProduitType(), $produit);

            return $this->render('GplainteBundle:Produit:addProduit.html.twig', array(
                'produit' => $form->createView(),
                'list'=>$listproduit,
                'val'=>$val,
                'id'=>$id
            ));

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

}
