<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GPlainte\GPlainteBundle\Entity\Parametres;
use GPlainte\GPlainteBundle\Form\ParametresType;

/**
 * Parametres controller.
 *
 */
class ParametresController extends Controller
{

    /**
     * Lists all Parametres entities.
     *
     */

    private $texte="Merci! votre plainte anonyme a été bien enregistrée";

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GplainteBundle:Parametres')->findAll();

        return $this->render('GplainteBundle:Parametres:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Parametres entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Parametres();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('parametres_show', array('id' => $entity->getId())));
        }

        return $this->render('GplainteBundle:Parametres:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Parametres entity.
     *
     * @param Parametres $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Parametres $entity)
    {
        $form = $this->createForm(new ParametresType(), $entity, array(
            'action' => $this->generateUrl('parametres_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Parametres entity.
     *
     */
    public function newAction()
    {
        $entity = new Parametres();
        $form   = $this->createCreateForm($entity);

        return $this->render('GplainteBundle:Parametres:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Parametres entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GplainteBundle:Parametres')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametres entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GplainteBundle:Parametres:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Parametres entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GplainteBundle:Parametres')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametres entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GplainteBundle:Parametres:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'id'=>$id
        ));
    }

    /**
    * Creates a form to edit a Parametres entity.
    *
    * @param Parametres $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Parametres $entity)
    {
        $form = $this->createForm(new ParametresType(), $entity, array(
            'action' => $this->generateUrl('parametres_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Parametres entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
         
        $entity = $em->getRepository('GplainteBundle:Parametres')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametres entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('parametres_edit', array('id' => $id)));
        }

        return $this->render('GplainteBundle:Parametres:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'id' => $id
        ));
    }
    /**
     * Deletes a Parametres entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GplainteBundle:Parametres')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Parametres entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('parametres'));
    }

    /**
     * Creates a form to delete a Parametres entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('parametres_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function sendSms($numeroplainte,$delai,$telephone){
        $this->texte="Merci votre plainte numero ".$numeroplainte." a ete bien enregistree, vous aurez une reponse au plus tard le ".$delai.". Ceci est un message automatisé, veuillez ne pas y répondre.";
        //$this->texte="Thanks your complaint #".$numeroplainte." has been recorded, you'll have an answer no later than ".$delai.". This is an automated message, please do not reply.";

        $lien="http://oceanicsms.com/api/http/sendmsg.php";
        $postfields = array(
            'user' => 'satisuimcec',
            'password' => 'SatisUimcec',
            'from'=>'SATIS',
            'to'=>$telephone,
            'text'=>$this->texte,
            'api'=>14265

        );
        $curl=curl_init();
//                 var_dump($result); die();
        curl_setopt($curl, CURLOPT_URL, $lien);
        curl_setopt($curl, CURLOPT_COOKIESESSION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
        curl_exec($curl);
        curl_close($curl);
    }


    public function sendSmsAffecter($telephone){
        $this->texte="Une nouvelle plainte vient d'être affectée à votre service. Ceci est un message automatisé, veuillez ne pas y répondre.";

        $lien="http://oceanicsms.com/api/http/sendmsg.php";
        $postfields = array(
            'user' => 'satisuimcec',
            'password' => 'SatisUimcec',
            'from'=>'SATIS',
            'to'=>$telephone,
            'text'=>$this->texte,
            'api'=>14265

        );
        $curl=curl_init();
//                 var_dump($result); die();
        curl_setopt($curl, CURLOPT_URL, $lien);
        curl_setopt($curl, CURLOPT_COOKIESESSION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
        curl_exec($curl);
        curl_close($curl);
    }

    public function notificationMobilePlainteTraiter($id){

        $lien="http://mobile.dmdsatis.com/web/app_dev.php/api/notificationplaintetaiters/".$id.".json";
        $postfields = array(
            'api'=>14265
        );
        $curl=curl_init();
        curl_setopt($curl, CURLOPT_URL, $lien);
        curl_setopt($curl, CURLOPT_COOKIESESSION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
        curl_exec($curl);
        curl_close($curl);
    }
    public function notificationMobileDenonciationTraiter($id){

        $lien="http://mobile.dmdsatis.com/web/app_dev.php/api/notificationdenonciationtaiters/".$id.".json";
        $postfields = array(
            'api'=>14265
        );
        $curl=curl_init();
        curl_setopt($curl, CURLOPT_URL, $lien);
        curl_setopt($curl, CURLOPT_COOKIESESSION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
        curl_exec($curl);
        curl_close($curl);
    }

    public function sendEmailAction(){

//try{}catch(\Swift_TransportException $e){
//    echo $e->getMessage();
//}
//        $path = substr($this->get('kernel')->getRootDir(), 0, -4) . '/src/GPlainte/NotificationBundle/Data/kiss me.mp3';

        $message = \Swift_Message::newInstance()
            ->setSubject('Bonjour à tous')
            ->setFrom(array('dmdsatis@gmail.com'=>"dmdsatis"))
            ->setTo('ahoumenoumaurice@gmail.com')
            ->setCharset('utf-8')
            ->setContentType('text/html')
            ->setBody($this->renderView('GplainteBundle:Email:Email.html.twig'))
        ;
//        $message->attach(
//            \Swift_Attachment::fromPath($path)->setFilename('kiss me.mp3')
//        );
        $this->get('mailer')->send($message);
        return new Response("très bien!");
    }


    public function sendFileErrorEmailAction(){

        $message = \Swift_Message::newInstance()
            ->setSubject('Bonjour à tous')
            ->setFrom(array('dmdsatis@gmail.com'=>"dmdsatis"))
            ->setTo('ahoumenoumaurice@gmail.com')
            ->setCharset('utf-8')
            ->setContentType('text/html')
            ->setBody($this->renderView('GplainteBundle:Email:Email.html.twig'));
        $this->get('mailer')->send($message);
        echo "Bon!"; die();
    }

    /**
     * @return mixed
     */
    public function getTexte()
    {
        return $this->texte;
    }

    /**
     * @param mixed $texte
     */
    public function setTexte($texte)
    {
        $this->texte = $texte;
    }



}
