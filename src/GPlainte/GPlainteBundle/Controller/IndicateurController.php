<?php

namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GPlainte\GPlainteBundle\Entity\NormeIndicateur;
use GPlainte\GPlainteBundle\Form\NormeIndicateurHandler;
use GPlainte\GPlainteBundle\Form\NormeIndicateurType;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;

class IndicateurController extends Controller
{

 
    
    public function addIndicateurAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $user = $this->container->get('security.context')->getToken()->getUser();

            $repository = $em->getRepository('GplainteBundle:NormeIndicateur');

            $listnorme = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            if ($val=="afficherForm"){

                $norme=$repository->find($id);
//            var_dump($norme); die();
                $form = $this->createForm(new NormeIndicateurType(), $norme);
                $val="modifier";

            }else
                if ($val=="modifier"){} elseif ($val=="supprimer"){
                    $norme = new NormeIndicateur();
                    $form = $this->createForm(new NormeIndicateurType(), $norme);
                }else{
                    $norme = new NormeIndicateur();
                    $form = $this->createForm(new NormeIndicateurType(), $norme);
                    $val="ajouter";
                }
            $entiteSuppr = $em->getRepository('GplainteBundle:NormeIndicateur')->find($id);


            if ($val=="ajouter"){
                $norme = new NormeIndicateur();
                $form = $this->createForm(new NormeIndicateurType(), $norme);
                $formHandler = new NormeIndicateurHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                $request=  $this->getRequest();
                $data=$request->request->get($form->getName());
                $libelle=$data['libelle'];
                $normeCh=$data['normeVal'];
                $jour=$data['jour'];
                $recup=  $em->getRepository('GplainteBundle:NormeIndicateur')->findBy(array('libelle'=>$libelle,'normeVal'=>$normeCh,'jour'=>$jour));


                if (count($recup)== 0){

                    if($formHandler->process())
                    {

                        $repository = $em->getRepository('GplainteBundle:NormeIndicateur');
                        $listnorme = $repository->findBy(
                            array(),
                            array('id'=>'DESC')
                        );

                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(76);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:NormeIndicateur:addNormeIndicateur.html.twig',array('norme' => $form->createView(),'msg'=>1,
                            'list'=>$listnorme,
                            'val'=>$val,
                            'id'=>$id,
                            'suppr'=>$entiteSuppr
                        ));

                    }

                } else {
                    $repository = $em->getRepository('GplainteBundle:NormeIndicateur');
                    $listnorme = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:NormeIndicateur:addNormeIndicateur.html.twig',array('norme' => $form->createView(),'msgErr'=>1,
                        'list'=>$listnorme,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }

            if ($val=="modifier" && $id!=0){

                if( !$norme = $em->getRepository('GplainteBundle:NormeIndicateur')->find($id) )
                {
                    $repository = $em->getRepository('GplainteBundle:NormeIndicateur');
                    $listnorme = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:NormeIndicateur:addNormeIndicateur.html.twig',array('norme' => $form->createView(),'msgErr'=>"Cette n'existe pas",
                        'list'=>$listnorme,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }

                $form = $this->createForm(new NormeIndicateurType(), $norme);
                $formHandler = new NormeIndicateurHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                if($formHandler->process())
                {
                    /*if($norme->getLibelle()=='Delai de traitement'){

                    }
                    if($norme->getLibelle()=='Periode de recurrence'){

                    }
                    if($norme->getLibelle()=='seuil de recurrence'){

                    }*/
                    $repository = $em->getRepository('GplainteBundle:NormeIndicateur');
                    $listnorme = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );

                    $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(77);
                    $event= new SaveComplaintEvent($action,$user);
                    $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                    return $this->render('GplainteBundle:NormeIndicateur:addNormeIndicateur.html.twig',array('norme' => $form->createView(),'msg'=>2,
                        'list'=>$listnorme,
                        'val'=>$val,
                        'id'=>$id,
                        'suppr'=>$entiteSuppr
                    ));
                }
            }

            $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(79);
            $event= new SaveComplaintEvent($action,$user);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:NormeIndicateur:addNormeIndicateur.html.twig',array('norme' => $form->createView(),
                'list'=>$listnorme,
                'val'=>$val,
                'id'=>$id,
                'suppr'=>$entiteSuppr
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


    public function deleteIndicateurAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em = $this->getDoctrine()->getManager();
            $user = $this->container->get('security.context')->getToken()->getUser();

            $entity = $em->getRepository('GplainteBundle:NormeIndicateur')->find($id);

            if (!$entity) {
                return $this->redirect($this->generateUrl('gplainte_add_indicateur',array('msgException'=>1)));

            }
            try{
                if ($this->getRequest()->getMethod()=='POST'){
                    $em->remove($entity);
                    $em->flush();
                        $action=$this->getDoctrine()->getRepository('UserBundle:DataEvent')->find(78);
                        $event= new SaveComplaintEvent($action,$user);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);
                    return $this->redirect($this->generateUrl('gplainte_add_indicateur',array('msg'=>3)));
                }
            }catch (\Doctrine\DBAL\DBALException $e){

                return $this->redirect($this->generateUrl('gplainte_add_indicateur',array('msgException'=>2)));
            }
            $repository = $em->getRepository('GplainteBundle:NormeIndicateur');
            $listnorme = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );

            $norme = new NormeIndicateur();
            $form = $this->createForm(new NormeIndicateurType(), $norme);

            return $this->render('GplainteBundle:NormeIndicateur:addNormeIndicateur.html.twig', array(
                'norme' => $form->createView(),
                'list'=>$listnorme,
                'val'=>$val,
                'id'=>$id
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }


    }

}
