<?php

namespace GPlainte\GPlainteBundle\Controller;

use GPlainte\GPlainteBundle\Form\ActionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use GPlainte\GPlainteBundle\Entity\Plainte;
use GPlainte\GPlainteBundle\Form\PlainteType;
use GPlainte\GPlainteBundle\Form\PlainteHandler;

use GPlainte\GPlainteBundle\Entity\Client;
use GPlainte\GPlainteBundle\Form\ClientType;
use GPlainte\GPlainteBundle\Form\ClientHandler;

use GPlainte\GPlainteBundle\Entity\Affecter;
use GPlainte\GPlainteBundle\Form\AffecterType;
use GPlainte\GPlainteBundle\Form\AffecterType2;

use GPlainte\GPlainteBundle\Form\AffecterHandler;

use GPlainte\NotificationBundle\Entity\Notifs;
use Symfony\Component\Validator\Constraints\DateTime;

use GPlainte\GPlainteBundle\Entity\Action;

use GPlainte\GPlainteBundle\Entity\Denonciation;
use GPlainte\GPlainteBundle\Form\DenonciationType;
use GPlainte\GPlainteBundle\Form\DenonciationHandler;

use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;
use GPlainte\GPlainteBundle\Journal\trackError;


class PlainteController extends Controller
{

//    protected $userEvent;

    public function currentUser(){
        $userEvent=$this->container->get('security.context')->getToken()->getUser();

    }


    public function accueilAction(){
//        echo "Bonjour"; die();
        return $this->render('GplainteBundle:Plainte:accueil.html.twig',array(

        ));
    }

    public function showPlainteAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }
        $em=$this->getDoctrine()->getManager();
        $trace= new trackError($em,$this->getRequest(),$this->container);
        $userEvent = $this->container->get('security.context')->getToken()->getUser();

        $em=$this->getDoctrine()->getManager();
        $repository = $em->getRepository('GplainteBundle:Plainte');
        if (isset($_POST["debut"]) && isset($_POST["fin"])){
            $datedebut=explode('-',$_POST["debut"]);
            $debut=$datedebut[2].'-'.$datedebut[1].'-'.$datedebut[0];
            $datefin=explode('-',$_POST["fin"]);
            $fin=$datefin[2].'-'.$datefin[1].'-'.$datefin[0];
//            echo $fin; die();
            $listplainte=$repository->getListePeriodePlainteNonAffecter($debut,$fin);
        }else{
            $listplainte=$repository->getListePlainteNonAffecter();
        }

        if ($val=="affecter"){
          $affecter = new Affecter();
           $formAffecter = $this->createForm(new AffecterType(), $affecter);
            $trace->trackEvent(5);
        }else{
            $formAffecter=null;
        }
        if ($val=="detail" AND $id!=0){
            $listplainte=$repository->find($id);
             return $this->render('GplainteBundle:Plainte:detailPlainte.html.twig',array(
               'list'=>$listplainte));

        }

        $trace->trackEvent(4);

         return $this->render('GplainteBundle:Plainte:showPlainte.html.twig',array(
               'list'=>$listplainte,
              'affecter' => $formAffecter->createView(),
                'val'=>$val,
                'id'=>$id
          ));

    }




    public function showPlainteArchivedAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }
        $em=$this->getDoctrine()->getManager();
        $repository = $em->getRepository('GplainteBundle:Plainte');
        $trace= new trackError($em,$this->getRequest(),$this->container);

        if (isset($_POST["debut"]) && isset($_POST["fin"])){
            $datedebut=explode('-',$_POST["debut"]);
            $debut=$datedebut[2].'-'.$datedebut[1].'-'.$datedebut[0];
            $datefin=explode('-',$_POST["fin"]);
            $fin=$datefin[2].'-'.$datefin[1].'-'.$datefin[0];
//            echo $fin; die();
            $listplainte=$repository->getListePeriodePlainteNonAffecter($debut,$fin);
        }else{
            $listplainte=$repository->getListePlainteNonAffecter();
        }

        if ($val=="affecter"){
            $affecter = new Affecter();
            $formAffecter = $this->createForm(new AffecterType(), $affecter);
        }else{
            $formAffecter=null;
        }
        if ($val=="detail" AND $id!=0){
            $listplainte=$repository->find($id);
           // var_dump($listplainte); die();
            return $this->render('GplainteBundle:Plainte:detailPlainteArchived.html.twig',array(
                'list'=>$listplainte));
        }

        return $this->render('GplainteBundle:Plainte:showPlainte.html.twig',array(
            'list'=>$listplainte,
            'affecter' => $formAffecter->createView(),
            'val'=>$val,
            'id'=>$id
        ));

    }

    public function exportPlainteAction(){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

        $user = $this->container->get('security.context')->getToken()->getUser();

        $em=$this->getDoctrine()->getManager();
        $repository = $em->getRepository('GplainteBundle:Plainte');
        $trace= new trackError($em,$this->getRequest(),$this->container);

        if (isset($_POST["debut"]) && isset($_POST["fin"])){
            $datedebut=explode('-',$_POST["debut"]);
            $debut=$datedebut[2].'-'.$datedebut[1].'-'.$datedebut[0];
            $datefin=explode('-',$_POST["fin"]);
            $fin=$datefin[2].'-'.$datefin[1].'-'.$datefin[0];
            $listplainte=$repository->getListePeriodePlainteNonAffecter($debut,$fin);
            $nb_ligne=count($listplainte);

            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

            for($q = 0; $q < $nb_ligne; $q++ ):

//                var_dump($listplainte[0]->getAgent()->getId()); die();

                    $datecreation=date_format($listplainte[$q]->getDateCreation(),'Y-m-d H:i:s');
                    $datemodification=date_format($listplainte[$q]->getDateModification(),'Y-m-d H:i:s');

                    if ($listplainte[$q]->getAgent()==null){
                        $agent="NULL";
                    }else{
                        $agent=$listplainte[$q]->getAgent()->getId();
                    }

                    if ($listplainte[$q]->getProduit()==null){
                        $produit="NULL";
                    }else{
                        $produit=$listplainte[$q]->getProduit()->getId();
                    }


                    if ($listplainte[$q]->getService()==null){
                        $service="NULL";
                    }else{
                        $service=$listplainte[$q]->getService()->getId();
                    }

                    if ($listplainte[$q]->getAutres()==null){
                        $autre="NULL";
                    }else{
                        $autre=$listplainte[$q]->getAutres()->getId();
                    }

                    if ($listplainte[$q]->getIndicateur()==null){
                        $indicateur="NULL";
                    }else{
                        $indicateur=$listplainte[$q]->getIndicateur()->getId();
                    }

//                    echo $agent;
//
//                    die();
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A'.$q, $agent)
                        ->setCellValue('B'.$q, $produit)
                        ->setCellValue('C'.$q, $listplainte[$q]->getClient()->getId())
                        ->setCellValue('D'.$q, $listplainte[$q]->getNiveau()->getId())
                        ->setCellValue('E'.$q, $service)
                        ->setCellValue('F'.$q, $datecreation)
                        ->setCellValue('G'.$q, $datemodification)
                        ->setCellValue('H'.$q, $listplainte[$q]->getDescription())
                        ->setCellValue('I'.$q,$autre )
                        ->setCellValue('J'.$q, $listplainte[$q]->getAffection())
                        ->setCellValue('K'.$q, $listplainte[$q]->getTraitement())
                        ->setCellValue('L'.$q, $listplainte[$q]->getAutreobjet())
                        ->setCellValue('M'.$q, $indicateur)
                        ->setCellValue('N'.$q, $listplainte[$q]->getBureau()->getId())
                        ->setCellValue('O'.$q, $listplainte[$q]->getTelephone())
                        ->setCellValue('P'.$q, $listplainte[$q]->getNumdossier() )
                        ->setCellValue('Q'.$q, $listplainte[$q]->getChoix()->getId())

            ;


            endfor;


            $phpExcelObject->getActiveSheet()->setTitle('Plaintes');

            $phpExcelObject->setActiveSheetIndex(0);

            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'export-plaintes.xls'
            );
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            $response->headers->set('Content-Disposition', $dispositionHeader);

            $trace->trackEvent(81);

            return $response;
        }

//        else{
//            $listplainte=$repository->getListePlainteNonAffecter();
//        }

        return $this->render('GplainteBundle:Plainte:export.html.twig',array(
//            'list'=>$listplainte,

        ));
    }

    public function addImportPlainteAction($val)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }
        $em=$this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();
        $trace= new trackError($em,$this->getRequest(),$this->container);

        $repository = $em->getRepository('GplainteBundle:Plainte');
        $plainte = new Plainte();

        if ($val==='import'){
            try{
                $request=$this->getRequest();
                $file=$request->files->get('excel');

                $excelObj = $this->get('phpexcel')->createPHPExcelObject($file);
                $excelObj->setActiveSheetIndex(0);
                //var_dump($excelObj->setActiveSheetIndex(0)->getHighestRow());
                if($excelObj->setActiveSheetIndex(0)->getHighestColumn()=="A" && $excelObj->setActiveSheetIndex(0)->getHighestRow()==1) {

                    return $this->render('GplainteBundle:Plainte:import.html.twig', array(
                        'excelvide' => 'Le fichier excel est invalide'
//            'plainte' => $form->createView()
                    ));

                }
                else{
                    $sheetData = $excelObj->getActiveSheet()->toArray(null, true, true, true);
                    $nb_ligne = count($sheetData);

//        var_dump($nb_ligne); die();
                    for ($q = 1; $q <= $nb_ligne; $q++):


                        $agent=$sheetData[$q]['A'];
                        $produit=$sheetData[$q]['B'];
                        $client=$sheetData[$q]['C'];
                        $niveau=$sheetData[$q]['D'];
                        $service=$sheetData[$q]['E'];
                        $dateCreation=$sheetData[$q]['F'];
                        $datemodification=$sheetData[$q]['G'];
                        $description=$sheetData[$q]['H'];
                        $autres=$sheetData[$q]['I'];
                        $affection=$sheetData[$q]['J'];
                        $traitement=$sheetData[$q]['K'];
                        $autreobjet=$sheetData[$q]['L'];
                        $indicateur=$sheetData[$q]['M'];
                        $bureau=$sheetData[$q]['N'];
                        $telephone=$sheetData[$q]['O'];
                        $numdossier=$sheetData[$q]['P'];
                        $choix=$sheetData[$q]['Q'];

                        if ($bureau==="NULL"){
                            $bureauValeur=NULL;
                        }else {

                            $bureauValeur=$em->getRepository('GplainteBundle:Bureau')->find($bureau);
                            $plainte->setBureau($bureauValeur);

                        }

                        if ($choix==="NULL"){
                            $choixValeur=NULL;
                        }else {

                            $choixValeur=$em->getRepository('GplainteBundle:Choix')->find($choix);
                            $plainte->setChoix($choixValeur);

                        }

                        if ($service==="NULL"){
                            $serviceValeur=NULL;
                        }else {

                            $serviceValeur=$em->getRepository('GplainteBundle:Service')->find($service);
                            $plainte->setService($serviceValeur);

                        }


                        if ($produit=="NULL"){
                            $produitValeur=NULL;
                        }else {
                            $produitValeur=$em->getRepository('GplainteBundle:Produit')->find($produit);
                            $plainte->setProduit($produitValeur);

                        }


                        if ($agent=="NULL"){
                            $agentValeur=NULL;
                        }else {
                            $agentValeur=$em->getRepository('GplainteBundle:Agent')->find($agent);
                            $plainte->setAgent($agentValeur);
                        }

                        if ($autres==="NULL"){
                            $autresValeur=NULL;
                        }else {
                            $autresValeur=$em->getRepository('GplainteBundle:Autres')->find($autres);
                            $plainte->setAutres($autresValeur);

                        }

                        if ($client==="NULL"){
                            $clientValeur=NULL;
                        }else {
                            $clientValeur=$em->getRepository('GplainteBundle:Client')->find($client);
                            $plainte->setClient($clientValeur);
                        }

                        if ($niveau==="NULL"){
                            $niveauValeur=NULL;
                        }else {
                            $niveauValeur=$em->getRepository('GplainteBundle:Niveau')->find($niveau);
                            $plainte->setNiveau($niveauValeur);

                        }


                        if ($indicateur==="NULL"){
                            $indicateurValeur=NULL;
                        }else {
                            $indicateurValeur=$em->getRepository('GplainteBundle:NormeIndicateur')->find($indicateur);
                            $plainte->setIndicateur($indicateurValeur);

                        }

                        $plainte->setAffection($affection);
                        $plainte->setTraitement($traitement);
                        $plainte->setTelephone($telephone);
                        $plainte->setNumdossier($numdossier);
                        $plainte->setAutreobjet($autreobjet);
//                $plainte->setBureau($bureau);
//                $plainte->setChoix($choix);
                        $plainte->setDateCreation(new \DateTime($dateCreation));
                        $plainte->setDateModification(new \DateTime($datemodification));
                        $plainte->setDescription($description);
                        $em->persist($plainte);

                    endfor;
                    $em->flush();
                    $trace->trackEvent(82);


                    return $this->render('GplainteBundle:Plainte:import.html.twig', array(
                        'msg'=>'Enregistrement effectué avec succès'
//            'plainte' => $form->createView()
                    ));
                }
            }catch (\Exception $e){
                $trace->traceError(82,$e->getMessage(),$e->getLine());
            }

        }

        return $this->render('GplainteBundle:Plainte:import.html.twig', array(
//            'plainte' => $form->createView()
        ));

    }

    public function addPlainteAction($val,$id,$idVal)
    {

        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

        $em=$this->getDoctrine()->getManager();
        $repository = $em->getRepository('GplainteBundle:Plainte');
        $repositoryClt = $em->getRepository('GplainteBundle:Client');
        $repository2 = $em->getRepository('GplainteBundle:NormeIndicateur');
        $trace= new trackError($em,$this->getRequest(),$this->container);


        $plainteMobile=$em->getRepository("GplainteBundle:PlainteMobile")->findBy(
            array('type'=>0,'validation'=>0),
            array('id'=>'DESC')
        );
        $denonciationMobile=$em->getRepository("GplainteBundle:PlainteMobile")->findBy(
            array('type'=>1,'validation'=>0),
            array('id'=>'DESC')
        );

        if (isset($_POST['ch']) && $_POST['ch']==1){
            $plainte = new Denonciation();
            $form = $this->createForm(new DenonciationType(), $plainte);

            $client = new Client();
            $formClient = $this->createForm(new ClientType(), $client);

            $ch=$_POST['ch'];
//            echo "fffffffffff"; die();
        }else{
            $plainte = new Plainte();
            $form = $this->createForm(new PlainteType(), $plainte);

            $client = new Client();
            $formClient = $this->createForm(new ClientType(), $client);
            $ch=0;
        }

//        $val="ajouter";

        if ($val==="ajouter") {
            $user = $this->container->get('security.context')->getToken()->getUser()->getAgent();
            $request=  $this->getRequest();
            $data=$request->request->get($form->getName());
            $userEvent = $this->container->get('security.context')->getToken()->getUser();
            $codeplaintemobile=$data['codeplaintemobile'];

            if ($_GET['chVal']==1){
                $denonciation = new Denonciation();
                $form = $this->createForm(new DenonciationType(), $denonciation);
                $data=$request->request->get($form->getName());
                $niveau=$data['niveau'];
                $codeplaintemobile=$data['codeplaintemobile'];
                $formHandler = new DenonciationHandler($form, $this->get('request'), $this->getDoctrine()->getManager(),$user);
            }else{
                $plainte = new Plainte();
                $form = $this->createForm(new PlainteType(), $plainte);
                $telephone=$data['telephone'];
                $numdossier=$data['numdossier'];
                $clientRecu=$data['client'];
//                echo $clientRecu; die();
                $niveau=$data['niveau'];
                $clientObjet=$repositoryClt->find($clientRecu);
                $nom=$clientObjet->getNom();
                $prenom=$clientObjet->getPrenom();

                $idClientObjet=$repositoryClt->findOneBy(array('nom'=>$nom,'prenom'=>$prenom));

                $formHandler = new PlainteHandler($form, $this->get('request'), $this->getDoctrine()->getManager(),$user);

                $path = substr($this->get('kernel')->getRootDir(), 0, -4) . '/src/GPlainte/NotificationBundle/Data/';
                $filename = 'notification.json';
                $params = json_decode(file_get_contents($path . '' . $filename));
                $arrayValue=(array)$params->dm;
                foreach ($arrayValue as $key=>$valeur){
                    if ($key==$niveau){
                        $delaiReponse=$valeur;
                    }
                }

                $date=new \DateTime();
                $nbre_jour='P'.$delaiReponse.'D';
                $jr=$date->add(new \DateInterval($nbre_jour));
                $jour=$date->format('d');
                $mois=$date->format('m');
                $annee=$date->format('Y');

//            traduction du mois
                switch ($mois){
                    case '01':
                        $mois='Janvier';
                        break;
                    case '02':
                        $mois='Février';
                        break;
                    case '03':
                        $mois='Mars';
                        break;
                    case '04':
                        $mois='Avril';
                        break;
                    case '05':
                        $mois='Mai';
                        break;
                    case '06':
                        $mois='Juin';
                        break;
                    case '07':
                        $mois='Juillet';
                        break;
                    case '08':
                        $mois='Août';
                        break;
                    case '09':
                        $mois='Septembre';
                        break;
                    case '10':
                        $mois='Octobre';
                        break;
                    case '11':
                        $mois='Novembre';
                        break;
                    case '12':
                        $mois='Décembre';
                        break;
                }

                $locale=$this->container->getParameter("locale");
                if ($locale=='fr'){
                    $delai=$jour.' '.$mois.' '.$annee;
                }else{
                    $delai=$date->format('m d Y');
                }

            }



//            $norme=$repository2->findOneById(1);

            if ($_GET['chVal']==1){

            }else{
                $telephoneCode=$params->codetel.''.$data["telephone"];
            }

            try{
                if ($formHandler->process()) {

                    $valeur=$repository->findBy(
                        array(),
                        array('id'=>'DESC'),
                        array(1,0)
                    );


// ---------------------------changement d'état des plaintes mobiles reccueillir-----------------------------------------------
                    $plaintemobile=$em->getRepository("GplainteBundle:PlainteMobile")->findOneBy(array('idplainte'=>$codeplaintemobile));
                    if($plaintemobile!=null){
						$plaintemobile->setValidation(1);
                    $em->persist($plaintemobile);
                    $em->flush();
					}
					
//-------------------------------------------------------------------------------------------------------------------------------

                    $sendSms= new ParametresController;
                    if ($_GET['chVal']==1){
                        $trace->trackEvent(3);

                    }else{
//                    die($telephoneCode);
                        $idClientObjet->setTelephone1($telephone);
                        $idClientObjet->setNumDossier($numdossier);
                        $em->persist($idClientObjet);
                        $em->flush();
                        $sendSms->sendSms($valeur[0]->getId(),$delai,$telephoneCode);

                        $trace->trackEvent(1);

                    }


                    return $this->redirect($this->generateUrl('gplainte_add_plainte', array('delai'=>$sendSms->getTexte(),
                        'objet'=>$plainteMobile,
						
                        'objetdenoncation'=>$denonciationMobile

                    )));

                }

            }catch (\Exception $e){
                $trace->traceError(1,$e->getMessage(),$e->getLine());
            }
        }
//        var_dump($plainteMobile); die();

        return $this->render('GplainteBundle:Plainte:addPlainte.html.twig', array(
            'plainte' => $form->createView(),
            'client'=>$formClient->createView(),
            'val'=>$val,
            'id'=>$id,
            'chVal'=>$ch,
            'chVal2'=>$idVal,
            'objet'=>$plainteMobile,
            'objetdenonciation'=>$denonciationMobile

        ));

    }





    public function showAffecterAction($id){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }
        $userEvent = $this->container->get('security.context')->getToken()->getUser();
        $em=$this->getDoctrine()->getManager();
        $repository = $em->getRepository('GplainteBundle:Affecter');
        $trace= new trackError($em,$this->getRequest(),$this->container);

        $user = $userEvent->getAgent();

        $idservice=$user->getService()->getId();
        $listeaffectation=$repository->getListeAffecter($idservice);
        $listeTraitementRejet=$repository->getListePlainteRejeterApresTraitement($idservice);

        $trace->trackEvent(6);



        return $this->render('GplainteBundle:Affecter:showAffecter.html.twig', array(
//                'affecter' => $form->createView()
                'list'=>$listeaffectation,
                'listRejet'=>$listeTraitementRejet
            ));
    }

    public function addAffecterAction($val,$id,$serviceid){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Plainte');
            $repositoryAffecter = $em->getRepository('GplainteBundle:Affecter');
            $repositoryAgent = $em->getRepository('GplainteBundle:Agent');
            $trace= new trackError($em,$this->getRequest(),$this->container);


//            echo "lp"; die();

            if ($val=="traiter" and $id!=0){

                $affectation=$repositoryAffecter->findOneByPlainte($id);
                if($affectation!=null){

                }
                else{
                    return  $this->redirect($this->generateUrl('gplainte_show_affecter'));
                }

                $form = $this->createForm(new AffecterType2($serviceid), $affectation);
                return $this->render('GplainteBundle:Affecter:addTraiter.html.twig', array(
                    'traiter' => $form->createView(),
                    'val'=>'ajouter',
                    'id'=>$id,
                    'objetPlainte'=>$affectation
                ));
            }

            if ($val=="afficherForm" and $id!=0){
                $affectation=$repositoryAffecter->find($id);
                $form = $this->createForm(new AffecterType2($serviceid), $affectation);
                return $this->render('GplainteBundle:Affecter:addTraiter.html.twig', array(
                    'traiter' => $form->createView(),
                    'val'=>'modifier',
                    'id'=>$id,
                    'objetPlainte'=>$affectation
                ));
            }

            if ($val=="ajouter"){

                try{
                    $affecter = new Affecter();
                    $form = $this->createForm(new AffecterType(), $affecter);
//        $formHandler = new AffecterHandler($form, $this->get('request'), $this->getDoctrine()->getManager());
                    $request=  $this->getRequest();
                    $data=$request->request->get($form->getName());
                    $agent=$repositoryAgent->findOneBy(array('service'=>$data["service"],'chef'=>1));
                    $dg=$repositoryAgent->findOneBy(array('dg'=>1));


                    $path = substr($this->get('kernel')->getRootDir(), 0, -4) . '/src/GPlainte/NotificationBundle/Data/';
                    $filename = 'notification.json';
                    $params = json_decode(file_get_contents($path . '' . $filename));

                    if($agent!=null){
                        $agentTel=$agent->getTelephone();
                        $agentEmail=$agent->getEmail();

                        if ($params->param_alerte_affectation_sms==1){
                            $sendSms= new ParametresController;
                            $sendSms->sendSmsAffecter($params->codetel.''.$agentTel);
                        }

//                    if ($params->param_alerte_affectation_mail==1){
//
//                    $message = \Swift_Message::newInstance()
//                        ->setSubject('Bonjour')
//                        ->setFrom('maurice@dmdsatis.com')
//                        ->setTo('ahoumenoumaurice@gmail.com')
//                        ->setBody($this->renderView('GplainteBundle:Email:Email.txt.twig', array()))
//                    ;
//                    $this->get('mailer')->send($message);
//
//                    }

                    }


                    if($dg!=null){
                        $agentTel=$dg->getTelephone();
                        $agentEmail=$dg->getEmail();

                        if ($params->param_alerte_affectation_sms==1){
                            $sendSms= new ParametresController;
                            $sendSms->sendSmsAffecter($params->codetel.''.$agentTel);
                        }

                        if ($params->param_alerte_affectation_mail==1){

                            $message = \Swift_Message::newInstance()
                                ->setSubject('Bonjour à tous')
                                ->setFrom('maurice@dmdsatis.com')
                                ->setTo('ahoumenoumaurice@gmail.com')
                                ->setCharset('utf-8')
                                ->setContentType('text/html')
                                ->setBody($this->renderView('GplainteBundle:Email:Email.html.twig'))
                            ;
                            $this->get('mailer')->send($message);

                        }

                    }


//                $serviceidRecup=$data["service"];
//                var_dump($agent->getTelephone()); die();

                    if( $request->getMethod() == 'POST' )
                    {
                        $form->handleRequest($request);
                        if($form->isValid())
                        {

//                $explo=explode("-",$data["dateAffectation"]);
//                $date=$explo[2].'-'.$explo[1].'-'.$explo[0];
                            $dateFormat=new \DateTime();
                            $plainte=$repository->find($id);
                            if($plainte->getAffection()==0) {
                                $plainte->setAffection(1);
                                $affecter->setDateAffectation($dateFormat);
                                $affecter->setApprobation(0);
                                $affecter->setNonapprouver(0);
                                $affecter->setPlainte($plainte);
                                $em->persist($plainte);
                                $em->persist($affecter);
                                $em->flush();
                            }
                        }

                        return  $this->redirect($this->generateUrl('gplainte_show_plainte'));
                    }
                }catch (\Exception $e){
                    $trace->traceError($e->getMessage(),$e->getLine());
                }

            }
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

    public function listArchivesAction(){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){

            $userEvent = $this->container->get('security.context')->getToken()->getUser();

            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Affecter');
            $repositoryDenonciation = $em->getRepository('GplainteBundle:Denonciation');
            $trace= new trackError($em,$this->getRequest(),$this->container);


            $listplainte=$repository->getListePlainteArchive();
            $listeDenonciation=$repositoryDenonciation->getListeDenonciationArchives();

            $trace->trackEvent(22);

            return $this->render('GplainteBundle:Plainte:plainteArchive.html.twig', array(
                'list'=>$listplainte,
                'list2'=>$listeDenonciation
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

    public function SuiviPlainteAction(){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){

            $userEvent = $this->container->get('security.context')->getToken()->getUser();

            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Affecter');
            $lesplaintes=$em->getRepository('GplainteBundle:Plainte')->findAll();
            $trace= new trackError($em,$this->getRequest(),$this->container);



            foreach($lesplaintes as $plaintes):
                $affectation=$repository->getEtatPlainteSuivi4($plaintes->getId());
                if ($affectation==null){
                    $tab[]=$plaintes;
                }else{
                    $proposition=$repository->getEtatPlainteSuivi3($plaintes->getId());
                    if ($proposition==null){
                        $tab[]=$affectation;
                    }else{
                        $notification=$repository->getEtatPlainteSuivi2($plaintes->getId());
                        if ($notification==null){
                            $tab[]=$proposition;
                        }else {
                            $satisfaction=$repository->getEtatPlainteSuivi1($plaintes->getId());
                            if ($satisfaction==null){
                                $tab[]=$notification;
                            }
                        }
                    }
                }
            endforeach;

            if (!isset($tab)){
                $tab=null;
            }
            $trace->trackEvent(21);

            return $this->render('GplainteBundle:Plainte:plainteSuivi.html.twig', array(
                'list'=>$tab,
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

    public function EtatPlainteAction($val,$id){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Affecter');
            $trace= new trackError($em,$this->getRequest(),$this->container);


            $satisfaction=$repository->getEtatPlainteSuivi1($id);
            $notification=$repository->getEtatPlainteSuivi2($id);
            $proposition=$repository->getEtatPlainteSuivi3($id);
            $affectation=$repository->getEtatPlainteSuivi4($id);

            $plainte=$em->getRepository('GplainteBundle:Plainte')->find($id);

            if ($satisfaction==null){
            }else {
                $etat=$satisfaction;
            }

            if ($notification==null){
            }else {
                $etat=$notification;
            }

            if ($proposition==null){
            }else {
                $etat=$proposition;
            }

            if ($affectation==null){
                $etat=$plainte;
            }else {
                $etat=$affectation;
            }


//        var_dump($satisfaction); die();

            return $this->render('GplainteBundle:Plainte:plainteEtat.html.twig', array(
                'satisfaction'=>$etat,
                'val'=>$val,
                'id'=>$id
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

    public function calculIndicateurAction(){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Plainte');
            $repository2 = $em->getRepository('GplainteBundle:Satisfaction');
            $repository3 = $em->getRepository('GplainteBundle:Affecter');
            $repositoryIndicateur = $em->getRepository('GplainteBundle:NormeIndicateur');

            $nbreJr=$repositoryIndicateur->findOneByMarqueur("efficacite");


            $listnombreplainterecu=$repository->getNombrePlainte();
            $NombrePlainteJustifiee=$repository->getNombrePlainteJustifiee();
            $NombrePlainteMesureCorrectiveApportee=$repository3->getNombrePlainteMesureCorrectiveApportee($nbreJr->getJour());
            $NombrePlainteSatisfait=$repository2->getNombrePlainteSatisfait();
            $NombrePlainteNiveau1=$repository->getNombrePlainteNiveau1();
//        $NombrePlainteNiveau23=$repository->getNombrePlainteNiveau23();


//        var_dump($NombrePlainteMesureCorrectiveApportee); die();

            if ($NombrePlainteJustifiee["nbre"]!=0){
                $enregistrementPlainte=round(($listnombreplainterecu["nbre"]/$NombrePlainteJustifiee["nbre"])*100,2);
            }else{
                $enregistrementPlainte=0;
            }
            if ($NombrePlainteNiveau1["nbre"]!=0){
                $efficacité1=round(($NombrePlainteMesureCorrectiveApportee["nbre"]/$NombrePlainteNiveau1["nbre"])*100,2);
            }else{
                $efficacité1=0;
            }
//        if ($NombrePlainteNiveau23["nbre"]!=0){
//            $efficacité23=round(($NombrePlainteMesureCorrectiveApportee["nbre"]/$NombrePlainteNiveau23["nbre"])*100,2);
//        }else{
//            $efficacité23=0;
//        }
            if ($NombrePlainteJustifiee["nbre"]!=0){
                $satisfaction=round(($NombrePlainteSatisfait["nbre"]/$NombrePlainteJustifiee["nbre"])*100,2);
            }else{
                $satisfaction=0;
            }


            $indicateurs=$repositoryIndicateur->findBy(array(),
                array('id'=>'DESC')
            );
            return $this->render('GplainteBundle:Plainte:indicateur.html.twig', array(
                'list'=>$indicateurs,
                'enregistrementPlainte'=>$enregistrementPlainte,
                'efficacite1'=>$efficacité1,
//            'efficacite23'=>$efficacité23,
                'satisfaction'=>$satisfaction
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }


    public function PlaintesRecurrentesAction(){
        $em=$this->getDoctrine()->getManager();
        $trace= new trackError($em,$this->getRequest(),$this->container);

        try{
            $listePlaintes=$em->getRepository('GplainteBundle:Plainte')->getPlaintesRecurentes();
//        var_dump($xx); die();
            foreach ($listePlaintes as $plaintes):
                if ($plaintes['nbre']>=5){
                    $listeObjetPlaintes[$plaintes['libelle']]=$em->getRepository('GplainteBundle:Plainte')->getListePlainte($plaintes['libelle']);
//                        var_dump($listeObjetPlaintes); die();

                }
            endforeach;
        }catch (\Exception $e){
            $trace->traceError($e->getMessage(),$e->getLine());
        }

        return $this->render('GplainteBundle:Plainte:plaintesRecurrentes.html.twig', array(
            'objetsPlaintes'=>$listeObjetPlaintes
        ));


    }


    public function showDenonciationAction($val,$id)
    {
        $em=$this->getDoctrine()->getManager();
        $repository = $em->getRepository('GplainteBundle:Denonciation');
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }
        $trace= new trackError($em,$this->getRequest(),$this->container);
        $userEvent = $this->container->get('security.context')->getToken()->getUser();

//        pour afficher les details
        if ($val==1){
            $detail=$repository->find($id);
            $trace->trackEvent(19);
            return $this->render('GplainteBundle:Plainte:detailDenonciation.html.twig',array(
                'list'=>$detail,
                'val'=>$val,
                'id'=>$id
            ));
        }elseif($val==='justifier'){
//            echo "bonjr"; die();
            $detail=$repository->find($id);
            $trace->trackEvent(19);

            return $this->render('GplainteBundle:Plainte:detailDenonciation.html.twig',array(
                'list'=>$detail,
                'val'=>$val,
                'id'=>$id
            ));
        }

        if ($val==2){
            $detail=$repository->find($id);
//            var_dump($detail); die();
            $detail->setJustification($_POST['ch']);
            $em->persist($detail);
            $em->flush();
            $listplainte=$repository->getListeDenonciation();
            $trace->trackEvent(20);

            return $this->render('GplainteBundle:Plainte:showDenonciation.html.twig',array(
                'list'=>$listplainte,
                'val'=>$val,
                'id'=>$id
            ));
        }

        if ($val==3){

            $listplainte=$repository->getListeDenonciation();
            return $this->render('GplainteBundle:Plainte:showDenonciation.html.twig',array(
                'list'=>$listplainte,
                'val'=>$val,
                'id'=>$id
            ));
        }

        $listplainte=$repository->getListeDenonciation();

        return $this->render('GplainteBundle:Plainte:showDenonciation.html.twig',array(
            'list'=>$listplainte,
            'val'=>$val,
            'id'=>$id
        ));

    }

    public function solutionDenonciationAction($id,$val){
            $em=$this->getDoctrine()->getManager();
            $repository=$em->getRepository('GplainteBundle:Denonciation');

            $detail=$repository->find($id);

//  --------------------- notification à l'appli mobile que la plainte a été traiter -------------------------------------
        $identifiantplaintemobile=$detail->getCodeplaintemobile();
        if ($identifiantplaintemobile!=NULL){
            $param=new ParametresController();
            $param->notificationMobileDenonciationTraiter($identifiantplaintemobile);
        }
//------------------------------------------------------------------------------------------------------------------------

            $detail->setJustification($_POST['ch']);
            if (isset($_POST['solution']) && $_POST['solution']!=''){
                $detail->setSolution($_POST['solution']);
            }

            $em->persist($detail);
            $em->flush();
            $listplainte=$repository->getListeDenonciation();
            return $this->render('GplainteBundle:Plainte:showDenonciation.html.twig',array(
                'list'=>$listplainte,
                'id'=>$id,
                'val'=>$val,
                'msg'=>1
    ));
}
public function showUserComplaintAction($id)
{
    $em = $this->getDoctrine()->getManager();
    $repository = $em->getRepository('GplainteBundle:Plainte');

    $plaintes = $repository->findByAgent($id);
    return $this->render('GplainteBundle:Plainte:userPlainte.html.twig',array(
        'plaintes'=>$plaintes,
        'id'=>$id,
        'msg'=>1
    ));
}

//    public function sendSMSAction(){
//
//    }

}
