<?php
namespace GPlainte\GPlainteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GPlainte\GPlainteBundle\Entity\Affecter;
use GPlainte\GPlainteBundle\Form\AffecterTraitementType;
use GPlainte\GPlainteBundle\Form\AffecterHandler2;
use GPlainte\GPlainteBundle\Entity\Approuver;
use GPlainte\GPlainteBundle\Form\AffecterHandler;
use GPlainte\GPlainteBundle\Entity\Notification;
use GPlainte\GPlainteBundle\Journal\JournalEvents;
use GPlainte\GPlainteBundle\Journal\SaveComplaintEvent;
use GPlainte\GPlainteBundle\Journal\ValuesEvent;

class AffecterController extends Controller
{

    public function addTraiterAction($val,$id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Affecter');
            $repositoryPlainte = $em->getRepository('GplainteBundle:Plainte');

            $userEvent = $this->container->get('security.context')->getToken()->getUser();
            $user = $userEvent->getAgent();

            $listeAffectations = $repository->findBy(
                array(),
                array('id'=>'DESC')
            );
            if ($val=="ajouter"){
                $Affecter = $repository->findOneByPlainte($id);
                $plainte=$repositoryPlainte->find($id);
                $form = $this->createForm(new AffecterTraitementType(), $Affecter);
                if ($plainte->getTraitement() == 0) {
                    $plainte->setTraitement(1);
                    $Affecter->setNonapprouver(0);
                    $em->persist($plainte);
                    $em->persist($Affecter);
                    $formHandler = new AffecterHandler2($form, $this->get('request'), $this->getDoctrine()->getManager(), $user);
                    $request = $this->getRequest();
                    $data = $request->request->get($form->getName());

                    if ($formHandler->process()) {
//                $repository = $em->getRepository('GplainteBundle:Affecter');
                        $listeAffectations = $repository->findBy(
                            array(),
                            array('id' => 'DESC')
                        );

                        $action=$em->getRepository('UserBundle:DataEvent')->find(7);
                        $event= new SaveComplaintEvent($action,$userEvent);
                        $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

                        return $this->render('GplainteBundle:Affecter:addTraiter.html.twig', array('traiter' => $form->createView(), 'msg' => 1,
                            'list' => $listeAffectations,
                            'val' => $val,
                            'id' => $id
                        ));

                    } else {
                        /*return $this->render('GplainteBundle:Affecter:addTraiter.html.twig',array('traiter' => $form->createView(),'msg'=>1,
                            'list'=>$listeAffectations,
                            'val'=>$val,
                            'id'=>$id
                        ));*/
                        return $this->redirect($this->generateUrl('gplainte_show_affecter', array('list' => $listeAffectations)));
                    }
                }else{
                    return $this->render('GplainteBundle:Affecter:addTraiter.html.twig', array('traiter' => $form->createView(), 'msg' => 0,
                        'list' => $listeAffectations,
                        'val' => $val,
                        'id' => $id
                    ));
                }
            }

            if ($val=="modifier" && $id!=0){

                if( !$affecter = $em->getRepository('GplainteBundle:Affecter')->find($id) )
                {
                    $repository = $em->getRepository('GplainteBundle:Affecter');
                    $form = $this->createForm(new AffecterTraitementType(), $affecter);
                    $listeAffectations = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Affecter:addTraiter.html.twig',array('traiter' => $form->createView(),'msgErr'=>"Cet enregistrement n'existe pas",
                        'list'=>$listeAffectations,
                        'val'=>$val,
                        'id'=>$id
                    ));
                }

                $form = $this->createForm(new AffecterTraitementType(), $affecter);
                $formHandler = new AffecterHandler2($form, $this->get('request'), $this->getDoctrine()->getManager(), $user);
                if($formHandler->process())
                {
                    $repository = $em->getRepository('GplainteBundle:Affecter');
                    $listeAffectations = $repository->findBy(
                        array(),
                        array('id'=>'DESC')
                    );
                    return $this->render('GplainteBundle:Affecter:addTraiter.html.twig',array('traiter' => $form->createView(),'msg'=>2,
                        'list'=>$listeAffectations,
                        'val'=>$val,
                        'id'=>$id
                    ));
                }
            }
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

        
    }

    public function showTraiterAction()
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Affecter');
            $userEvent = $this->container->get('security.context')->getToken()->getUser();

            $listetraiter=$repository->getListeTraiter();

            $action=$em->getRepository('UserBundle:DataEvent')->find(8);
            $event= new SaveComplaintEvent($action,$userEvent);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:Affecter:showTraiter.html.twig', array(
                'list'=>$listetraiter,

            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }



    }

    public function detailTraiterAction($id)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Affecter');
            $userEvent = $this->container->get('security.context')->getToken()->getUser();

            $listetraiter=$repository->getListeTraitement($id);

            $action=$em->getRepository('UserBundle:DataEvent')->find(9);
            $event= new SaveComplaintEvent($action,$userEvent);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:Affecter:detailTraitement.html.twig', array(
                'list'=>$listetraiter,
//                'val'=>$val,
                'id'=>$id
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }


    }


    public function approbationTraiterAction($id,$val)
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Affecter');

            $affectation=$repository->find($id);
            $user = $this->container->get('security.context')->getToken()->getUser();
            if ($val==1){
                $approuver=new Approuver();
                $approuver->setNom($user->getAgent()->getNom());
                $approuver->setPrenom($user->getAgent()->getPrenom());
                $approuver->setAffecter($affectation);
                $affectation->setApprobation(1);
                $affectation->setNonapprouver(0);
                $em->persist($approuver);
                $em->persist($affectation);
                $em->flush();
                $action=$em->getRepository('UserBundle:DataEvent')->find(10);
                $event= new SaveComplaintEvent($action,$user);
                $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);
            }elseif ($val==2){
                $affectation->getPlainte()->setTraitement(0);
                $affectation->setNonapprouver(1);
                $em->persist($affectation);
                $em->flush();

                $action=$em->getRepository('UserBundle:DataEvent')->find(11);
                $event= new SaveComplaintEvent($action,$user);
                $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);
            }


            $listetraiter=$repository->getListeTraiter();

            return $this->render('GplainteBundle:Affecter:showTraiter.html.twig', array(
                'list'=>$listetraiter,

            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }


    }

    public function showPlainteApprouverAction()
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $em=$this->getDoctrine()->getManager();
            $repository = $em->getRepository('GplainteBundle:Affecter');
            $userEvent = $this->container->get('security.context')->getToken()->getUser();

            $listetraiter=$repository->getListePlaintesApprouver();

            $action=$em->getRepository('UserBundle:DataEvent')->find(12);
            $event= new SaveComplaintEvent($action,$userEvent);
            $this->get('event_dispatcher')->dispatch (JournalEvents::onSaveComplaint , $event);

            return $this->render('GplainteBundle:Affecter:showPlainteApprouver.html.twig', array(
                'list'=>$listetraiter,

            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

}