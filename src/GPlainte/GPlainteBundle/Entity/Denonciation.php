<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Denonciation
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\DenonciationRepository")
 */
class Denonciation
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Agent",inversedBy="denonciation", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $agent;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\NormeIndicateur",inversedBy="denonciation", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $indicateur;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Produit",inversedBy="denonciation", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $produit;


    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Niveau",inversedBy="denonciation", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $niveau;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Service",inversedBy="denonciation", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $service;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Autres",inversedBy="denonciation", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $autres;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Bureau",inversedBy="denonciation", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Veuillez selectionner le bureau")
     */
    private $bureau;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Choix",inversedBy="denonciation", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=false)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Veuillez sélectionner la formulation de votre plainte")
     */
    private $choix;


    /**
     * @var \DateTime
     *
     * @Doctrine\ORM\Mapping\Column(name="dateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @Doctrine\ORM\Mapping\Column(name="dateModification", type="datetime")
     */
    private $dateModification;


    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="autreobjet", type="text", nullable=true)
     */
    private $autreobjet;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="description", type="text", nullable=true)
     */
    private $description;




    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="justification", type="text", nullable=true)
     */
    private $justification;

    /**
     * @var text
     *
     * @Doctrine\ORM\Mapping\Column(name="solution", type="text", nullable=true)
     */
    private $solution;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="nom", type="string", nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="telephone", type="string", nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="agence", type="string", nullable=true)
     */
    private $agence;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="etatclient", type="boolean")
     */
    private $etatclient;

    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="mobile", type="boolean", nullable=false)
     */
    private $mobile;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="codeplaintemobile", type="text", nullable=true)
     */
    private $codeplaintemobile;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dateCreation=new \DateTime();
        $this->dateModification=new \DateTime();
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     * @return Denonciation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime 
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModification
     *
     * @param \DateTime $dateModification
     * @return Denonciation
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification
     *
     * @return \DateTime 
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * Set autreobjet
     *
     * @param string $autreobjet
     * @return Denonciation
     */
    public function setAutreobjet($autreobjet)
    {
        $this->autreobjet = $autreobjet;

        return $this;
    }

    /**
     * Get autreobjet
     *
     * @return string 
     */
    public function getAutreobjet()
    {
        return $this->autreobjet;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Denonciation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }



    /**
     * Set agent
     *
     * @param \GPlainte\GPlainteBundle\Entity\Agent $agent
     * @return Denonciation
     */
    public function setAgent(\GPlainte\GPlainteBundle\Entity\Agent $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \GPlainte\GPlainteBundle\Entity\Agent 
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set indicateur
     *
     * @param \GPlainte\GPlainteBundle\Entity\NormeIndicateur $indicateur
     * @return Denonciation
     */
    public function setIndicateur(\GPlainte\GPlainteBundle\Entity\NormeIndicateur $indicateur = null)
    {
        $this->indicateur = $indicateur;

        return $this;
    }

    /**
     * Get indicateur
     *
     * @return \GPlainte\GPlainteBundle\Entity\NormeIndicateur 
     */
    public function getIndicateur()
    {
        return $this->indicateur;
    }

    /**
     * Set produit
     *
     * @param \GPlainte\GPlainteBundle\Entity\Produit $produit
     * @return Denonciation
     */
    public function setProduit(\GPlainte\GPlainteBundle\Entity\Produit $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \GPlainte\GPlainteBundle\Entity\Produit 
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set niveau
     *
     * @param \GPlainte\GPlainteBundle\Entity\Niveau $niveau
     * @return Denonciation
     */
    public function setNiveau(\GPlainte\GPlainteBundle\Entity\Niveau $niveau = null)
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * Get niveau
     *
     * @return \GPlainte\GPlainteBundle\Entity\Niveau 
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * Set service
     *
     * @param \GPlainte\GPlainteBundle\Entity\Service $service
     * @return Denonciation
     */
    public function setService(\GPlainte\GPlainteBundle\Entity\Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \GPlainte\GPlainteBundle\Entity\Service 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set autres
     *
     * @param \GPlainte\GPlainteBundle\Entity\Autres $autres
     * @return Denonciation
     */
    public function setAutres(\GPlainte\GPlainteBundle\Entity\Autres $autres = null)
    {
        $this->autres = $autres;

        return $this;
    }

    /**
     * Get autres
     *
     * @return \GPlainte\GPlainteBundle\Entity\Autres 
     */
    public function getAutres()
    {
        return $this->autres;
    }

    /**
     * Set bureau
     *
     * @param \GPlainte\GPlainteBundle\Entity\Bureau $bureau
     * @return Denonciation
     */
    public function setBureau(\GPlainte\GPlainteBundle\Entity\Bureau $bureau = null)
    {
        $this->bureau = $bureau;

        return $this;
    }

    /**
     * Get bureau
     *
     * @return \GPlainte\GPlainteBundle\Entity\Bureau 
     */
    public function getBureau()
    {
        return $this->bureau;
    }

    /**
     * Set choix
     *
     * @param \GPlainte\GPlainteBundle\Entity\Choix $choix
     * @return Denonciation
     */
    public function setChoix(\GPlainte\GPlainteBundle\Entity\Choix $choix)
    {
        $this->choix = $choix;

        return $this;
    }

    /**
     * Get choix
     *
     * @return \GPlainte\GPlainteBundle\Entity\Choix 
     */
    public function getChoix()
    {
        return $this->choix;
    }



    /**
     * Set justification
     *
     * @param string $justification
     * @return Denonciation
     */
    public function setJustification($justification)
    {
        $this->justification = $justification;

        return $this;
    }

    /**
     * Get justification
     *
     * @return string 
     */
    public function getJustification()
    {
        return $this->justification;
    }

    /**
     * Set solution
     *
     * @param string $solution
     * @return Denonciation
     */
    public function setSolution($solution)
    {
        $this->solution = $solution;

        return $this;
    }

    /**
     * Get solution
     *
     * @return string 
     */
    public function getSolution()
    {
        return $this->solution;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Denonciation
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Denonciation
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set agence
     *
     * @param string $agence
     * @return Denonciation
     */
    public function setAgence($agence)
    {
        $this->agence = $agence;

        return $this;
    }

    /**
     * Get agence
     *
     * @return string 
     */
    public function getAgence()
    {
        return $this->agence;
    }

   

    /**
     * Set etatclient
     *
     * @param boolean $etatclient
     * @return Denonciation
     */
    public function setEtatclient($etatclient)
    {
        $this->etatclient = $etatclient;

        return $this;
    }

    /**
     * Get etatclient
     *
     * @return boolean 
     */
    public function getEtatclient()
    {
        return $this->etatclient;
    }

    /**
     * Set mobile
     *
     * @param boolean $mobile
     * @return Denonciation
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return boolean 
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set codeplaintemobile
     *
     * @param string $codeplaintemobile
     * @return Denonciation
     */
    public function setCodeplaintemobile($codeplaintemobile)
    {
        $this->codeplaintemobile = $codeplaintemobile;

        return $this;
    }

    /**
     * Get codeplaintemobile
     *
     * @return string 
     */
    public function getCodeplaintemobile()
    {
        return $this->codeplaintemobile;
    }
}
