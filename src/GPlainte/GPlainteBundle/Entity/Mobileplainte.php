<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mobileplainte
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\MobileplainteRepository")
 */
class Mobileplainte
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="etat", type="boolean")
     */
    private $etat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="etatclient", type="boolean")
     */
    private $etatclient;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=12)
     */
    private $telephone;

    /**
     * @ORM\Column(name="bureau", type="string", length=200, nullable=true)
     */
    private $bureau;

    /**
     * @ORM\Column(name="sfd", type="string", length=200)
     */
    private $sfd;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Niveau",inversedBy="plainte", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $niveau;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Choix",inversedBy="plainte", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=false)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Veuillez sélectionner la formulation de votre plainte")
     */
    private $choix;

    /**
     * @var \DateTime
     *
     * @Doctrine\ORM\Mapping\Column(name="dateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @Doctrine\ORM\Mapping\Column(name="dateModification", type="datetime")
     */
    private $dateModification;


    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="autreobjet", type="text", nullable=true)
     */
    private $autreobjet;

    function __construct() {
        $this->dateCreation=new \DateTime();
        $this->dateModification=new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Mobileplainte
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Mobileplainte
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set etat
     *
     * @param boolean $etat
     * @return Mobileplainte
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return boolean 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Mobileplainte
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     * @return Mobileplainte
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime 
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModification
     *
     * @param \DateTime $dateModification
     * @return Mobileplainte
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification
     *
     * @return \DateTime 
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * Set autreobjet
     *
     * @param string $autreobjet
     * @return Mobileplainte
     */
    public function setAutreobjet($autreobjet)
    {
        $this->autreobjet = $autreobjet;

        return $this;
    }

    /**
     * Get autreobjet
     *
     * @return string 
     */
    public function getAutreobjet()
    {
        return $this->autreobjet;
    }


    /**
     * Set niveau
     *
     * @param \GPlainte\GPlainteBundle\Entity\Niveau $niveau
     * @return Mobileplainte
     */
    public function setNiveau(\GPlainte\GPlainteBundle\Entity\Niveau $niveau = null)
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * Get niveau
     *
     * @return \GPlainte\GPlainteBundle\Entity\Niveau 
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * Set choix
     *
     * @param \GPlainte\GPlainteBundle\Entity\Choix $choix
     * @return Mobileplainte
     */
    public function setChoix(\GPlainte\GPlainteBundle\Entity\Choix $choix)
    {
        $this->choix = $choix;

        return $this;
    }

    /**
     * Get choix
     *
     * @return \GPlainte\GPlainteBundle\Entity\Choix 
     */
    public function getChoix()
    {
        return $this->choix;
    }

    /**
     * Set bureau
     *
     * @param string $bureau
     * @return Mobileplainte
     */
    public function setBureau($bureau)
    {
        $this->bureau = $bureau;

        return $this;
    }

    /**
     * Get bureau
     *
     * @return string 
     */
    public function getBureau()
    {
        return $this->bureau;
    }

    /**
     * Set sfd
     *
     * @param string $sfd
     * @return Mobileplainte
     */
    public function setSfd($sfd)
    {
        $this->sfd = $sfd;

        return $this;
    }

    /**
     * Get sfd
     *
     * @return string 
     */
    public function getSfd()
    {
        return $this->sfd;
    }

    /**
     * Set etatclient
     *
     * @param boolean $etatclient
     * @return Mobileplainte
     */
    public function setEtatclient($etatclient)
    {
        $this->etatclient = $etatclient;

        return $this;
    }

    /**
     * Get etatclient
     *
     * @return boolean 
     */
    public function getEtatclient()
    {
        return $this->etatclient;
    }
}
