<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Satisfaction
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\SatisfactionRepository")
 */
class Satisfaction
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="libelle", type="string", length=5)
     */
    private $libelle;

    /**
     * @var datetime
     *
     * @Doctrine\ORM\Mapping\Column(name="rdv", type="datetime", nullable=true)
     */
    private $rdv;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="reformulation", type="text", nullable=true)
     */
    private $reformulation;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="verifier", type="string", nullable=true)
     */
    private $verifier;

    /**
     * @var \DateTime
     *
     * @Doctrine\ORM\Mapping\Column(name="dateSatisfaction", type="datetime", nullable=true)
     */
    private $dateSatisfaction;
    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="mention", type="boolean", nullable=true)
     */
    private $mention;

    public function __construct(){
        $this->dateSatisfaction=new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Satisfaction
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return Satisfaction
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set rdv
     *
     * @param \DateTime $rdv
     * @return Satisfaction
     */
    public function setRdv($rdv)
    {
        $this->rdv = $rdv;

        return $this;
    }

    /**
     * Get rdv
     *
     * @return \DateTime 
     */
    public function getRdv()
    {
        return $this->rdv;
    }

    /**
     * Set reformulation
     *
     * @param string $reformulation
     * @return Satisfaction
     */
    public function setReformulation($reformulation)
    {
        $this->reformulation = $reformulation;

        return $this;
    }

    /**
     * Get reformulation
     *
     * @return string 
     */
    public function getReformulation()
    {
        return $this->reformulation;
    }



    /**
     * Set verifier
     *
     * @param string $verifier
     * @return Satisfaction
     */
    public function setVerifier($verifier)
    {
        $this->verifier = $verifier;

        return $this;
    }

    /**
     * Get verifier
     *
     * @return string 
     */
    public function getVerifier()
    {
        return $this->verifier;
    }

    /**
     * Set dateSatisfaction
     *
     * @param \DateTime $dateSatisfaction
     * @return Satisfaction
     */
    public function setDateSatisfaction($dateSatisfaction)
    {
        $this->dateSatisfaction = $dateSatisfaction;

        return $this;
    }

    /**
     * Get dateSatisfaction
     *
     * @return \DateTime 
     */
    public function getDateSatisfaction()
    {
        return $this->dateSatisfaction;
    }
    /**
     * Set mention
     *
     * @param boolean $mention
     * @return Plainte
     */
    public function setMention($mention)
    {
        $this->mention = $mention;

        return $this;
    }

    /**
     * Get mention
     *
     * @return boolean
     */
    public function getMention()
    {
        return $this->mention;
    }

}
