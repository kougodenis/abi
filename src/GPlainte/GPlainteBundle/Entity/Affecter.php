<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Affecter
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\AffecterRepository")
 */
class Affecter
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Service")
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=false)
     */
    private $service;
    
    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Plainte",inversedBy="affecter", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=false)
     */
    private $plainte;


    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Action",inversedBy="affecter", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $action;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Agent",inversedBy="affecter", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $agent;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Approuver",mappedBy="affecter")
     */
    private $approuver;
    /**
     * @var \DateTime
     *
     * @Doctrine\ORM\Mapping\Column(name="dateAffectation", type="datetime")
     */
    private $dateAffectation;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="recevable", type="string", length=10, nullable=true)
     */
    private $recevable;

    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="approbation", type="boolean")
     */
    private $approbation;

    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="nonapprouver", type="boolean")
     */
    private $nonapprouver;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Set dateAffectation
     *
     * @param \DateTime $dateAffectation
     * @return Affecter
     */
    public function setDateAffectation($dateAffectation)
    {
        $this->dateAffectation = $dateAffectation;

        return $this;
    }

    /**
     * Get dateAffectation
     *
     * @return \DateTime 
     */
    public function getDateAffectation()
    {
        return $this->dateAffectation;
    }

    /**
     * Set recevable
     *
     * @param string $recevable
     * @return Affecter
     */
    public function setRecevable($recevable)
    {
        $this->recevable = $recevable;

        return $this;
    }

    /**
     * Get recevable
     *
     * @return string 
     */
    public function getRecevable()
    {
        return $this->recevable;
    }

    /**
     * Set service
     *
     * @param \GPlainte\GPlainteBundle\Entity\Service $service
     * @return Affecter
     */
    public function setService(\GPlainte\GPlainteBundle\Entity\Service $service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \GPlainte\GPlainteBundle\Entity\Service 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set plainte
     *
     * @param \GPlainte\GPlainteBundle\Entity\Plainte $plainte
     * @return Affecter
     */
    public function setPlainte(\GPlainte\GPlainteBundle\Entity\Plainte $plainte)
    {
        $this->plainte = $plainte;

        return $this;
    }

    /**
     * Get plainte
     *
     * @return \GPlainte\GPlainteBundle\Entity\Plainte 
     */
    public function getPlainte()
    {
        return $this->plainte;
    }

    /**
     * Set action
     *
     * @param \GPlainte\GPlainteBundle\Entity\Action $action
     * @return Affecter
     */
    public function setAction(\GPlainte\GPlainteBundle\Entity\Action $action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return \GPlainte\GPlainteBundle\Entity\Action 
     */
    public function getAction()
    {
        return $this->action;
    }


    /**
     * Set agent
     *
     * @param \GPlainte\GPlainteBundle\Entity\Agent $agent
     * @return Affecter
     */
    public function setAgent(\GPlainte\GPlainteBundle\Entity\Agent $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \GPlainte\GPlainteBundle\Entity\Agent 
     */
    public function getAgent()
    {
        return $this->agent;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->approuver = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add approuver
     *
     * @param \GPlainte\GPlainteBundle\Entity\Approuver $approuver
     * @return Affecter
     */
    public function addApprouver(\GPlainte\GPlainteBundle\Entity\Approuver $approuver)
    {
        $this->approuver[] = $approuver;

        return $this;
    }

    /**
     * Remove approuver
     *
     * @param \GPlainte\GPlainteBundle\Entity\Approuver $approuver
     */
    public function removeApprouver(\GPlainte\GPlainteBundle\Entity\Approuver $approuver)
    {
        $this->approuver->removeElement($approuver);
    }

    /**
     * Get approuver
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getApprouver()
    {
        return $this->approuver;
    }

    /**
     * Set approbation
     *
     * @param boolean $approbation
     * @return Affecter
     */
    public function setApprobation($approbation)
    {
        $this->approbation = $approbation;

        return $this;
    }

    /**
     * Get approbation
     *
     * @return boolean 
     */
    public function getApprobation()
    {
        return $this->approbation;
    }



    /**
     * Set nonapprouver
     *
     * @param boolean $nonapprouver
     * @return Affecter
     */
    public function setNonapprouver($nonapprouver)
    {
        $this->nonapprouver = $nonapprouver;

        return $this;
    }

    /**
     * Get nonapprouver
     *
     * @return boolean 
     */
    public function getNonapprouver()
    {
        return $this->nonapprouver;
    }
}
