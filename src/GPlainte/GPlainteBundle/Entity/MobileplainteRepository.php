<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * MobileplainteRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MobileplainteRepository extends EntityRepository
{
    public function getLastInsertDenonciation(){
        $query=$this->_em->createQuery("SELECT n.id as code FROM GplainteBundle:Mobileplainte n  ORDER BY n.id DESC")
            ->setMaxResults(1);
        return $query->getSingleResult();
    }
}
