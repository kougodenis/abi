<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Institution
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\InstitutionRepository")
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
class Institution
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Service",mappedBy="institution")
     */
    private $service;
    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Agence",mappedBy="institution")
     */
    private $agence;
    
    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="denominationSociale", type="string", length=255)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Précisez le denomination sociale")
     */
    private $denominationSociale;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="contact", type="string", length=50)
     * /**
     * @var string
     *
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Le numéro ne doit pas être vide")
     * @Symfony\Component\Validator\Constraints\Type(type="numeric", message="Le numéro téléphone n'est pas valide")
     * @Symfony\Component\Validator\Constraints\Length(
     *      min = 8,
     *      max = 10,
     *      minMessage = "Le numéro de téléphone doit contenir au moins {{ limit }} chiffres")
     *
     */
    private $contact;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="email", type="string", length=50)
     * @Symfony\Component\Validator\Constraints\Email(message = "L'email '{{ value }}' n'est pas valide.")
     */
    private $email;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="numAgrement", type="string", length=200)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Précisez le numéro agrément")
     */
    private $numAgrement;

    /**
     * @Doctrine\ORM\Mapping\Column(type="string", length=255, nullable=true)
     * @Symfony\Component\Validator\Constraints\Blank
     */
    protected $name;
    /**
     * @Doctrine\ORM\Mapping\Column(type="string", length=255, nullable=true)
     */
    protected $path;
    /**
     * @Symfony\Component\Validator\Constraints\File(maxSize="6000000")
     */
    public $file;


    private $tempFilename ;

    public function setFile ( UploadedFile $file )
 {
   $this->file = $file ;

        // On vé rifie si on avait déjà un fichier pour cette entit é
    if ( null !== $this->path) {
        // On sauvegarde l'extension du fichier pour le supprimerplus tard
    $this->tempFilename = $this->path;

        // On ré initialise les valeurs des attributs url et alt
    $this->path = null ;
    }
    }


    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path;
    }
    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir().'/'.$this->path;
    }
    protected function getUploadRootDir()
    {
        // le chemin absolu du répertoire où les documents uploadés doivent être sauvegardés
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    protected function getUploadDir()
    {
        return 'uploads/photo';
    }


    /**
     * @Doctrine\ORM\Mapping\PrePersist()
     * @Doctrine\ORM\Mapping\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            // faites ce que vous voulez pour générer un nom unique
            $this->path = sha1(uniqid(mt_rand(), true)).'.'.$this->file->guessExtension();
        }
    }

    /**
     * @Doctrine\ORM\Mapping\PostPersist()
     * @Doctrine\ORM\Mapping\PostUpdate()
     */
    public function upload()
    {
        // la propriété « file » peut être vide si le champ n'est pas requis
        if (null === $this->file) {
            return;
        }

        $this->file->move($this->getUploadRootDir(), $this->path);
        unset($this->file);
    }

    /**
     * @Doctrine\ORM\Mapping\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set denominationSociale
     *
     * @param string $denominationSociale
     * @return Institution
     */
    public function setDenominationSociale($denominationSociale)
    {
        $this->denominationSociale = $denominationSociale;

        return $this;
    }

    /**
     * Get denominationSociale
     *
     * @return string 
     */
    public function getDenominationSociale()
    {
        return $this->denominationSociale;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return Institution
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Institution
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set numAgrement
     *
     * @param string $numAgrement
     * @return Institution
     */
    public function setNumAgrement($numAgrement)
    {
        $this->numAgrement = $numAgrement;

        return $this;
    }

    /**
     * Get numAgrement
     *
     * @return string 
     */
    public function getNumAgrement()
    {
        return $this->numAgrement;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->service = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add service
     *
     * @param \GPlainte\GPlainteBundle\Entity\Service $service
     * @return Institution
     */
    public function addService(\GPlainte\GPlainteBundle\Entity\Service $service)
    {
        $this->service[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \GPlainte\GPlainteBundle\Entity\Service $service
     */
    public function removeService(\GPlainte\GPlainteBundle\Entity\Service $service)
    {
        $this->service->removeElement($service);
    }

    /**
     * Get service
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Add agence
     *
     * @param \GPlainte\GPlainteBundle\Entity\Agence $agence
     * @return Institution
     */
    public function addAgence(\GPlainte\GPlainteBundle\Entity\Agence $agence)
    {
        $this->agence[] = $agence;

        return $this;
    }

    /**
     * Remove agence
     *
     * @param \GPlainte\GPlainteBundle\Entity\Agence $agence
     */
    public function removeAgence(\GPlainte\GPlainteBundle\Entity\Agence $agence)
    {
        $this->agence->removeElement($agence);
    }

    /**
     * Get agence
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAgence()
    {
        return $this->agence;
    }
    
    function getName() {
        return $this->name;
    }

    function getPath() {
        return $this->path;
    }


    function setName($name) {
        $this->name = $name;
        return $this;
    }

    function setPath($path) {
        $this->path = $path;
        return $this;
    }

    


}
