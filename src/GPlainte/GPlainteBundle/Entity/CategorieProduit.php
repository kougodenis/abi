<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategorieProduit
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\CategorieProduitRepository")
 */
class CategorieProduit
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Produit",mappedBy="categorieproduit")
     */
    private $produit;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="libelle", type="string", length=200)
     */
    private $libelle;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return CategorieProduit
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produit = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add produit
     *
     * @param \GPlainte\GPlainteBundle\Entity\Produit $produit
     * @return CategorieProduit
     */
    public function addProduit(\GPlainte\GPlainteBundle\Entity\Produit $produit)
    {
        $this->produit[] = $produit;

        return $this;
    }

    /**
     * Remove produit
     *
     * @param \GPlainte\GPlainteBundle\Entity\Produit $produit
     */
    public function removeProduit(\GPlainte\GPlainteBundle\Entity\Produit $produit)
    {
        $this->produit->removeElement($produit);
    }

    /**
     * Get produit
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProduit()
    {
        return $this->produit;
    }
}
