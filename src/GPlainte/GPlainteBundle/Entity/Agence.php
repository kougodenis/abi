<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Agence
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\AgenceRepository")
 */
class Agence
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Institution",inversedBy="agence", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=false)
     */
    private $institution;
    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Bureau",mappedBy="agence")
     */
    private $bureau;
    
    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="libelle", type="string", length=255)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Le libelle ne doit pas être vide")
     */
    private $libelle;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Agence
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bureau = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set institution
     *
     * @param \GPlainte\GPlainteBundle\Entity\Institution $institution
     * @return Agence
     */
    public function setInstitution(\GPlainte\GPlainteBundle\Entity\Institution $institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get institution
     *
     * @return \GPlainte\GPlainteBundle\Entity\Institution 
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Add bureau
     *
     * @param \GPlainte\GPlainteBundle\Entity\Bureau $bureau
     * @return Agence
     */
    public function addBureau(\GPlainte\GPlainteBundle\Entity\Bureau $bureau)
    {
        $this->bureau[] = $bureau;

        return $this;
    }

    /**
     * Remove bureau
     *
     * @param \GPlainte\GPlainteBundle\Entity\Bureau $bureau
     */
    public function removeBureau(\GPlainte\GPlainteBundle\Entity\Bureau $bureau)
    {
        $this->bureau->removeElement($bureau);
    }

    /**
     * Get bureau
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBureau()
    {
        return $this->bureau;
    }
}
