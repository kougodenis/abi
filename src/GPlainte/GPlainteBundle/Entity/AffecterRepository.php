<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TraiterRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AffecterRepository extends EntityRepository
{
    public function getListeAffecter($service){
        $query=$this->_em->createQuery("SELECT a,p FROM GplainteBundle:Affecter a JOIN a.plainte p WHERE p.affection='1' and a.service='$service' and p.traitement='0' ORDER BY a.id DESC");
        return $query->getResult();
    }

    public function getListePlainteRejeterApresTraitement($service){
        $query=$this->_em->createQuery("SELECT a,p FROM GplainteBundle:Affecter a JOIN a.plainte p WHERE p.affection='1' and a.service='$service' and a.nonapprouver='1' ORDER BY a.id DESC");
        return $query->getResult();
    }

    public function getListeTraiter(){
        $query=$this->_em->createQuery("SELECT a,c FROM GplainteBundle:Affecter a JOIN a.action c WHERE a.approbation='0' and a.nonapprouver='0' ORDER BY a.id DESC");
        return $query->getResult();
    }

    public function getListeTraitement($id){
        $query=$this->_em->createQuery("SELECT a,c FROM GplainteBundle:Affecter a JOIN a.action c WHERE a.id='$id'");
        return $query->getSingleResult();
    }

    public function getListePlaintesApprouver(){
        $query=$this->_em->createQuery("SELECT a,c FROM GplainteBundle:Affecter a JOIN a.action c WHERE a.approbation='1' and c.notification=0");
        return $query->getResult();
    }

    public function getListeNotification(){
       $query=$this->_em->createQuery("SELECT a,c,n FROM GplainteBundle:Affecter a JOIN a.action c JOIN c.notifier n WHERE a.approbation='1' and c.notification=1");
        return $query->getResult();
    }


    public function getListeSatisfaction(){
        $query=$this->_em->createQuery("SELECT a,c,n FROM GplainteBundle:Affecter a JOIN a.action c JOIN c.notifier n JOIN n.satisfaction s WHERE a.approbation='1' and c.notification=1");
        return $query->getResult();
    }

    public function getListePlainteArchive(){
        $query=$this->_em->createQuery("SELECT a,c,n,s FROM GplainteBundle:Affecter a JOIN a.action c JOIN c.notifier n JOIN n.satisfaction s WHERE s.libelle='O'");
        return $query->getResult();
    }

    public function getEtatPlainteSuivi1($id){
        $query=$this->_em->createQuery("SELECT a,c,n,s FROM GplainteBundle:Affecter a JOIN a.action c JOIN c.notifier n JOIN n.satisfaction s WHERE a.plainte='$id'");
        return $query->getOneOrNullResult();
    }

    public function getEtatPlainteSuivi2($id){
        $query=$this->_em->createQuery("SELECT a,c,n FROM GplainteBundle:Affecter a JOIN a.action c JOIN c.notifier n WHERE a.plainte='$id'");
        return $query->getOneOrNullResult();
    }

    public function getEtatPlainteSuivi3($id){
        $query=$this->_em->createQuery("SELECT a,c FROM GplainteBundle:Affecter a JOIN a.action c WHERE a.plainte='$id'");
        return $query->getOneOrNullResult();
    }

    public function getEtatPlainteSuivi4($id){
        $query=$this->_em->createQuery("SELECT a FROM GplainteBundle:Affecter a WHERE a.plainte='$id'");
        return $query->getOneOrNullResult();
    }

//    public function getListePlainteSuivi(){
//        $query=$this->_em->createQuery("SELECT a,c,n,s FROM GplainteBundle:Affecter a JOIN a.action c JOIN c.notifier n JOIN n.satisfaction s ORDER BY a.plainte DESC");
//        return $query->getResult();
//    }
//
//    public function getListePlainteSuivi2(){
//        $query=$this->_em->createQuery("SELECT a,c,n FROM GplainteBundle:Affecter a JOIN a.action c JOIN c.notifier n ORDER BY a.plainte DESC");
//        return $query->getResult();
//    }
//
//    public function getListePlainteSuivi3(){
//        $query=$this->_em->createQuery("SELECT a,c FROM GplainteBundle:Affecter a JOIN a.action c ORDER BY a.plainte DESC");
//        return $query->getResult();
//    }
//
//    public function getListePlainteSuivi4(){
//        $query=$this->_em->createQuery("SELECT a FROM GplainteBundle:Affecter a ORDER BY a.plainte DESC");
//        return $query->getResult();
//    }

    public function getNombrePlainteMesureCorrectiveApportee($delai){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c  WHERE p.traitement=1 AND DATE_DIFF(c.dateAction,p.dateCreation)<=:delai");
        $query->setParameter('delai',$delai);
        return $query->getSingleResult();
    }

    public function getNombreNotification($debut,$fin){
        $query=$this->_em->createQuery("SELECT count(n.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN c.notifier n WHERE p.dateCreation BETWEEN :debut AND :fin");
        $query->setParameter('debut',$debut)
            ->setParameter('fin',$fin);
        return $query->getSingleResult();
    }

    public function getNombrePlainteTraiteDelais($debut,$fin,$delai){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN a.approuver v WHERE p.traitement=1 AND p.dateCreation BETWEEN :debut AND :fin AND DATE_DIFF(v.dateApprobation,a.dateAffectation)<=:delai");
        $query->setParameter('delai',$delai)
            ->setParameter('debut',$debut)
        ->setParameter('fin',$fin);
        return $query->getSingleResult();
    }

    public function getNombrePlainteResolue(){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c WHERE p.traitement=1");
        return $query->getSingleResult();
    }

    public function getNombrePlainteEnCoursTraitement(){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p WHERE p.affection='1' and p.traitement='0'");
        return $query->getSingleResult();
    }

    public function getTempsMoyenTraitementPlainte(){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre, AVG(DATE_DIFF(v.dateApprobation,a.dateAffectation)) as jr FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN a.approuver v JOIN p.niveau n JOIN n.typeniveau t WHERE p.traitement=1");
        return $query->getSingleResult();
    }

    public function getNombrePlainteSensibleEtOrdinaireTraiteDelais($debut,$fin,$delai,$gravite){
    $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN a.approuver v JOIN p.niveau n JOIN n.typeniveau t WHERE p.traitement=1 AND t.gravite=$gravite AND p.dateCreation BETWEEN :debut AND :fin AND DATE_DIFF(v.dateApprobation,a.dateAffectation)<=:delai");
    $query->setParameter('delai',$delai)
        ->setParameter('debut',$debut)
        ->setParameter('fin',$fin);
    return $query->getSingleResult();
}

    public function getNombrePlainteSensibleEtOrdinaireTraite($debut,$fin,$gravite){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN a.approuver v JOIN p.niveau n JOIN n.typeniveau t WHERE p.traitement=1 AND t.gravite=$gravite AND p.dateCreation BETWEEN :debut AND :fin");
        $query
            ->setParameter('debut',$debut)
            ->setParameter('fin',$fin);
        return $query->getSingleResult();
    }


    public function getNombrePlainteSensibleEtOrdinaireTraiteDelaisBureau($debut,$fin,$delai,$gravite,$bureau){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN a.approuver v JOIN p.niveau n JOIN n.typeniveau t WHERE p.bureau=:bureau AND p.traitement=1 AND t.gravite=$gravite AND p.dateCreation BETWEEN :debut AND :fin AND DATE_DIFF(v.dateApprobation,a.dateAffectation)<=:delai");
        $query->setParameter('delai',$delai)
            ->setParameter('debut',$debut)
            ->setParameter('fin',$fin)
            ->setParameter('bureau',$bureau);
        return $query->getSingleResult();
    }

    public function getNombrePlainteSensibleEtOrdinaireTraiteBureau($debut,$fin,$gravite,$bureau){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN a.approuver v JOIN p.niveau n JOIN n.typeniveau t WHERE p.bureau=:bureau AND p.traitement=1 AND t.gravite=$gravite AND p.dateCreation BETWEEN :debut AND :fin");
        $query
            ->setParameter('debut',$debut)
            ->setParameter('fin',$fin)
            ->setParameter('bureau',$bureau);
        return $query->getSingleResult();
    }




    public function getNombrePlainteTraiteDelaisDeuxSemaines($debut,$fin){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN a.approuver v WHERE p.traitement=1 AND p.dateCreation BETWEEN :debut AND :fin AND DATE_DIFF(v.dateApprobation,a.dateAffectation)<= 14 AND DATE_DIFF(v.dateApprobation,a.dateAffectation) > 7");
        $query
        ->setParameter('debut',$debut)
        ->setParameter('fin',$fin);
        return $query->getSingleResult();
    }
    public function getNombrePlainteTraiteDelaisMois($debut,$fin){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN a.approuver v WHERE p.traitement=1 AND p.dateCreation BETWEEN :debut AND :fin AND DATE_DIFF(v.dateApprobation,a.dateAffectation)<=30 AND DATE_DIFF(v.dateApprobation,a.dateAffectation) > 14");
        $query
        ->setParameter('debut',$debut)
        ->setParameter('fin',$fin);
        return $query->getSingleResult();
    }

    public function getNombrePlainteTraiteDelaisSuperieurMois($debut,$fin){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN a.approuver v WHERE p.traitement=1 AND p.dateCreation BETWEEN :debut AND :fin AND DATE_DIFF(v.dateApprobation,a.dateAffectation)>30");
        $query
        ->setParameter('debut',$debut)
        ->setParameter('fin',$fin);
        return $query->getSingleResult();
    }

    public function getNombrePlainteTraiteEnRetard($debut,$fin,$delai){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN a.approuver v WHERE p.traitement=1 AND p.dateCreation BETWEEN :debut AND :fin AND DATE_DIFF(v.dateApprobation,a.dateAffectation)>:delai");
        $query->setParameter('delai',$delai)
            ->setParameter('debut',$debut)
            ->setParameter('fin',$fin);
        return $query->getSingleResult();
    }

    public function getListeMesurePreventive($debut,$fin){
        $query=$this->_em->createQuery("SELECT c.prevention FROM GplainteBundle:Affecter a join a.action c WHERE c.dateAction BETWEEN '$debut' AND '$fin' ORDER BY c.id DESC");
        return $query->getResult();
    }

    public function getListeServicesLesPlusSollicites($debut,$fin){
        $query=$this->_em->createQuery("SELECT max(s.libelle) as maximum FROM GplainteBundle:Affecter a join a.service s WHERE a.dateAffectation BETWEEN :debut AND :fin GROUP BY s.id ORDER BY s.id DESC");
        $query->setParameter('debut',$debut)
            ->setParameter('fin',$fin);
        return $query->getResult();
    }

    public function getListeMesurePreventiveParAgence($debut,$fin){
        $query=$this->_em->createQuery("SELECT c.prevention, g.libelle FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN p.bureau b JOIN b.agence g WHERE p.traitement=1 and c.dateAction BETWEEN '$debut' AND '$fin'");
        return $query->getResult();
    }


    public function getNombrePlainteTraiteDelaisBureau($debut,$fin,$bureau,$delai){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN a.approuver v WHERE p.bureau=:bureau and p.traitement=1 AND p.dateCreation BETWEEN :debut AND :fin AND DATE_DIFF(v.dateApprobation,a.dateAffectation)<=:delai");
        $query->setParameter('delai',$delai)
            ->setParameter('debut',$debut)
            ->setParameter('fin',$fin)
            ->setParameter('bureau',$bureau);
        return $query->getSingleResult();
    }

    public function getNombrePlainteTraiteEnRetardBureau($debut,$fin,$bureau,$delai){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN a.approuver v WHERE p.bureau=:bureau and p.traitement=1 AND p.dateCreation BETWEEN :debut AND :fin AND DATE_DIFF(v.dateApprobation,a.dateAffectation)>:delai");
        $query->setParameter('delai',$delai)
            ->setParameter('debut',$debut)
            ->setParameter('fin',$fin)
            ->setParameter('bureau',$bureau);
        return $query->getSingleResult();
    }

    public function getNombreNotificationBureau($debut,$fin,$bureau){
        $query=$this->_em->createQuery("SELECT count(n.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN c.notifier n WHERE p.bureau=:bureau and p.dateCreation BETWEEN :debut AND :fin");
        $query->setParameter('debut',$debut)
            ->setParameter('fin',$fin)
            ->setParameter('bureau',$bureau);
        return $query->getSingleResult();
    }

    public function getNombrePlainteInsatisfaitBureau($debut,$fin,$bureau){
        $query=$this->_em->createQuery("SELECT count(p.id) as nbre FROM GplainteBundle:Affecter a JOIN a.plainte p JOIN a.action c JOIN c.notifier n JOIN n.satisfaction s WHERE p.bureau=:bureau and s.dateSatisfaction BETWEEN :debut and :fin and s.libelle='N' and s.reformulation!='NULL'");
        $query->setParameter('debut',$debut)
            ->setParameter('fin',$fin)
            ->setParameter('bureau',$bureau);
        return $query->getSingleResult();
    }

    public function getListeServicesLesPlusSollicitesBureau($debut,$fin,$bureau){
        $query=$this->_em->createQuery("SELECT max(s.libelle) as maximum FROM GplainteBundle:Affecter a join a.plainte p join a.service s WHERE p.bureau=:bureau and a.dateAffectation BETWEEN :debut AND :fin GROUP BY s.libelle");
        $query->setParameter('debut',$debut)
            ->setParameter('fin',$fin)
            ->setParameter('bureau',$bureau);
        return $query->getResult();
    }
}
