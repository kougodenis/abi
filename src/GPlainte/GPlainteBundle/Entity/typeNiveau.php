<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * typeNiveau
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\typeNiveauRepository")
 */
class typeNiveau
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Niveau",mappedBy="typeniveau")
     */
    private $niveau;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="libelle", type="string", length=200)
     */
    private $libelle;

    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="gravite", type="boolean")
     */
    private $gravite;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return typeNiveau
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->niveau = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add niveau
     *
     * @param \GPlainte\GPlainteBundle\Entity\Niveau $niveau
     * @return typeNiveau
     */
    public function addNiveau(\GPlainte\GPlainteBundle\Entity\Niveau $niveau)
    {
        $this->niveau[] = $niveau;

        return $this;
    }

    /**
     * Remove niveau
     *
     * @param \GPlainte\GPlainteBundle\Entity\Niveau $niveau
     */
    public function removeNiveau(\GPlainte\GPlainteBundle\Entity\Niveau $niveau)
    {
        $this->niveau->removeElement($niveau);
    }

    /**
     * Get niveau
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNiveau()
    {
        return $this->niveau;
    }
    

    /**
     * Set gravite
     *
     * @param boolean $gravite
     * @return typeNiveau
     */
    public function setGravite($gravite)
    {
        $this->gravite = $gravite;

        return $this;
    }

    /**
     * Get gravite
     *
     * @return boolean 
     */
    public function getGravite()
    {
        return $this->gravite;
    }
}
