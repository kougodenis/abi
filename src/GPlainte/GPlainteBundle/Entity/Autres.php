<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Autres
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\AutresRepository")
 */
class Autres
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Plainte",mappedBy="autres")
     */
    private $plainte;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Denonciation",mappedBy="autres")
     */
    private $denonciation;
    
    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="libelle", type="string", length=200, nullable=true)
     */
    private $libelle;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return ServiceRendu
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->plainte = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add plainte
     *
     * @param \GPlainte\GPlainteBundle\Entity\Plainte $plainte
     * @return Autres
     */
    public function addPlainte(\GPlainte\GPlainteBundle\Entity\Plainte $plainte)
    {
        $this->plainte[] = $plainte;

        return $this;
    }

    /**
     * Remove plainte
     *
     * @param \GPlainte\GPlainteBundle\Entity\Plainte $plainte
     */
    public function removePlainte(\GPlainte\GPlainteBundle\Entity\Plainte $plainte)
    {
        $this->plainte->removeElement($plainte);
    }

    /**
     * Get plainte
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlainte()
    {
        return $this->plainte;
    }

    /**
     * Add denonciation
     *
     * @param \GPlainte\GPlainteBundle\Entity\Denonciation $denonciation
     * @return Autres
     */
    public function addDenonciation(\GPlainte\GPlainteBundle\Entity\Denonciation $denonciation)
    {
        $this->denonciation[] = $denonciation;

        return $this;
    }

    /**
     * Remove denonciation
     *
     * @param \GPlainte\GPlainteBundle\Entity\Denonciation $denonciation
     */
    public function removeDenonciation(\GPlainte\GPlainteBundle\Entity\Denonciation $denonciation)
    {
        $this->denonciation->removeElement($denonciation);
    }

    /**
     * Get denonciation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDenonciation()
    {
        return $this->denonciation;
    }
}
