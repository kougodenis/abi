<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Plainte
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\PlainteRepository")
 */
class Plainte
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Agent",inversedBy="plainte", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $agent;
    
    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\NormeIndicateur",inversedBy="plainte", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $indicateur;
    
    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Produit",inversedBy="plainte", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $produit;
    
    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Client",inversedBy="plainte", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=false)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Veuillez sélectionner le client")
     * 
     */
    private $client;
    
    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Niveau",inversedBy="plainte", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $niveau;
    
    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Service",inversedBy="plainte", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $service;
    
    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Autres",inversedBy="plainte", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $autres;
    
    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Bureau",inversedBy="plainte", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Veuillez selectionner le bureau")
     */
    private $bureau;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Choix",inversedBy="plainte", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=false)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Veuillez sélectionner la formulation de votre plainte")
     */
    private $choix;
    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Affecter",mappedBy="plainte")
     */
    private $affecter;
    
    /**
     * @var \DateTime
     *
     * @Doctrine\ORM\Mapping\Column(name="dateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @Doctrine\ORM\Mapping\Column(name="dateModification", type="datetime")
     */
    private $dateModification;
    

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="autreobjet", type="text", nullable=true)
     */
    private $autreobjet;
    
    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="telephone", type="text", nullable=true)
     * @Symfony\Component\Validator\Constraints\Type(type="numeric", message="La valeur n'est pas valide")
     * @Symfony\Component\Validator\Constraints\Length(
     *      min = 8,
     *      max = 10,
     *      exactMessage = "Le numéro de téléphone doit contenir au moins {{ limit }} chiffres")
     */
    private $telephone;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="numdossier", type="text", nullable=true)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Le numéro dossier ne doit pas être vide")
     */
    private $numdossier;

    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="affection", type="boolean", nullable=false)
     */
    private $affection;

    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="traitement", type="boolean", nullable=false)
     */
    private $traitement;

    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="mobile", type="boolean", nullable=false)
     */
    private $mobile;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="codeplaintemobile", type="text", nullable=true)
     */
    private $codeplaintemobile;
    
    /**
     * Get id
     *
     * @return integer 
     */

    public function getId()
    {
        return $this->id;
    }

    function __construct() {
        $this->dateCreation=new \DateTime();
        $this->dateModification=new \DateTime();
    }
    
    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     * @return Plainte
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime 
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModification
     *
     * @param \DateTime $dateModification
     * @return Plainte
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification
     *
     * @return \DateTime 
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * Set agent
     *
     * @param \GPlainte\GPlainteBundle\Entity\Agent $agent
     * @return Plainte
     */
    public function setAgent(\GPlainte\GPlainteBundle\Entity\Agent $agent)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \GPlainte\GPlainteBundle\Entity\Agent 
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set produit
     *
     * @param \GPlainte\GPlainteBundle\Entity\Produit $produit
     * @return Plainte
     */
    public function setProduit(\GPlainte\GPlainteBundle\Entity\Produit $produit)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \GPlainte\GPlainteBundle\Entity\Produit 
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set client
     *
     * @param \GPlainte\GPlainteBundle\Entity\Client $client
     * @return Plainte
     */
    public function setClient(\GPlainte\GPlainteBundle\Entity\Client $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \GPlainte\GPlainteBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set niveau
     *
     * @param \GPlainte\GPlainteBundle\Entity\Niveau $niveau
     * @return Plainte
     */
    public function setNiveau(\GPlainte\GPlainteBundle\Entity\Niveau $niveau)
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * Get niveau
     *
     * @return \GPlainte\GPlainteBundle\Entity\Niveau 
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    

    /**
     * Set description
     *
     * @param string $description
     * @return Plainte
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set service
     *
     * @param \GPlainte\GPlainteBundle\Entity\Service $service
     * @return Plainte
     */
    public function setService(\GPlainte\GPlainteBundle\Entity\Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \GPlainte\GPlainteBundle\Entity\Service 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set autres
     *
     * @param \GPlainte\GPlainteBundle\Entity\Autres $autres
     * @return Plainte
     */
    public function setAutres(\GPlainte\GPlainteBundle\Entity\Autres $autres = null)
    {
        $this->autres = $autres;

        return $this;
    }

    /**
     * Get autres
     *
     * @return \GPlainte\GPlainteBundle\Entity\Autres 
     */
    public function getAutres()
    {
        return $this->autres;
    }

    /**
     * Add affecter
     *
     * @param \GPlainte\GPlainteBundle\Entity\Affecter $affecter
     * @return Plainte
     */
    public function addAffecter(\GPlainte\GPlainteBundle\Entity\Affecter $affecter)
    {
        $this->affecter[] = $affecter;

        return $this;
    }

    /**
     * Remove affecter
     *
     * @param \GPlainte\GPlainteBundle\Entity\Affecter $affecter
     */
    public function removeAffecter(\GPlainte\GPlainteBundle\Entity\Affecter $affecter)
    {
        $this->affecter->removeElement($affecter);
    }

    /**
     * Get affecter
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAffecter()
    {
        return $this->affecter;
    }

    /**
     * Set affection
     *
     * @param boolean $affection
     * @return Plainte
     */
    public function setAffection($affection)
    {
        $this->affection = $affection;

        return $this;
    }

    /**
     * Get affection
     *
     * @return boolean 
     */
    public function getAffection()
    {
        return $this->affection;
    }

    /**
     * Set traitement
     *
     * @param boolean $traitement
     * @return Plainte
     */
    public function setTraitement($traitement)
    {
        $this->traitement = $traitement;

        return $this;
    }

    /**
     * Get traitement
     *
     * @return boolean 
     */
    public function getTraitement()
    {
        return $this->traitement;
    }

    /**
     * Set autreobjet
     *
     * @param string $autreobjet
     * @return Plainte
     */
    public function setAutreobjet($autreobjet)
    {
        $this->autreobjet = $autreobjet;

        return $this;
    }

    /**
     * Get autreobjet
     *
     * @return string 
     */
    public function getAutreobjet()
    {
        return $this->autreobjet;
    }

    /**
     * Set indicateur
     *
     * @param \GPlainte\GPlainteBundle\Entity\NormeIndicateur $indicateur
     * @return Plainte
     */
    public function setIndicateur(\GPlainte\GPlainteBundle\Entity\NormeIndicateur $indicateur = null)
    {
        $this->indicateur = $indicateur;

        return $this;
    }

    /**
     * Get indicateur
     *
     * @return \GPlainte\GPlainteBundle\Entity\NormeIndicateur 
     */
    public function getIndicateur()
    {
        return $this->indicateur;
    }

    /**
     * Set bureau
     *
     * @param \GPlainte\GPlainteBundle\Entity\Bureau $bureau
     * @return Plainte
     */
    public function setBureau(\GPlainte\GPlainteBundle\Entity\Bureau $bureau = null)
    {
        $this->bureau = $bureau;

        return $this;
    }

    /**
     * Get bureau
     *
     * @return \GPlainte\GPlainteBundle\Entity\Bureau 
     */
    public function getBureau()
    {
        return $this->bureau;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Plainte
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set numdossier
     *
     * @param string $numdossier
     * @return Plainte
     */
    public function setNumdossier($numdossier)
    {
        $this->numdossier = $numdossier;

        return $this;
    }

    /**
     * Get numdossier
     *
     * @return string 
     */
    public function getNumdossier()
    {
        return $this->numdossier;
    }

    /**
     * Set choix
     *
     * @param \GPlainte\GPlainteBundle\Entity\Choix $choix
     * @return Plainte
     */
    public function setChoix(\GPlainte\GPlainteBundle\Entity\Choix $choix)
    {
        $this->choix = $choix;

        return $this;
    }

    /**
     * Get choix
     *
     * @return \GPlainte\GPlainteBundle\Entity\Choix 
     */
    public function getChoix()
    {
        return $this->choix;
    }


    /**
     * Set mobile
     *
     * @param boolean $mobile
     * @return Plainte
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return boolean 
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set codeplaintemobile
     *
     * @param string $codeplaintemobile
     * @return Plainte
     */
    public function setCodeplaintemobile($codeplaintemobile)
    {
        $this->codeplaintemobile = $codeplaintemobile;

        return $this;
    }

    /**
     * Get codeplaintemobile
     *
     * @return string 
     */
    public function getCodeplaintemobile()
    {
        return $this->codeplaintemobile;
    }
}
