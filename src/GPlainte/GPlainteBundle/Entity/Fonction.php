<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fonction
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\FonctionRepository")
 */
class Fonction
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Agent",mappedBy="fonction")
     * 
     */
    private $agent;
    
    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="libelle", type="string", length=255)
     */
    private $libelle;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Fonction
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->agent = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add agent
     *
     * @param \GPlainte\GPlainteBundle\Entity\Agent $agent
     * @return Fonction
     */
    public function addAgent(\GPlainte\GPlainteBundle\Entity\Agent $agent)
    {
        $this->agent[] = $agent;

        return $this;
    }

    /**
     * Remove agent
     *
     * @param \GPlainte\GPlainteBundle\Entity\Agent $agent
     */
    public function removeAgent(\GPlainte\GPlainteBundle\Entity\Agent $agent)
    {
        $this->agent->removeElement($agent);
    }

    /**
     * Get agent
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAgent()
    {
        return $this->agent;
    }
}
