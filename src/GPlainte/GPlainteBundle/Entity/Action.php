<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Action
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\ActionRepository")
 */
class Action
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Affecter",mappedBy="action")
     */
    private $affecter;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Notification")
     * @Doctrine\ORM\Mapping\JoinColumn (nullable =true)
     */
    private $notifier;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="solution", type="text")
     */
    private $solution;
    
    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="prevention", type="text")
     */
    private $prevention;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="notification", type="boolean")
     */
    private $notification;

    /**
     * @var \DateTime
     *
     * @Doctrine\ORM\Mapping\Column(name="dateAction", type="datetime")
     */
    private $dateAction;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Action
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return Action
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }
    


    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->affecter = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set solution
     *
     * @param string $solution
     * @return Action
     */
    public function setSolution($solution)
    {
        $this->solution = $solution;

        return $this;
    }

    /**
     * Get solution
     *
     * @return string 
     */
    public function getSolution()
    {
        return $this->solution;
    }

    /**
     * Set prevention
     *
     * @param string $prevention
     * @return Action
     */
    public function setPrevention($prevention)
    {
        $this->prevention = $prevention;

        return $this;
    }

    /**
     * Get prevention
     *
     * @return string 
     */
    public function getPrevention()
    {
        return $this->prevention;
    }

    /**
     * Add affecter
     *
     * @param \GPlainte\GPlainteBundle\Entity\Affecter $affecter
     * @return Action
     */
    public function addAffecter(\GPlainte\GPlainteBundle\Entity\Affecter $affecter)
    {
        $this->affecter[] = $affecter;

        return $this;
    }

    /**
     * Remove affecter
     *
     * @param \GPlainte\GPlainteBundle\Entity\Affecter $affecter
     */
    public function removeAffecter(\GPlainte\GPlainteBundle\Entity\Affecter $affecter)
    {
        $this->affecter->removeElement($affecter);
    }

    /**
     * Get affecter
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAffecter()
    {
        return $this->affecter;
    }

    /**
     * Set dateAction
     *
     * @param \DateTime $dateAction
     * @return Action
     */
    public function setDateAction($dateAction)
    {
        $this->dateAction = $dateAction;

        return $this;
    }

    /**
     * Get dateAction
     *
     * @return \DateTime 
     */
    public function getDateAction()
    {
        return $this->dateAction;
    }


    /**
     * Set notification
     *
     * @param boolean $notification
     * @return Action
     */
    public function setNotification($notification)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Get notification
     *
     * @return boolean 
     */
    public function getNotification()
    {
        return $this->notification;
    }



    /**
     * Set notifier
     *
     * @param \GPlainte\GPlainteBundle\Entity\Notification $notifier
     * @return Action
     */
    public function setNotifier(\GPlainte\GPlainteBundle\Entity\Notification $notifier = null)
    {
        $this->notifier = $notifier;

        return $this;
    }

    /**
     * Get notifier
     *
     * @return \GPlainte\GPlainteBundle\Entity\Notification 
     */
    public function getNotifier()
    {
        return $this->notifier;
    }
}
