<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * Agent
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\AgentRepository")
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
class Agent
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Service",inversedBy="agent", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $service;
    
    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Fonction",inversedBy="agent", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=false)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Le libelle ne doit pas être vide")
     */
    private $fonction;
    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Plainte",mappedBy="agent")
     */
    private $plainte;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Denonciation",mappedBy="agent")
     */
    private $denonciation;
    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\UserBundle\Entity\User",mappedBy="agent")
     */
    private $user;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Notification",mappedBy="agent")
     */
    private $notification;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Affecter",mappedBy="agent")
     * 
     */
    private $affecter;
    
    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="nom", type="string", length=100)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Le champ nom ne doit pas être vide")
     */
    private $nom;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="prenom", type="string", length=200)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Le champ prénom ne doit pas être vide")
     */
    private $prenom;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="sexe", type="string", length=1)
     */
    private $sexe;


    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="telephone", type="string", length=50, nullable=false)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Le numéro ne doit pas être vide")
     * @Symfony\Component\Validator\Constraints\Type(type="numeric", message="Le numéro téléphone n'est pas valide")
     * @Symfony\Component\Validator\Constraints\Length(
     *      min = 8,
     *      max = 10,
     *      minMessage = "Le numéro de téléphone doit contenir au moins {{ limit }} chiffres")
     */
    private $telephone;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="email", type="string", length=200, nullable=false)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="L'email ne doit pas être vide")
     * @Symfony\Component\Validator\Constraints\Email(message="L'email '{{ value }}' n'est pas valide.")
     */
    private $email;

    private $nomprenom;

    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="chef", type="boolean")
     */
    private $chef;

    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="dg", type="boolean")
     */
    private $dg;

    /**
     * @Doctrine\ORM\Mapping\Column(type="string", length=255, nullable=true)
     * @Symfony\Component\Validator\Constraints\Blank
     */
    public $name;
    /**
     * @Doctrine\ORM\Mapping\Column(type="string", length=255, nullable=true)
     */
    public $path;
    /**
     * @Symfony\Component\Validator\Constraints\File(maxSize="6000000")
     */
    public $file;


    private $tempFilename ;

    public function setFile ( UploadedFile $file )
 {
   $this->file = $file ;

        // On vé rifie si on avait déjà un fichier pour cette entit é
    if ( null !== $this->path) {
        // On sauvegarde l'extension du fichier pour le supprimerplus tard
    $this->tempFilename = $this->path;

        // On ré initialise les valeurs des attributs url et alt
    $this->path = null ;
    }
    }


    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path;
    }
    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir().'/'.$this->path;
    }
    protected function getUploadRootDir()
    {
        // le chemin absolu du répertoire où les documents uploadés doivent être sauvegardés
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    protected function getUploadDir()
    {
        return 'uploads/photo';
    }


    /**
     * @Doctrine\ORM\Mapping\PrePersist()
     * @Doctrine\ORM\Mapping\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            // faites ce que vous voulez pour générer un nom unique
            $this->path = sha1(uniqid(mt_rand(), true)).'.'.$this->file->guessExtension();
        }
    }

    /**
     * @Doctrine\ORM\Mapping\PostPersist()
     * @Doctrine\ORM\Mapping\PostUpdate()
     */
    public function upload()
    {
        // la propriété « file » peut être vide si le champ n'est pas requis
        if (null === $this->file) {
            return;
        }

        $this->file->move($this->getUploadRootDir(), $this->path);
        unset($this->file);
    }

    /**
     * @Doctrine\ORM\Mapping\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Agent
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Agent
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     * @return Agent
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string 
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Agent
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Agent
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->plainte = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set service
     *
     * @param \GPlainte\GPlainteBundle\Entity\Service $service
     * @return Agent
     */
    public function setService(\GPlainte\GPlainteBundle\Entity\Service $service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \GPlainte\GPlainteBundle\Entity\Service 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Add plainte
     *
     * @param \GPlainte\GPlainteBundle\Entity\Plainte $plainte
     * @return Agent
     */
    public function addPlainte(\GPlainte\GPlainteBundle\Entity\Plainte $plainte)
    {
        $this->plainte[] = $plainte;

        return $this;
    }

    /**
     * Remove plainte
     *
     * @param \GPlainte\GPlainteBundle\Entity\Plainte $plainte
     */
    public function removePlainte(\GPlainte\GPlainteBundle\Entity\Plainte $plainte)
    {
        $this->plainte->removeElement($plainte);
    }

    /**
     * Get plainte
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlainte()
    {
        return $this->plainte;
    }

   

    /**
     * Add affecter
     *
     * @param \GPlainte\GPlainteBundle\Entity\Affecter $affecter
     * @return Agent
     */
    public function addAffecter(\GPlainte\GPlainteBundle\Entity\Affecter $affecter)
    {
        $this->affecter[] = $affecter;

        return $this;
    }

    /**
     * Remove affecter
     *
     * @param \GPlainte\GPlainteBundle\Entity\Affecter $affecter
     */
    public function removeAffecter(\GPlainte\GPlainteBundle\Entity\Affecter $affecter)
    {
        $this->affecter->removeElement($affecter);
    }

    /**
     * Get affecter
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAffecter()
    {
        return $this->affecter;
    }

    /**
     * @return mixed
     */
    public function getNomprenom()
    {
        return $this->nom.' '.$this->prenom;
    }

    /**
     * @param mixed $nomprenom
     */
    public function setNomprenom($nomprenom)
    {
        $this->nomprenom = $nomprenom;
    }



    /**
     * Add user
     *
     * @param \GPlainte\UserBundle\Entity\User $user
     * @return Agent
     */
    public function addUser(\GPlainte\UserBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \GPlainte\UserBundle\Entity\User $user
     */
    public function removeUser(\GPlainte\UserBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add notification
     *
     * @param \GPlainte\GPlainteBundle\Entity\Notification $notification
     * @return Agent
     */
    public function addNotification(\GPlainte\GPlainteBundle\Entity\Notification $notification)
    {
        $this->notification[] = $notification;

        return $this;
    }

    /**
     * Remove notification
     *
     * @param \GPlainte\GPlainteBundle\Entity\Notification $notification
     */
    public function removeNotification(\GPlainte\GPlainteBundle\Entity\Notification $notification)
    {
        $this->notification->removeElement($notification);
    }

    /**
     * Get notification
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Set fonction
     *
     * @param \GPlainte\GPlainteBundle\Entity\Fonction $fonction
     * @return Agent
     */
    public function setFonction(\GPlainte\GPlainteBundle\Entity\Fonction $fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction
     *
     * @return \GPlainte\GPlainteBundle\Entity\Fonction 
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Agent
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Agent
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Add denonciation
     *
     * @param \GPlainte\GPlainteBundle\Entity\Denonciation $denonciation
     * @return Agent
     */
    public function addDenonciation(\GPlainte\GPlainteBundle\Entity\Denonciation $denonciation)
    {
        $this->denonciation[] = $denonciation;

        return $this;
    }

    /**
     * Remove denonciation
     *
     * @param \GPlainte\GPlainteBundle\Entity\Denonciation $denonciation
     */
    public function removeDenonciation(\GPlainte\GPlainteBundle\Entity\Denonciation $denonciation)
    {
        $this->denonciation->removeElement($denonciation);
    }

    /**
     * Get denonciation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDenonciation()
    {
        return $this->denonciation;
    }

    /**
     * Set chef
     *
     * @param boolean $chef
     * @return Agent
     */
    public function setChef($chef)
    {
        $this->chef = $chef;

        return $this;
    }

    /**
     * Get chef
     *
     * @return boolean 
     */
    public function getChef()
    {
        return $this->chef;
    }

    /**
     * Set dg
     *
     * @param boolean $dg
     * @return Agent
     */
    public function setDg($dg)
    {
        $this->dg = $dg;

        return $this;
    }

    /**
     * Get dg
     *
     * @return boolean 
     */
    public function getDg()
    {
        return $this->dg;
    }
}
