<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Approuver
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\ApprouverRepository")
 */
class Approuver
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Affecter",inversedBy="approuver", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $affecter;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="nom", type="string", length=200)
     */
    private $nom;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="prenom", type="string", length=200)
     */
    private $prenom;

    /**
     * @var \DateTime
     *
     * @Doctrine\ORM\Mapping\Column(name="dateApprobation", type="datetime")
     */
    private $dateApprobation;

    public function __construct(){
        $this->dateApprobation=new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Approuver
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Approuver
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set affecter
     *
     * @param \GPlainte\GPlainteBundle\Entity\Affecter $affecter
     * @return Approuver
     */
    public function setAffecter(\GPlainte\GPlainteBundle\Entity\Affecter $affecter = null)
    {
        $this->affecter = $affecter;

        return $this;
    }

    /**
     * Get affecter
     *
     * @return \GPlainte\GPlainteBundle\Entity\Affecter 
     */
    public function getAffecter()
    {
        return $this->affecter;
    }

    /**
     * Set dateApprobation
     *
     * @param \DateTime $dateApprobation
     * @return Approuver
     */
    public function setDateApprobation($dateApprobation)
    {
        $this->dateApprobation = $dateApprobation;

        return $this;
    }

    /**
     * Get dateApprobation
     *
     * @return \DateTime 
     */
    public function getDateApprobation()
    {
        return $this->dateApprobation;
    }
}
