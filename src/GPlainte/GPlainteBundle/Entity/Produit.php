<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Produit
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\ProduitRepository")
 */
class Produit
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\CategorieProduit",inversedBy="produit", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $categorieproduit;
    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Plainte",mappedBy="produit")
     */
    private $plainte;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Denonciation",mappedBy="produit")
     */
    private $denonciation;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="libelle", type="string", length=200)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Précisez le libelle du produit")
     */
    private $libelle;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="description", type="text")
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Précisez la description du produit")
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Produit
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Produit
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->plainte = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add plainte
     *
     * @param \GPlainte\GPlainteBundle\Entity\Plainte $plainte
     * @return Produit
     */
    public function addPlainte(\GPlainte\GPlainteBundle\Entity\Plainte $plainte)
    {
        $this->plainte[] = $plainte;

        return $this;
    }

    /**
     * Remove plainte
     *
     * @param \GPlainte\GPlainteBundle\Entity\Plainte $plainte
     */
    public function removePlainte(\GPlainte\GPlainteBundle\Entity\Plainte $plainte)
    {
        $this->plainte->removeElement($plainte);
    }

    /**
     * Get plainte
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlainte()
    {
        return $this->plainte;
    }

    /**
     * Set categorieproduit
     *
     * @param \GPlainte\GPlainteBundle\Entity\CategorieProduit $categorieproduit
     * @return Produit
     */
    public function setCategorieproduit(\GPlainte\GPlainteBundle\Entity\CategorieProduit $categorieproduit = null)
    {
        $this->categorieproduit = $categorieproduit;

        return $this;
    }

    /**
     * Get categorieproduit
     *
     * @return \GPlainte\GPlainteBundle\Entity\CategorieProduit 
     */
    public function getCategorieproduit()
    {
        return $this->categorieproduit;
    }

    /**
     * Add denonciation
     *
     * @param \GPlainte\GPlainteBundle\Entity\Denonciation $denonciation
     * @return Produit
     */
    public function addDenonciation(\GPlainte\GPlainteBundle\Entity\Denonciation $denonciation)
    {
        $this->denonciation[] = $denonciation;

        return $this;
    }

    /**
     * Remove denonciation
     *
     * @param \GPlainte\GPlainteBundle\Entity\Denonciation $denonciation
     */
    public function removeDenonciation(\GPlainte\GPlainteBundle\Entity\Denonciation $denonciation)
    {
        $this->denonciation->removeElement($denonciation);
    }

    /**
     * Get denonciation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDenonciation()
    {
        return $this->denonciation;
    }
}
