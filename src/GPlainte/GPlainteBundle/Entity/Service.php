<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Service
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\ServiceRepository")
 */
class Service
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Institution",inversedBy="service", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=false)
     */
    private $institution;
   
    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Agent",mappedBy="service")
     */
    private $agent;
    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Plainte",mappedBy="service")
     */
    private $plainte;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Denonciation",mappedBy="service")
     */
    private $denonciation;
    
    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="libelle", type="string", length=200)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Précisez le nom du service")
     */
    private $libelle;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Service
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produit = new \Doctrine\Common\Collections\ArrayCollection();
        $this->agent = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set institution
     *
     * @param \GPlainte\GPlainteBundle\Entity\Institution $institution
     * @return Service
     */
    public function setInstitution(\GPlainte\GPlainteBundle\Entity\Institution $institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get institution
     *
     * @return \GPlainte\GPlainteBundle\Entity\Institution 
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Add agent
     *
     * @param \GPlainte\GPlainteBundle\Entity\Agent $agent
     * @return Service
     */
    public function addAgent(\GPlainte\GPlainteBundle\Entity\Agent $agent)
    {
        $this->agent[] = $agent;

        return $this;
    }

    /**
     * Remove agent
     *
     * @param \GPlainte\GPlainteBundle\Entity\Agent $agent
     */
    public function removeAgent(\GPlainte\GPlainteBundle\Entity\Agent $agent)
    {
        $this->agent->removeElement($agent);
    }

    /**
     * Get agent
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Add plainte
     *
     * @param \GPlainte\GPlainteBundle\Entity\Plainte $plainte
     * @return Service
     */
    public function addPlainte(\GPlainte\GPlainteBundle\Entity\Plainte $plainte)
    {
        $this->plainte[] = $plainte;

        return $this;
    }

    /**
     * Remove plainte
     *
     * @param \GPlainte\GPlainteBundle\Entity\Plainte $plainte
     */
    public function removePlainte(\GPlainte\GPlainteBundle\Entity\Plainte $plainte)
    {
        $this->plainte->removeElement($plainte);
    }

    /**
     * Get plainte
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlainte()
    {
        return $this->plainte;
    }

    /**
     * Add denonciation
     *
     * @param \GPlainte\GPlainteBundle\Entity\Denonciation $denonciation
     * @return Service
     */
    public function addDenonciation(\GPlainte\GPlainteBundle\Entity\Denonciation $denonciation)
    {
        $this->denonciation[] = $denonciation;

        return $this;
    }

    /**
     * Remove denonciation
     *
     * @param \GPlainte\GPlainteBundle\Entity\Denonciation $denonciation
     */
    public function removeDenonciation(\GPlainte\GPlainteBundle\Entity\Denonciation $denonciation)
    {
        $this->denonciation->removeElement($denonciation);
    }

    /**
     * Get denonciation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDenonciation()
    {
        return $this->denonciation;
    }
}
