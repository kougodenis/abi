<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Client
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\ClientRepository")
 * @Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity(
 *     fields={"numDossier","telephone1"},
 *     errorPath="numDossier",
 *     message="This port is already in use on that host."
 * )
 */
class Client
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Plainte",mappedBy="client")
     */
    private $plainte;
    

    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Satisfaction")
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $satisfaction;
    
    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="nom", type="string", length=200)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Le nom ne doit pas être vide")
     * @Symfony\Component\Validator\Constraints\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Le nom ne peut pas contenir un chiffre",
     *     htmlPattern=false
     * )
     */
    private $nom;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="prenom", type="string", length=200)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Le prénom ne doit pas être vide")
     * @Symfony\Component\Validator\Constraints\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Le prénom ne peut pas contenir un chiffre",
     *     htmlPattern=false)
     */
    private $prenom;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="raisonSociale", type="string", length=200, nullable=true)
     */
    private $raisonSociale;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="email", type="string", length=50, nullable=true)
     * @Symfony\Component\Validator\Constraints\Email(message = "L'email '{{ value }}' n'est pas valide.")
     */
    private $email;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="numDossier", type="string", length=200)
     */
    private $numDossier;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="quartier", type="string", length=200)
     */
    private $quartier;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="telephone1", type="string", length=20)
     * @Symfony\Component\Validator\Constraints\Type(type="numeric", message="Le numéro téléphone n'est pas valide")
     * @Symfony\Component\Validator\Constraints\Length(
     *      min = 8,
     *      max = 10,
     *      minMessage = "Le numéro de téléphone doit contenir au moins {{ limit }} chiffres")
     */
    private $telephone1;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="ville", type="string", length=200)
     */
    private $ville;


    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="telephone2", type="string", length=20, nullable=true)
     */
    private $telephone2;

    /**
     * @var \DateTime
     *
     * @Doctrine\ORM\Mapping\Column(name="dateCreation", type="datetime")
     */
    private $dateCreation;


    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="sexe", type="string", length=20)
     */
    private $sexe;

    private $nomprenom;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

   
    
    /**
     * Set nom
     *
     * @param string $nom
     * @return Client
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Client
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set raisonSociale
     *
     * @param string $raisonSociale
     * @return Client
     */
    public function setRaisonSociale($raisonSociale)
    {
        $this->raisonSociale = $raisonSociale;

        return $this;
    }

    /**
     * Get raisonSociale
     *
     * @return string 
     */
    public function getRaisonSociale()
    {
        return $this->raisonSociale;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set numDossier
     *
     * @param string $numDossier
     * @return Client
     */
    public function setNumDossier($numDossier)
    {
        $this->numDossier = $numDossier;

        return $this;
    }

    /**
     * Get numDossier
     *
     * @return string 
     */
    public function getNumDossier()
    {
        return $this->numDossier;
    }

    /**
     * Set quartier
     *
     * @param string $quartier
     * @return Client
     */
    public function setQuartier($quartier)
    {
        $this->quartier = $quartier;

        return $this;
    }

    /**
     * Get quartier
     *
     * @return string 
     */
    public function getQuartier()
    {
        return $this->quartier;
    }

    /**
     * Set telephone1
     *
     * @param string $telephone1
     * @return Client
     */
    public function setTelephone1($telephone1)
    {
        $this->telephone1 = $telephone1;

        return $this;
    }

    /**
     * Get telephone1
     *
     * @return string 
     */
    public function getTelephone1()
    {
        return $this->telephone1;
    }

    /**
     * Set telephone2
     *
     * @param string $telephone2
     * @return Client
     */
    public function setTelephone2($telephone2)
    {
        $this->telephone2 = $telephone2;

        return $this;
    }

    /**
     * Get telephone2
     *
     * @return string 
     */
    public function getTelephone2()
    {
        return $this->telephone2;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dateCreation=new \DateTime();
        $this->plainte = new \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * Add plainte
     *
     * @param \GPlainte\GPlainteBundle\Entity\Plainte $plainte
     * @return Client
     */
    public function addPlainte(\GPlainte\GPlainteBundle\Entity\Plainte $plainte)
    {
        $this->plainte[] = $plainte;

        return $this;
    }

    /**
     * Remove plainte
     *
     * @param \GPlainte\GPlainteBundle\Entity\Plainte $plainte
     */
    public function removePlainte(\GPlainte\GPlainteBundle\Entity\Plainte $plainte)
    {
        $this->plainte->removeElement($plainte);
    }

    /**
     * Get plainte
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlainte()
    {
        return $this->plainte;
    }


    /**
     * Set satisfaction
     *
     * @param \GPlainte\GPlainteBundle\Entity\Satisfaction $satisfaction
     * @return Client
     */
    public function setSatisfaction(\GPlainte\GPlainteBundle\Entity\Satisfaction $satisfaction = null)
    {
        $this->satisfaction = $satisfaction;

        return $this;
    }

    /**
     * Get satisfaction
     *
     * @return \GPlainte\GPlainteBundle\Entity\Satisfaction 
     */
    public function getSatisfaction()
    {
        return $this->satisfaction;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     * @return Client
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime 
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }
    
    /**
     * @return mixed
     */
    public function getNomprenom()
    {
        return $this->nom.' '.$this->prenom;
    }

    /**
     * @param mixed $nomprenom
     */
    public function setNomprenom($nomprenom)
    {
        $this->nomprenom = $nomprenom;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Client
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     * @return Client
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string 
     */
    public function getSexe()
    {
        return $this->sexe;
    }
}
