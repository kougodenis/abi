<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlainteMobile
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\PlainteMobileRepository")
 */
class PlainteMobile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idplainte", type="integer")
     */
    private $idplainte;

    /**
     * @var string
     *
     * @ORM\Column(name="nomcomplet", type="string", length=200, nullable=true)
     */
    private $nomcomplet;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=30, nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="raisonsociale", type="string", length=200, nullable=true)
     */
    private $raisonsociale;

    /**
     * @var string
     *
     * @ORM\Column(name="institution", type="string", length=100)
     */
    private $institution;

    /**
     * @var string
     *
     * @ORM\Column(name="uid", type="string", length=200, nullable=true)
     */
    private $uid;

    /**
     * @var string
     *
     * @ORM\Column(name="nomfichier", type="string", length=200, nullable=true)
     */
    private $nomfichier;

    /**
     * @var string
     *
     * @ORM\Column(name="formatfichier", type="string", length=20, nullable=true)
     */
    private $formatfichier;

    /**
     * @var boolean
     *
     * @ORM\Column(name="type", type="boolean")
     */
    private $type;


    /**
     * @var boolean
     *
     * @ORM\Column(name="validation", type="boolean")
     */
    private $validation;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomcomplet
     *
     * @param string $nomcomplet
     * @return PlainteMobile
     */
    public function setNomcomplet($nomcomplet)
    {
        $this->nomcomplet = $nomcomplet;

        return $this;
    }

    /**
     * Get nomcomplet
     *
     * @return string 
     */
    public function getNomcomplet()
    {
        return $this->nomcomplet;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return PlainteMobile
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return PlainteMobile
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set raisonsociale
     *
     * @param string $raisonsociale
     * @return PlainteMobile
     */
    public function setRaisonsociale($raisonsociale)
    {
        $this->raisonsociale = $raisonsociale;

        return $this;
    }

    /**
     * Get raisonsociale
     *
     * @return string 
     */
    public function getRaisonsociale()
    {
        return $this->raisonsociale;
    }

    /**
     * Set institution
     *
     * @param string $institution
     * @return PlainteMobile
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get institution
     *
     * @return string 
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set uid
     *
     * @param string $uid
     * @return PlainteMobile
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return string 
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set nomfichier
     *
     * @param string $nomfichier
     * @return PlainteMobile
     */
    public function setNomfichier($nomfichier)
    {
        $this->nomfichier = $nomfichier;

        return $this;
    }

    /**
     * Get nomfichier
     *
     * @return string 
     */
    public function getNomfichier()
    {
        return $this->nomfichier;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return PlainteMobile
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return boolean 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set idplainte
     *
     * @param integer $idplainte
     * @return PlainteMobile
     */
    public function setIdplainte($idplainte)
    {
        $this->idplainte = $idplainte;

        return $this;
    }

    /**
     * Get idplainte
     *
     * @return integer 
     */
    public function getIdplainte()
    {
        return $this->idplainte;
    }

    /**
     * Set formatfichier
     *
     * @param string $formatfichier
     * @return PlainteMobile
     */
    public function setFormatfichier($formatfichier)
    {
        $this->formatfichier = $formatfichier;

        return $this;
    }

    /**
     * Get formatfichier
     *
     * @return string 
     */
    public function getFormatfichier()
    {
        return $this->formatfichier;
    }

    /**
     * Set validation
     *
     * @param boolean $validation
     * @return PlainteMobile
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * Get validation
     *
     * @return boolean 
     */
    public function getValidation()
    {
        return $this->validation;
    }
}
