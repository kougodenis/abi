<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * NormeIndicateur
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\NormeIndicateurRepository")
 */
class NormeIndicateur
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Plainte",mappedBy="indicateur")
     */
    private $plainte;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\GPlainteBundle\Entity\Denonciation",mappedBy="indicateur")
     */
    private $denonciation;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToOne(targetEntity="GPlainte\GPlainteBundle\Entity\ObjectifIndicateur")
     */
    private $objectifindicateur;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="libelle", type="string", length=255)
     * @Symfony\Component\Validator\Constraints\NotBlank(message="Entrez le nom de la norme")
     */
    private $libelle;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="normeVal", type="string", length=200)
     * @Symfony\Component\Validator\Constraints\Type(type="integer", message="Entrez un entier")
     */
    private $normeVal;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="normeUnite", type="string", length=2, nullable=true)
     */
    private $normeUnite;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="normeOp", type="string", length=2, nullable=true)
     */
    private $normeOp;

    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="valide", type="boolean", nullable=true)
     */
    private $valide;

    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="jour", type="integer")
     * @Symfony\Component\Validator\Constraints\Type(type="integer", message="Entrez un entier")
     */
    private $jour;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="marqueur", type="string", length=50, nullable=true)
     */
    private $marqueur;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return NormeIndicateur
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }


    /**
     * Set jour
     *
     * @param integer $jour
     * @return NormeIndicateur
     */
    public function setJour($jour)
    {
        $this->jour = $jour;

        return $this;
    }

    /**
     * Get jour
     *
     * @return integer 
     */
    public function getJour()
    {
        return $this->jour;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->plainte = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add plainte
     *
     * @param \GPlainte\GPlainteBundle\Entity\Plainte $plainte
     * @return NormeIndicateur
     */
    public function addPlainte(\GPlainte\GPlainteBundle\Entity\Plainte $plainte)
    {
        $this->plainte[] = $plainte;

        return $this;
    }

    /**
     * Remove plainte
     *
     * @param \GPlainte\GPlainteBundle\Entity\Plainte $plainte
     */
    public function removePlainte(\GPlainte\GPlainteBundle\Entity\Plainte $plainte)
    {
        $this->plainte->removeElement($plainte);
    }

    /**
     * Get plainte
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlainte()
    {
        return $this->plainte;
    }

    /**
     * Set objectifindicateur
     *
     * @param \GPlainte\GPlainteBundle\Entity\ObjectifIndicateur $objectifindicateur
     * @return NormeIndicateur
     */
    public function setObjectifindicateur(\GPlainte\GPlainteBundle\Entity\ObjectifIndicateur $objectifindicateur = null)
    {
        $this->objectifindicateur = $objectifindicateur;

        return $this;
    }

    /**
     * Get objectifindicateur
     *
     * @return \GPlainte\GPlainteBundle\Entity\ObjectifIndicateur
     */
    public function getObjectifindicateur()
    {
        return $this->objectifindicateur;
    }

    /**
     * Set normeVal
     *
     * @param string $normeVal
     * @return NormeIndicateur
     */
    public function setNormeVal($normeVal)
    {
        $this->normeVal = $normeVal;

        return $this;
    }

    /**
     * Get normeVal
     *
     * @return string
     */
    public function getNormeVal()
    {
        return $this->normeVal;
    }

    /**
     * Set normeUnite
     *
     * @param string $normeUnite
     * @return NormeIndicateur
     */
    public function setNormeUnite($normeUnite)
    {
        $this->normeUnite = $normeUnite;

        return $this;
    }

    /**
     * Get normeUnite
     *
     * @return string
     */
    public function getNormeUnite()
    {
        return $this->normeUnite;
    }

    /**
     * Set normeOp
     *
     * @param string $normeOp
     * @return NormeIndicateur
     */
    public function setNormeOp($normeOp)
    {
        $this->normeOp = $normeOp;

        return $this;
    }

    /**
     * Get normeOp
     *
     * @return string
     */
    public function getNormeOp()
    {
        return $this->normeOp;
    }

    /**
     * Set valide
     *
     * @param boolean $valide
     * @return NormeIndicateur
     */
    public function setValide($valide)
    {
        $this->valide = $valide;

        return $this;
    }

    /**
     * Get valide
     *
     * @return boolean
     */
    public function getValide()
    {
        return $this->valide;
    }

    /**
     * Add denonciation
     *
     * @param \GPlainte\GPlainteBundle\Entity\Denonciation $denonciation
     * @return NormeIndicateur
     */
    public function addDenonciation(\GPlainte\GPlainteBundle\Entity\Denonciation $denonciation)
    {
        $this->denonciation[] = $denonciation;

        return $this;
    }

    /**
     * Remove denonciation
     *
     * @param \GPlainte\GPlainteBundle\Entity\Denonciation $denonciation
     */
    public function removeDenonciation(\GPlainte\GPlainteBundle\Entity\Denonciation $denonciation)
    {
        $this->denonciation->removeElement($denonciation);
    }

    /**
     * Get denonciation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDenonciation()
    {
        return $this->denonciation;
    }

    /**
     * Set marqueur
     *
     * @param string $marqueur
     * @return NormeIndicateur
     */
    public function setMarqueur($marqueur)
    {
        $this->marqueur = $marqueur;

        return $this;
    }

    /**
     * Get marqueur
     *
     * @return string 
     */
    public function getMarqueur()
    {
        return $this->marqueur;
    }
}
