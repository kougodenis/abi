<?php

namespace GPlainte\GPlainteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @Doctrine\ORM\Mapping\Table()
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\GPlainteBundle\Entity\NotificationRepository")
 */
class Notification
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Agent",inversedBy="notification", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $agent;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Satisfaction")
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $satisfaction;

    /**
     * @var \DateTime
     *
     * @Doctrine\ORM\Mapping\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="moyencommunication", type="string")
     */
    private $moyencommunication;

//    public function __construct(){
//        $this->date=new \DateTime();
//    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Notification
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }



    /**
     * Set agent
     *
     * @param \GPlainte\GPlainteBundle\Entity\Agent $agent
     * @return Notification
     */
    public function setAgent(\GPlainte\GPlainteBundle\Entity\Agent $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \GPlainte\GPlainteBundle\Entity\Agent 
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set moyencommunication
     *
     * @param string $moyencommunication
     * @return Notification
     */
    public function setMoyencommunication($moyencommunication)
    {
        $this->moyencommunication = $moyencommunication;

        return $this;
    }

    /**
     * Get moyencommunication
     *
     * @return string 
     */
    public function getMoyencommunication()
    {
        return $this->moyencommunication;
    }

    /**
     * Set satisfaction
     *
     * @param \GPlainte\GPlainteBundle\Entity\Satisfaction $satisfaction
     * @return Notification
     */
    public function setSatisfaction(\GPlainte\GPlainteBundle\Entity\Satisfaction $satisfaction = null)
    {
        $this->satisfaction = $satisfaction;

        return $this;
    }

    /**
     * Get satisfaction
     *
     * @return \GPlainte\GPlainteBundle\Entity\Satisfaction 
     */
    public function getSatisfaction()
    {
        return $this->satisfaction;
    }
}
