<?php
/**
 * Created by PhpStorm.
 * User: Maurice
 * Date: 13/10/2015
 * Time: 17:12
 */
namespace GPlainte\GPlainteBundle\Dql;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;

/**
 * "MEDIAN" "(" SimpleArithmeticExpression ")". Modified from DoctrineExtensions\Query\Mysql\Year
 *
 * @category DoctrineExtensions
 * @package DoctrineExtensions\Query\Mysql
 * @author Rafael Kassner <kassner@gmail.com>
 * @author Sarjono Mukti Aji <me@simukti.net>
 * @license MIT License
 */
class Getdate extends FunctionNode
{
    public $getdate;

    /**
     * @override
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return "GETDATE(" . $sqlWalker->walkArithmeticPrimary($this->getdate) . ")";
    }

    /**
     * @override
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $this->getdate = $parser->ArithmeticPrimary();

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}