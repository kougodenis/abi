<?php

namespace GPlainte\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends BaseController
{
        public function registerAction(Request $request)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        //var_dump($id); die();
        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
            $data=$request->request->get($form->getName()); 
            $rol = array($data['rol']);
//            $dateExpiration = $data['dateExpiration'];
            $user->setRoles($rol);
//            $dateExp=new \DateTime($dateExpiration);
//           
//            $user->setExpiresAt($dateExp);
            $userManager->updateUser($user);
            return $this->redirect($this->generateUrl('fos_user_registration_register',array('msg'=>1)));

//            if (null === $response = $event->getResponse()) {
//                $url = $this->generateUrl('fos_user_register');
//                $response = new RedirectResponse($url);
//            }
//
//            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
//
//            return $response;
        }
        $em=$this->getDoctrine()->getManager();
        $users = $em->getRepository('UserBundle:User')->findAll();
        return $this->render('UserBundle:Registration:register.html.twig', array(
            'form' => $form->createView(),
            'list' => $users
        ));
    }


    public function modifUserAction($id)
    {
//        var_dump($id); die();
        $request=$this->getRequest();

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager -> findUserBy ( array ('id' => $id));
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        //var_dump($id); die();
//        $user = $userManager->createUser();
//        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
            $data=$request->request->get($form->getName());
            $rol = array($data['rol']);
            $user->setRoles($rol);
//            $dateExpiration = $data['dateExpiration'];
//            $dateExp=new \DateTime($dateExpiration);
//            $user->setExpiresAt($dateExp);
            $userManager->updateUser($user);

////            if (null === $response = $event->getResponse()) {
////                $url = $this->generateUrl('fos_user_registration_register');
////                $response = new RedirectResponse($url);
////            }
//
//            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
//
//            return $response;
            return $this->redirect($this->generateUrl('fos_user_registration_register',array('msg'=>2)));
        }
        $em=$this->getDoctrine()->getManager();
        $users = $em->getRepository('UserBundle:User')->findAll();
        return $this->render('UserBundle:Registration:register.html.twig', array(
            'form' => $form->createView(),
            'list' => $users,
            'id'=>$id
        ));
    }


}