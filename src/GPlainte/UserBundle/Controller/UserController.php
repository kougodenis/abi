<?php

namespace GPlainte\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    public function showAction()
    {
        $em=$this->getDoctrine()->getManager();
        $users = $em->getRepository('UserBundle:User')->findAll();
       // var_dump($users); die();
        return $this->render('UserBundle:Profile:listUsers.html.twig', array('list' => $users));
    }

    public function desactiverAction($id){
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager -> findUserBy ( array ('id' => $id));
        $user->setEnabled(0);
        $userManager -> updateUser ( $user );

        return $this->redirect($this->generateUrl('fos_user_registration_register'));
    }

    public function activerAction($id){
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager -> findUserBy ( array ('id' => $id));
        $user->setEnabled(1);
        $userManager -> updateUser ( $user );

        return $this->redirect($this->generateUrl('fos_user_registration_register'));
    }
    public function showJournalAction()
    {
        $em=$this->getDoctrine()->getManager();
        $logins = $em->getRepository('UserBundle:Login')->findAll();
        // var_dump($users); die();
        return $this->render('UserBundle:Security:journal.html.twig', array('list' => $logins));
    }
}
