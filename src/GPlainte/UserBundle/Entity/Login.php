<?php

namespace GPlainte\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Login
 *
 * @Doctrine\ORM\Mapping\Table(name="logins")
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\UserBundle\Entity\LoginRepository")
 */
class Login
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="ipaddress", type="string", length=24)
     */
    private $ipaddress;

    /**
     * @var \DateTime
     *
     * @Doctrine\ORM\Mapping\Column(name="createdat", type="datetime")
     */
    private $createdat;
    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\UserBundle\Entity\User")
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\UserBundle\Entity\DataEvent",inversedBy="login", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $dataevent;




//    public function __construct(){
//        $this->activite="Authentification";
//    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ipaddress
     *
     * @param string $ipaddress
     * @return Login
     */
    public function setIpaddress($ipaddress)
    {
        $this->ipaddress = $ipaddress;

        return $this;
    }

    /**
     * Get ipaddress
     *
     * @return string 
     */
    public function getIpaddress()
    {
        return $this->ipaddress;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return Login
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set user
     *
     * @param \GPlainte\UserBundle\Entity\User $user
     * @return Login
     */
    public function setUser(\GPlainte\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \GPlainte\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }



    /**
     * Set dataevent
     *
     * @param \GPlainte\UserBundle\Entity\DataEvent $dataevent
     * @return Login
     */
    public function setDataevent(\GPlainte\UserBundle\Entity\DataEvent $dataevent = null)
    {
        $this->dataevent = $dataevent;

        return $this;
    }

    /**
     * Get dataevent
     *
     * @return \GPlainte\UserBundle\Entity\DataEvent 
     */
    public function getDataevent()
    {
        return $this->dataevent;
    }
}
