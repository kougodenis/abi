<?php

namespace GPlainte\UserBundle\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\Table(name="users")
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="GPlainte\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser {
    /**
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\Column(type="integer")
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    public $rol;
    /**
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $logged;
    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\GPlainteBundle\Entity\Agent",inversedBy="user", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $agent;




    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    



    /**
     * Set agent
     *
     * @param \GPlainte\GPlainteBundle\Entity\Agent $agent
     * @return User
     */
    public function setAgent(\GPlainte\GPlainteBundle\Entity\Agent $agent)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \GPlainte\GPlainteBundle\Entity\Agent 
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set logged
     *
     * @param boolean $logged
     * @return User
     */
    public function setLogged($logged)
    {
        $this->logged = $logged;

        return $this;
    }

    /**
     * Get logged
     *
     * @return boolean 
     */
    public function getLogged()
    {
        return $this->logged;
    }


}
