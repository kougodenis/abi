<?php

namespace GPlainte\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DataEvent
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GPlainte\UserBundle\Entity\DataEventRepository")
 */
class DataEvent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=200)
     */
    private $libelle;

    /**
     *
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="GPlainte\UserBundle\Entity\Login",mappedBy="dataevent")
     */
    private $login;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return DataEvent
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->login = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add login
     *
     * @param \GPlainte\UserBundle\Entity\Login $login
     * @return DataEvent
     */
    public function addLogin(\GPlainte\UserBundle\Entity\Login $login)
    {
        $this->login[] = $login;

        return $this;
    }

    /**
     * Remove login
     *
     * @param \GPlainte\UserBundle\Entity\Login $login
     */
    public function removeLogin(\GPlainte\UserBundle\Entity\Login $login)
    {
        $this->login->removeElement($login);
    }

    /**
     * Get login
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLogin()
    {
        return $this->login;
    }
}
