<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 29/04/2016
 * Time: 12:15
 */

namespace GPlainte\UserBundle\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;
class AlreadyLoggedException extends AuthenticationException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
        return 'You are already logged. Please contact your administrator';
    }
}