<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 29/04/2016
 * Time: 11:11
 */

namespace GPlainte\UserBundle\EventListener;

use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use GPlainte\UserBundle\Entity\Login;
class UserConnected
{
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    // M�thode pour journaliser les connexions
    public function journalize($user)
    {
        $action=$this->em->getRepository('UserBundle:DataEvent')->find(27);
        $login = new Login();
        $login->setUser($user);
        $login->setCreatedat(new \Datetime());
        $login->setIpaddress('0.0.0.0');
        $login->setDataevent($action);
        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $this->em->persist($login);

        // actually executes the queries (i.e. the INSERT query)
        $this->em->flush();
    }

    public function journalizeActivity($code,$user)
    {
        $login = new Login();
        $login->setUser($user);
        $login->setDataevent($code);
        $login->setCreatedat(new \Datetime());
        $login->setIpaddress('0.0.0.0');

        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $this->em->persist($login);

        // actually executes the queries (i.e. the INSERT query)
        $this->em->flush();
    }

    public function createLoggedCookie(){
        $response = new Response();
        //$response->headers->setCookie(new Cookie("_sts_logged", 1, 0, '/', null, false, false));
        $response->headers->setCookie(new Cookie("_sts_logged", 1, time() + (10*365*24*60*60)));
        $response->send();
    }

    // M�thode pour refuser la connexion simultan�e des utilisateurs avec un mm compte
    public function normalize(Response $response, $remainingDays)
    {
        $content = $response->getContent();

        return $response;
    }
    function get_ip_address() {
        $ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
        foreach ($ip_keys as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    // trim for safety measures
                    $ip = trim($ip);
                    // attempt to validate IP
                    if (validate_ip($ip)) {
                        return $ip;
                    }
                }
            }
        }

        return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
    }


    /**
     * Ensures an ip address is both a valid IP and does not fall within
     * a private network range.
     */
    function validate_ip($ip)
    {
        if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
            return false;
        }
        return true;
    }

}