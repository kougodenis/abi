<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 29/04/2016
 * Time: 11:14
 */

namespace GPlainte\UserBundle\EventListener;

use Symfony\Component\HttpFoundation\Response;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;
class LogoutHandler implements LogoutHandlerInterface

{
    private $userManager;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    public function logout (Request $request, Response $response, TokenInterface $token){
        $user = $token->getUser();
        if($user->getLogged()){
            $user->setLogged(false);
            $this->userManager->updateUser($user);
        }
    }
}