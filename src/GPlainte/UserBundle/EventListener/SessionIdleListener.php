<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 09/05/2016
 * Time: 17:34
 */

namespace GPlainte\UserBundle\EventListener;

use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\SecurityContextInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SessionIdleListener
{
    private $userManager;
    protected $userConnected;
    protected $session;
    protected $securityContext;
    protected $router;
    protected $maxIdleTime;

    public function __construct(UserConnected $userConnected,UserManagerInterface $userManager,SessionInterface $session, SecurityContextInterface $securityContext, RouterInterface $router, $maxIdleTime = 1800)
    {
        $this->session = $session;
        $this->securityContext = $securityContext;
        $this->router = $router;
        $this->maxIdleTime = $maxIdleTime;
        $this->userManager = $userManager;
        $this->userConnected = $userConnected;

    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST != $event->getRequestType()) {

            return;
        }

        if ($this->maxIdleTime > 0) {

            $this->session->start();
            $lapse = time() - $this->session->getMetadataBag()->getLastUsed();

            if ($lapse > $this->maxIdleTime) {
                $user = $this->securityContext->getToken()->getUser();
                $this->securityContext->setToken(null);
                $this->session->getFlashBag()->set('info', 'You have been logged out due to inactivity.');
                //Log user out

                //$user = $evt->getAuthenticationToken()->getUser();
                $user->setLogged(false);
                $this->userManager->updateUser($user);
                // Change the route if you are not using FOSUserBundle.
                $event->setResponse(new RedirectResponse($this->router->generate('fos_user_security_login')));
            }
        }
    }

}