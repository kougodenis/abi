<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 29/04/2016
 * Time: 11:12
 */

namespace GPlainte\UserBundle\EventListener;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use GPlainte\UserBundle\Exception\AlreadyLoggedException;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;
class UserConnectedListener
{
    private $userManager;
    protected $userConnected;
    protected $container;

    public function __construct($container,UserConnected $userConnected,UserManagerInterface $userManager)
    {
        $this->container=$container;
        $this->userManager = $userManager;
        $this->userConnected = $userConnected;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        $request=$this->container->get("request");
        $cookies=$request->cookies;

        if($user->getLogged() && !$cookies->has('_sts_logged')){

            throw new AlreadyLoggedException('this user is already logged');
        }else{
            $user->setLogged(true);
            $this->userManager->updateUser($user);
            $this->userConnected->journalize($user);
            $this->userConnected->createLoggedCookie();
        }
    }
}