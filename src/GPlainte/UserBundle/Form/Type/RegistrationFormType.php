<?php

namespace GPlainte\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        // add your custom field
        $builder
           
                ->add('agent',
                'entity', array(
                'class' => 'GPlainte\GPlainteBundle\Entity\Agent',
                'property' => 'nomprenom',
                'multiple'=>false,
                'empty_value'=>"agent.select",
                'label'=>"Nom et prénom", 'translation_domain'=>'messages'

            )
            )
             
            ->add('rol','choice',array('choices'=>array('ROLE_ANALYSTE'=>'profil.analyste',
                'ROLE_VERIFICATEUR'=>'profil.superviseur',
                'ROLE_REDACTEUR'=>'profil.editeur',
                'ROLE_PILOTE'=>'profil.pilote',
				'ROLE_ADMIN'=>'profil.administrateur',
                'ROLE_PERSONNEL'=>'profil.personnel',
                'ROLE_DE'=>'profil.executif',
                'ROLE_RH'=>'profil.respohr'
                ),
                'empty_value'=>'profil.select','label'=>'Profil:','translation_domain'=>'messages'))
             
;
        
    }
//    public function getParent()
//    {
//        return 'fos_user_registration';
//    }
        
    public function getName()
    {
        return 'GPlainte_user_registration';
    }
}

?>
