<?php
namespace GPlainte\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseType;

class ProfileFormType extends BaseType
{
    public function buildUserForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildUserForm($builder, $options);
         $builder
            ->add('institution','entity',array('class'=>'factSheetBundle:Institution','property'=>'sigle','expanded'=>FALSE,
                'multiple'=>false,))
             ->add('institution','entity',array('class'=>'factSheetBundle:Institution','property'=>'sigle','expanded'=>FALSE,
                 'multiple'=>false,))
            ->add('emailParrain','text',array('required'=>false))
            ->add('options','entity',array('class'=>'FiadPlusBundle:Options','property'=>'nom','expanded'=>FALSE, 
                'multiple'=>false,))

;
    }

    public function getName()
    {
        return 'Gplainte_user_profile';
    }
}