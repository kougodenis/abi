<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 17/12/2015
 * Time: 13:58
 */

namespace GPlainte\UserBundle\Redirection;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Routing\Router;

class AfterLoginRedirection implements AuthenticationSuccessHandlerInterface{

    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;
    /**
    �* @param RouterInterface $router
    �*/
    public function __construct(RouterInterface $router){
        $this->router = $router;
    }


    /**
     * This is called when an interactive authentication attempt succeeds. This
     * is called by authentication listeners inheriting from
     * AbstractAuthenticationListener.
     *
     * @param Request $request
     * @param TokenInterface $token
     *
     * @return Response never null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        // TODO: Implement onAuthenticationSuccess() method.
        // On r�cup�re la liste des r�les d'un utilisateur
        $roles = $token->getRoles();
        // On transforme le tableau d'instance en tableau simple
        $rolesTab = array_map(function($role){
            return $role->getRole();
        }, $roles);
        $request->setLocale($request->getLocale());
        if (in_array('ROLE_PERSONNEL', $rolesTab, true) || in_array('ROLE_SUPER_ADMIN', $rolesTab, true))
            $redirection = new RedirectResponse($this->router->generate('gplainte_personnel_dashboard'));
        else
            $redirection = new RedirectResponse($this->router->generate('gplainte_dashboard'));
        return $redirection;
    }
}