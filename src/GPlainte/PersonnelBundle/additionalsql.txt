ALTER TABLE complaint ADD ticket_number INT NOT NULL;

ALTER TABLE complaint ADD published_at DATETIME NOT NULL, ADD solution LONGTEXT NOT NULL, ADD action_preventive LONGTEXT NOT NULL, ADD observation LONGTEXT NOT NULL, ADD is_published INT NOT NULL;
ALTER TABLE complaint CHANGE is_anonyme is_anonyme TINYINT(1) NOT NULL, CHANGE published_at published_at DATETIME DEFAULT NULL, CHANGE solution solution LONGTEXT DEFAULT NULL, CHANGE action_preventive action_preventive LONGTEXT DEFAULT NULL, CHANGE observation observation LONGTEXT DEFAULT NULL;

ALTER TABLE complaint CHANGE user_id user_id INT DEFAULT NULL;

ALTER TABLE complaint ADD is_archived INT NOT NULL;