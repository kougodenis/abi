<?php

namespace GPlainte\PersonnelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ComplaintType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('object')
            ->add('ticketNumber')
            ->add('place')
            ->add('indexed')
            ->add('dateEvent')
            ->add('createdAt')
            ->add('publishedAt')
            ->add('description')
            ->add('solution')
            ->add('actionPreventive')
            ->add('observation')
            ->add('isAnonyme')
            ->add('isRecevable')
            ->add('isSatisfied')
            ->add('isPublished')
            ->add('isProcessed')
            ->add('isValidated')
            ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPlainte\PersonnelBundle\Entity\Complaint'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gplainte_personnelbundle_complaint';
    }
}
