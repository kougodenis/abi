<?php

namespace GPlainte\PersonnelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Complaint
 *
 * @Doctrine\ORM\Mapping\Table(name="complaint")
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks()
 */
class Complaint
{
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="id", type="integer")
     * @Doctrine\ORM\Mapping\Id
     * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="object", type="string")
     */
    private $object;

    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="ticket_number", type="integer")
     */
    private $ticketNumber;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="place", type="string")
     */
    private $place;

    /**
     * @var string
     *
     * @Doctrine\ORM\Mapping\Column(name="indexed", type="string")
     */
    private $indexed;

    /**
     * @var datetime
     *
     * @Doctrine\ORM\Mapping\Column(name="date_event", type="datetime")
     */
    private $dateEvent;

    /**
     * @var datetime
     *
     * @Doctrine\ORM\Mapping\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var datetime
     *
     * @Doctrine\ORM\Mapping\Column(name="published_at", type="datetime", nullable=true)
     */
    private $publishedAt;

    /**
     * @var text
     *
     * @Doctrine\ORM\Mapping\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var text
     *
     * @Doctrine\ORM\Mapping\Column(name="solution", type="text", nullable=true)
     */
    private $solution;

    /**
     * @var text
     *
     * @Doctrine\ORM\Mapping\Column(name="action_preventive", type="text", nullable=true)
     */
    private $actionPreventive;

    /**
     * @var text
     *
     * @Doctrine\ORM\Mapping\Column(name="observation", type="text", nullable=true)
     */
    private $observation;
    /**
     * @var boolean
     *
     * @Doctrine\ORM\Mapping\Column(name="is_anonyme", type="boolean")
     */
    private $isAnonyme=false;
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="is_recevable", type="integer")
     */
    private $isRecevable=2;

    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="is_satisfied", type="integer")
     */
    private $isSatisfied=0;
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="is_archived", type="integer")
     */
    private $isArchived=0;
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="is_published", type="integer")
     */
    private $isPublished=0;
    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="is_processed", type="integer")
     */
    private $isProcessed=0;

    /**
     * @var integer
     *
     * @Doctrine\ORM\Mapping\Column(name="is_validated", type="integer")
     */
    private $isValidated=0;


    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="GPlainte\UserBundle\Entity\User")
     * @Doctrine\ORM\Mapping\JoinColumn(nullable=true)
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isRecevable
     *
     * @param integer $isRecevable
     * @return Complaint
     */
    public function setIsRecevable($isRecevable)
    {
        $this->isRecevable = $isRecevable;

        return $this;
    }

    /**
     * Get isRecevable
     *
     * @return integer
     */
    public function getIsRecevable()
    {
        return $this->isRecevable;
    }

    /**
     * Set isSatisfied
     *
     * @param integer $isSatisfied
     * @return Complaint
     */
    public function setIsSatisfied($isSatisfied)
    {
        $this->isSatisfied = $isSatisfied;

        return $this;
    }

    /**
     * Get isSatisfied
     *
     * @return integer
     */
    public function getIsSatisfied()
    {
        return $this->isSatisfied;
    }

    /**
     * Set isProcessed
     *
     * @param integer $isProcessed
     * @return Complaint
     */
    public function setIsProcessed($isProcessed)
    {
        $this->isProcessed = $isProcessed;

        return $this;
    }

    /**
     * Get isProcessed
     *
     * @return integer
     */
    public function getIsProcessed()
    {
        return $this->isProcessed;
    }

    /**
     * Set isValidated
     *
     * @param integer $isValidated
     * @return Complaint
     */
    public function setIsValidated($isValidated)
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    /**
     * Get isValidated
     *
     * @return integer
     */
    public function getIsValidated()
    {
        return $this->isValidated;
    }

    /**
     * @Doctrine\ORM\Mapping\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Set object
     *
     * @param string $object
     * @return Complaint
     */
    public function setObject($object)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get object
     *
     * @return string 
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * Set place
     *
     * @param string $place
     * @return Complaint
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string 
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set indexed
     *
     * @param string $indexed
     * @return Complaint
     */
    public function setIndexed($indexed)
    {
        $this->indexed = $indexed;

        return $this;
    }

    /**
     * Get indexed
     *
     * @return string 
     */
    public function getIndexed()
    {
        return $this->indexed;
    }

    /**
     * Set dateEvent
     *
     * @param \DateTime $dateEvent
     * @return Complaint
     */
    public function setDateEvent($dateEvent)
    {
        $this->dateEvent = $dateEvent;

        return $this;
    }

    /**
     * Get dateEvent
     *
     * @return \DateTime 
     */
    public function getDateEvent()
    {
        return $this->dateEvent;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Complaint
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Complaint
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set user
     *
     * @param \GPlainte\UserBundle\Entity\User $user
     * @return Complaint
     */
    public function setUser(\GPlainte\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \GPlainte\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set ticketNumber
     *
     * @param integer $ticketNumber
     * @return Complaint
     */
    public function setTicketNumber($ticketNumber)
    {
        $this->ticketNumber = $ticketNumber;

        return $this;
    }

    /**
     * Get ticketNumber
     *
     * @return integer 
     */
    public function getTicketNumber()
    {
        return $this->ticketNumber;
    }

    /**
     * Set isAnonyme
     *
     * @param integer $isAnonyme
     * @return Complaint
     */
    public function setIsAnonyme($isAnonyme)
    {
        $this->isAnonyme = $isAnonyme;

        return $this;
    }

    /**
     * Get isAnonyme
     *
     * @return integer 
     */
    public function getIsAnonyme()
    {
        return $this->isAnonyme;
    }

    /**
     * Set solution
     *
     * @param string $solution
     * @return Complaint
     */
    public function setSolution($solution)
    {
        $this->solution = $solution;

        return $this;
    }

    /**
     * Get solution
     *
     * @return string 
     */
    public function getSolution()
    {
        return $this->solution;
    }

    /**
     * Set actionPreventive
     *
     * @param string $actionPreventive
     * @return Complaint
     */
    public function setActionPreventive($actionPreventive)
    {
        $this->actionPreventive = $actionPreventive;

        return $this;
    }

    /**
     * Get actionPreventive
     *
     * @return string 
     */
    public function getActionPreventive()
    {
        return $this->actionPreventive;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return Complaint
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set isPublished
     *
     * @param integer $isPublished
     * @return Complaint
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return integer 
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set publishedAt
     *
     * @param \DateTime $publishedAt
     * @return Complaint
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime 
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set isArchived
     *
     * @param integer $isArchived
     * @return Complaint
     */
    public function setIsArchived($isArchived)
    {
        $this->isArchived = $isArchived;

        return $this;
    }

    /**
     * Get isArchived
     *
     * @return integer 
     */
    public function getIsArchived()
    {
        return $this->isArchived;
    }
}
