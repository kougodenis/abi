<?php

namespace GPlainte\PersonnelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use GPlainte\PersonnelBundle\Entity\Complaint;


class ComplaintController extends Controller
{
    public function dashboardAction(){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            //get Complaint's repository
            $repository = $this->getDoctrine()
                ->getRepository('GPlaintePersonnelBundle:Complaint');
            // query for all complaints matching the is_published, is_anonyme
            $complaints = $repository->findBy(
                array('isAnonyme' => 1,'isPublished' => 1),
                array('createdAt' => 'desc')
            );
            return $this->render('GPlaintePersonnelBundle:Complaint:dashboard.html.twig',array(
                'complaints' =>$complaints
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }
    public function mycomplaintsAction(Request $request){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $user = $this->container->get('security.context')->getToken()->getUser();
            $user->getId();
            //get Complaint's repository
            $repository = $this->getDoctrine()
                ->getRepository('GPlaintePersonnelBundle:Complaint');
            // query for all complaints matching the is_published, is_anonyme
            $mycomplaints = $repository->findBy(
                array('user' => $user->getId(),'isAnonyme'=>0),
                array('createdAt' => 'desc')
            );
            // create form for saving a complaint
            $complaint=new Complaint();
            $form = $this->createFormBuilder($complaint)

                ->add('object','text')
                ->add('ticketNumber','text')
                ->add('place', 'text')
                ->add('indexed','text')
                //->add('dateEvent','date', array('widget' => 'single_text'))
                ->add('dateEvent','text')
                ->add('description','textarea')
                ->add('isAnonyme',null,array('required'=>false))

                ->add('save', 'submit')
                ->getForm();
            $form->handleRequest($request);
            if ($this->getRequest()->getMethod() == 'POST')
            {
                $em = $this->getDoctrine()->getManager();

                $newComplaint = new Complaint();

                $newComplaint->setTicketNumber($form->get('ticketNumber')->getData());
                //print_r($form->get('ticketNumber')->getData());

                $newComplaint->setObject($form->get('object')->getData());

                $s=$form->get('dateEvent')->getData();
                //$s= (new \DateTime($s))->format('d/m/Y');
                $newComplaint->setDateEvent(\DateTime::createFromFormat('d/m/Y', $s));
                /*var_dump(date_format(new \DateTime($s),'d/m/Y'));
                var_dump($s);
                var_dump($newComplaint->getDateEvent());*/
                $newComplaint->setPlace($form->get('place')->getData());
                $newComplaint->setIndexed($form->get('indexed')->getData());
                $newComplaint->setDescription($form->get('description')->getData());
                $newComplaint->setIsAnonyme($form->get('isAnonyme')->getData());
                if($form->get('isAnonyme')->getData()!=1){
                    $newComplaint->setUser($user);
                }
                // TODO: Mailing here
                //$mailer = $this->container->get('mailer');
                $message = \Swift_Message::newInstance()
                    ->setSubject('New complaint created')
                    ->setFrom('festus@gmail.com')
                    ->setTo('festus@dmdconsult.com')
                    ->setBody(
                        $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                            'Emails/complaintcreated.html.twig',
                            array('ticketNumber' => $newComplaint->getTicketNumber(),'object' => $newComplaint->getObject(),'isAnonymous' => $newComplaint->getIsAnonyme())
                        ),
                        'text/html'
                    )
                ;
                $this->get('mailer')->send($message);

                //print_r($newComplaint);
                $em->persist($newComplaint);
                $em->flush();
                $msg=1;
                return $this->redirect($this->generateUrl('gplainte_personnel_mycomplaints',array('msg'=>$msg)));
            }




            return $this->render('GPlaintePersonnelBundle:Complaint:mycomplaints.html.twig',array(
                'mycomplaints' =>$mycomplaints,
                'form' => $form->createView(),
                /*'formValid' => $formValid,*/
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }
    public function despoliationAction(){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            //get Complaint's repository
            $repository = $this->getDoctrine()
                ->getRepository('GPlaintePersonnelBundle:Complaint');
            // query for all complaints matching the is_published, is_anonyme
            $dcomplaints = $repository->findBy(
                array('isRecevable' => 2),
                array('createdAt' => 'desc')
            );
            return $this->render('GPlaintePersonnelBundle:Complaint:despoliation.html.twig',array(
                'dcomplaints'=>$dcomplaints,
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }
    public function processingAction(Request $request){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            //get Complaint's repository
            $repository = $this->getDoctrine()
                ->getRepository('GPlaintePersonnelBundle:Complaint');
            // query for all complaints matching the is_published, is_anonyme
            $pcomplaints = $repository->findBy(
                array('isRecevable' => 1,'isProcessed' => 0),
                array('createdAt' => 'desc')
            );
            // create form for saving a complaint
            $complaint=new Complaint();
            $form = $this->createFormBuilder($complaint)
                ->add('solution','textarea')
                ->add('actionPreventive','textarea')
                ->add('observation', 'textarea')

                ->add('process', 'submit')
                ->add('validate', 'submit')
                ->getForm();
            $form->handleRequest($request);
            if ($this->getRequest()->getMethod() == 'POST')
            {
                $em = $this->getDoctrine()->getManager();
                $prcomplaint = $repository->findOneBy(
                    array('id' => $request->request->get('cid'))

                );
                if($form->get('process')->isClicked()){

                    $prcomplaint->setSolution($form->get('solution')->getData());
                    $prcomplaint->setActionPreventive($form->get('actionPreventive')->getData());
                    $prcomplaint->setObservation($form->get('observation')->getData());
                    $prcomplaint->setIsProcessed(1);
                    // TODO: Mailing here to DE
                    //$mailer = $this->container->get('mailer');
                    $message = \Swift_Message::newInstance()
                        ->setSubject('New complaint created')
                        ->setFrom('festus@gmail.com')
                        ->setTo('festus@dmdconsult.com')
                        ->setBody(
                            $this->renderView(
                            // app/Resources/views/Emails/registration.html.twig
                                'Emails/rhprocess.html.twig',
                                array('ticketNumber' => $prcomplaint->getTicketNumber(),'object' => $prcomplaint->getObject(),'isAnonymous' => $prcomplaint->getIsAnonyme())
                            ),
                            'text/html'
                        )
                    ;
                    $this->get('mailer')->send($message);

                }
                else{
                    $prcomplaint->setSolution($form->get('solution')->getData());
                    $prcomplaint->setActionPreventive($form->get('actionPreventive')->getData());
                    $prcomplaint->setObservation($form->get('observation')->getData());
                    $prcomplaint->setIsProcessed(1);
                    $prcomplaint->setIsValidated(1);
                    if($prcomplaint->getIsAnonyme()==true){
                        $prcomplaint->setIsPublished(1);
                        $prcomplaint->setPublishedAt(new \DateTime());
                    }

                }




                //print_r($newComplaint);
                $em->persist($prcomplaint);
                $em->flush();
                $msg=1;
                return $this->redirect($this->generateUrl('gplainte_personnel_processing',array('msg'=>$msg)));
            }
            return $this->render('GPlaintePersonnelBundle:Complaint:processing.html.twig',array(
                'pcomplaints'=>$pcomplaints,
                'form' => $form->createView(),
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }
    public function validationAction(Request $request){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            //get Complaint's repository
            $repository = $this->getDoctrine()
                ->getRepository('GPlaintePersonnelBundle:Complaint');
            // query for all complaints matching the is_published, is_anonyme
            $vcomplaints = $repository->findBy(
                array('isRecevable' => 1,'isProcessed' => 1,'isValidated' => 0 ),
                array('createdAt' => 'desc')
            );
            // create form for saving a complaint
            $complaint=new Complaint();
            $form = $this->createFormBuilder($complaint)
                ->add('solution','textarea')
                ->add('actionPreventive','textarea')
                ->add('observation', 'textarea')
                ->add('validate', 'submit')
                ->getForm();
            $form->handleRequest($request);
            if ($this->getRequest()->getMethod() == 'POST')
            {
                $em = $this->getDoctrine()->getManager();
                $vacomplaint = $repository->findOneBy(
                    array('id' => $request->request->get('cid'))

                );

                $vacomplaint->setSolution($form->get('solution')->getData());
                $vacomplaint->setActionPreventive($form->get('actionPreventive')->getData());
                $vacomplaint->setObservation($form->get('observation')->getData());
                $vacomplaint->setIsProcessed(1);
                $vacomplaint->setIsValidated(1);
                if($vacomplaint->getIsAnonyme()==true){
                    $vacomplaint->setIsPublished(1);
                    $vacomplaint->setPublishedAt(new \DateTime());
                }

                //print_r($newComplaint);
                $em->persist($vacomplaint);
                $em->flush();
                $msg=1;
                return $this->redirect($this->generateUrl('gplainte_personnel_validation',array('msg'=>$msg)));
            }
            return $this->render('GPlaintePersonnelBundle:Complaint:validation.html.twig',array(
                'vcomplaints'=>$vcomplaints,
                'form' => $form->createView(),
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }
    public function archivingAction(){
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            //get Complaint's repository
            $repository = $this->getDoctrine()
                ->getRepository('GPlaintePersonnelBundle:Complaint');
            // query for all complaints matching the is_published, is_anonyme
            $archcomplaints = $repository->findBy(
                array('isArchived'=>1),
                array('createdAt' => 'desc')
            );
            return $this->render('GPlaintePersonnelBundle:Complaint:archiving.html.twig',array(
                'archcomplaints' =>$archcomplaints,
            ));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }

    public function receiveAction($id){
        //get Complaint's repository
        $repository = $this->getDoctrine()
            ->getRepository('GPlaintePersonnelBundle:Complaint');
        // query for all complaints matching the is_published, is_anonyme
        $nrComplaint = $repository->findOneBy(
            array('id' => $id)
        );
        /*if($nrComplaint->getIsAnonyme()==true){
            $nrComplaint->setSolution("Plainte irrecevable");

        }*/
        $nrComplaint->setIsRecevable(1);
        /*$nrComplaint->setPublishedAt(new \DateTime());
        $nrComplaint->setIsPublished(1);*/
        $em = $this->getDoctrine()->getManager();

        $em->persist($nrComplaint);
        $em->flush();
        $msg=1;
        return $this->redirect($this->generateUrl('gplainte_personnel_despoliation',array('msg'=>$msg)));


    }
    public function notreceiveAction($id){
        //get Complaint's repository
        $repository = $this->getDoctrine()
            ->getRepository('GPlaintePersonnelBundle:Complaint');
        // query for all complaints matching the is_published, is_anonyme
        $nrComplaint = $repository->findOneBy(
            array('id' => $id)
        );
        if($nrComplaint->getIsAnonyme()==true){
            $nrComplaint->setSolution("Plainte irrecevable");


        }
        $nrComplaint->setIsRecevable(0);
        $nrComplaint->setPublishedAt(new \DateTime());
        $nrComplaint->setIsPublished(1);
        $nrComplaint->setIsArchived(1);

        $em = $this->getDoctrine()->getManager();

        $em->persist($nrComplaint);
        $em->flush();
        $msg=1;
        return $this->redirect($this->generateUrl('gplainte_personnel_despoliation',array('msg'=>$msg)));


    }
    public function satisfyAction($id){
        //get Complaint's repository
        $repository = $this->getDoctrine()
            ->getRepository('GPlaintePersonnelBundle:Complaint');
        // query for all complaints matching the is_published, is_anonyme
        $sComplaint = $repository->findOneBy(
            array('id' => $id)
        );
        /*if($nrComplaint->getIsAnonyme()==true){
            $nrComplaint->setSolution("Plainte irrecevable");

        }*/
        $sComplaint->setIsSatisfied(1);
        $sComplaint->setIsArchived(1);
        /*$nrComplaint->setPublishedAt(new \DateTime());
        $nrComplaint->setIsPublished(1);*/
        $em = $this->getDoctrine()->getManager();

        $em->persist($sComplaint);
        $em->flush();
        $msg=1;
        return $this->redirect($this->generateUrl('gplainte_personnel_mycomplaints',array('msg'=>$msg)));


    }
    public function notsatisfyAction($id){
        //get Complaint's repository
        $repository = $this->getDoctrine()
            ->getRepository('GPlaintePersonnelBundle:Complaint');
        // query for all complaints matching the is_published, is_anonyme
        $nsComplaint = $repository->findOneBy(
            array('id' => $id)
        );
        /*if($nrComplaint->getIsAnonyme()==true){
            $nrComplaint->setSolution("Plainte irrecevable");

        }*/
        $nsComplaint->setIsRecevable(1);
        $nsComplaint->setIsProcessed(0);
        $nsComplaint->setIsValidated(0);
        /*$nrComplaint->setPublishedAt(new \DateTime());
        $nrComplaint->setIsPublished(1);*/

        // TODO: Mailing here
        //$mailer = $this->container->get('mailer');
        $message = \Swift_Message::newInstance()
            ->setSubject('New complaint created')
            ->setFrom('festus@gmail.com')
            ->setTo('festus@dmdconsult.com')
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'Emails/usernotsatisfied.html.twig',
                    array('ticketNumber' => $nsComplaint->getTicketNumber(),'object' => $nsComplaint->getObject(),'isAnonymous' => $nsComplaint->getIsAnonyme())
                ),
                'text/html'
            )
        ;
        $this->get('mailer')->send($message);
        $em = $this->getDoctrine()->getManager();

        $em->persist($nsComplaint);
        $em->flush();
        $msg=1;
        return $this->redirect($this->generateUrl('gplainte_personnel_mycomplaints',array('msg'=>$msg)));


    }
}
