
############################################################
#   CUSTOMIZR BUNDLE    ####################################
#   By FESTUS CODJO     ####################################
############################################################


INSTALLATION
1- Extract the zip file in the following folder src/GPlainte/
2- Enable the Bundle in the kernel by adding this line in app/AppKernel.php
    new GPlainte\CustomizrBundle\CustomizrBundle(),
3- Copy asset/Customizr in the web directory of the project
4- Copy asset/jquery-minicolors in web/assets/plugins/
INTEGRATION

1- Import routing config to main config by adding the following code to app/config/routing.yml
    customizr:
        resource: "@CustomizrBundle/Resources/config/routing.yml"
        prefix:   /

2- Add a link in the menu and set its href to
    {{path('customizr_theme)}}
3- Open app/resources/views/base.html.twig and add
    * in the stylesheet section:
     <!--customizr link-->
     <link href="{{asset('assets/plugins/jquery-minicolors/jquery.minicolors.css')}}" type="text/css" rel="stylesheet" />
     <!--end customizr link-->
    * in the javascript section:
     <!--customizr script-->
     <script type="text/javascript" src="{{asset('customizr/customizr.js')}}"></script>

         <script>
         $(document).ready(function() {
             //set customizr root directory
             var _CUSTOMIZR_DIR = "{{asset('customizr')}}";
             //call to function responsible for setting the theme
             overrider(_CUSTOMIZR_DIR)
         });
     </script>
     <!--end customizr script-->
4- Add id="companyLogo" to logo's img markup
5- Add id="avatar_span" to the span below the avatar

////CHANGES TO PARENT
