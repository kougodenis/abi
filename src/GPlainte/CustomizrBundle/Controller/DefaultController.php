<?php

namespace GPlainte\CustomizrBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Filesystem\Filesystem;
class DefaultController extends Controller
{

    public function indexAction()
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            return $this->render('CustomizrBundle:Default:index.html.twig');

        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }
    }
    public function themeAction()
    {
        if($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $str=file_get_contents('customizr/data/customizr.json');
            $json = json_decode($str, true);
            return $this->render('CustomizrBundle:Default:theme.html.twig',array("parameters"=>$json));
        }
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }

    }
    public function applyAction(Request $request)
    {

            //Collecting form data
            $name=$request->request->get('file_logo_name');
            $directory="customizr/logo";
            $file_logo_url = $request->files->get('file_logo_url');
            $flag=false;
            if($file_logo_url!=null){
                if($file_logo_url->move($directory, $name)){
                    $flag=true;
                }
            }
            $table_header = $request->request->get('table_header');
            $link_active = $request->request->get('link_active');
            $link_hover = $request->request->get('link_hover');
            $logout_button_default = $request->request->get('logout_button_default');
            $logout_button_hover = $request->request->get('logout_button_hover');
            $primary_button_hover = $request->request->get('primary_button_hover');
            $primary_button_default = $request->request->get('primary_button_default');
            //
            $data = array(
                'flag' => $flag,
                'status' => 'success',
                'file_logo_name' => $name,
                'table_header' =>$table_header,
                'link_active' =>$link_active,
                'link_hover' =>$link_hover,
                'logout_button_default' =>$logout_button_default,
                'logout_button_hover' =>$logout_button_hover,
                'primary_button_hover' =>$primary_button_hover,
                'primary_button_default' =>$primary_button_default,
            );
            //
            $json= json_encode($data);
            //
            $response= new JsonResponse($data);
            //
            $fs = new Filesystem();

            try {
                $fs->dumpFile('customizr/data/customizr.json', $json);
            }
            catch(IOException $e) {
                echo "An error occurred while creating your file at ".$e->getPath();
            }
            //
            return $response ;
        /*}
        else{
            return $this->redirect( $this->generateUrl('fos_user_security_login'));
        }*/

    }
}
