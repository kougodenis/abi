Instructions en ligne de commande
Vous pouvez également télécharger des fichiers existants à partir de votre ordinateur en suivant les instructions ci-dessous.


Configuration globale Git
git config --global user.name "Edgar D. AYENA"
git config --global user.email "ayenadedg@gmail.com"

Créer un nouveau référentiel
clone de git https://gitlab.com/kougodenis/abi.git
cd abi
touchez README.md
git add LISEZMOI.md
git commit -m "add README"
git push -u origine master

Poussez un dossier existant
cd nom_fichier existant
git init
git remote ajouter l'origine https://gitlab.com/kougodenis/abi.git
git add.
git commit -m "Commit initial"
git push -u origine master

Poussez un référentiel Git existant
cd existant_repo
git remote renommer l'origine ancienne-origine
git remote ajouter l'origine https://gitlab.com/kougodenis/abi.git 
git push -u origin --all
git push -u origin --tags