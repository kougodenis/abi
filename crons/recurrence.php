<?php
	//Setting path for json file
	$path = '../src/GPlainte/NotificationBundle/Data/';
    $filename = 'notification.json';
    $params = json_decode(file_get_contents($path . '' . $filename));
	//Computing duration
	$datetime=explode(' ',$params->ref_date);
    $date = explode('.',$datetime[0]);
    $hour = explode(':',$datetime[1]);
    $duration = time()-mktime($hour[0],$hour[1],$hour[2],$date[1],$date[0],$date[2]);
	//Reset reference date in notification.json
	if($duration >= intval($params->pr)*24*86400){
		$params->ref_date = datetime('d.m.Y H:i:s');
		$data = json_encode(array('dm' => $dureeMax, 'pr' => $periodeRecurrence, 'sr' => $seuilRecurrence, 'mails' => $mails, 'profils' => $profils,'ref_date'=>$today));
		file_put_contents($path . '' . $filename, $data);
	}
?>