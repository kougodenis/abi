<?php
$servername = "localhost";
$username = "root";
$password = "";

try {
    $conn = new PDO("mysql:host=$servername;dbname=satis", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $txt = "Connected successfully"; 
    $myfile = file_put_contents('connections.lg', $txt.PHP_EOL , FILE_APPEND);
    }
catch(PDOException $e)
    {
    $txt = "Connection failed: " . $e->getMessage();
    $myfile = file_put_contents('connections.lg', $txt.PHP_EOL , FILE_APPEND);
    }