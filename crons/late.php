<?php
	//DB connection for mysql, sql server.....
	$sgbd='mysql';
	switch($sgbd){
		case 'mysql':
			require_once('mysql.php');
		    break;
		case 'sqlserver':
		    require_once('sqlserver.php');
		    break;
		default:
		    require_once('mysql.php');
		    break;
	}
	//Setting path for json file
	$path = '../src/GPlainte/NotificationBundle/Data/';
    $filename = 'notification.json';
    $params = json_decode(file_get_contents($path . '' . $filename));
    //Fetching complaints that are late
	$currentDate = time();
	
	//$results = 'select * from plainte where TIMESTAMPDIFF(SECOND ,unix_timestamp(dateCreation),'.$currentDate.')>='.intval($params->dm)*24*86400;
	//SELECT * FROM `plainte` WHERE (UNIX_TIMESTAMP() - UNIX_TIMESTAMP(dateCreation))>=4147200
	//SELECT * FROM `plainte` WHERE (UNIX_TIMESTAMP() - UNIX_TIMESTAMP(dateCreation))>=4147200 AND traitement = 0
	//SELECT * FROM `plainte` WHERE (UNIX_TIMESTAMP() - UNIX_TIMESTAMP(dateCreation))>=4147200 AND traitement = 0 ORDER BY `dateCreation` DESC 
	//$query = 'select * from plainte where '.$currentDate - UNIX_TIMESTAMP(dateCreation).'>='.intval($params->dm)*24*86400;
	$query = $conn->prepare('select * from plainte where :currentDate - UNIX_TIMESTAMP(dateCreation)>=:dm ORDER BY dateCreation DESC');
	$query->bindValue(':currentDate', $currentDate);
	$query->bindValue(':dm',intval($params->dm)*24*86400);
	$query->execute();
	$results = $query->fetchAll();
	//var_dump($results);
	//'select * from affecter leftjoin'
	/*echo 'Il y a '.count($results).' plaintes en retard dans le systeme.</br>';
	foreach($results as $row => $result){
		echo ''.$result["id"].'</br>';
	}*/
	
	if(count($results)>0){
		foreach($results as $row => $result){
			//echo $result["id"].'<br>';
			
			//check if plainte id is already in notifs table, if it is in then we shouldn't save it again.
			$countquery = $conn->prepare('select count(*) as plaintein from notifs where plainteid = ?');
			//$countquery->bindValue(':i', $result["id"]);
			//print_r($countquery);
			$countquery->execute(array($result["id"]));
			$count = $countquery->fetchColumn();
			//print_r($count);
			if($count==0){
				//Create and save notifications if notification		
				$msg = 'La plainte plainte ayant pour id '.$result["id"].' enregistrée le '.$result["dateCreation"].' a excédé la durée maximale('.$params->dm.') des plaintes dans le systeme sans être traitées.';
				$insertquery=$conn->prepare('INSERT INTO notifs VALUES("",?,"late",?,?,?)');
				$insertquery->execute(array($msg,date("Y-m-d H:i:s"),date("Y-m-d H:i:s"),$result["id"]));
				//Create and send mail associated with notifications
				//mail( $to, $subject, $message, $headers, $parameters );
			}
			
		}

	}
	
	
	/**/
$conn=null;
?>